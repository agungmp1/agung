-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 24, 2021 at 05:50 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_karya`
--

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE `buildings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `locuseId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buildings`
--

INSERT INTO `buildings` (`id`, `name`, `locuseId`) VALUES
(1, 'Asrama Putra A', 1),
(2, 'Asrama Putra B', 1),
(3, 'Asrama Putra C', 1),
(4, 'Asrama Pusdiklat', 1),
(5, 'Gedung C', 2),
(6, 'Gedung D', 2),
(7, 'Gedung F', 2);

-- --------------------------------------------------------

--
-- Table structure for table `configs`
--

CREATE TABLE `configs` (
  `id` int(11) NOT NULL,
  `fastingopen` tinyint(1) NOT NULL DEFAULT 0,
  `fastingdate` varchar(255) DEFAULT NULL,
  `commerceopen` tinyint(1) NOT NULL DEFAULT 0,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configs`
--

INSERT INTO `configs` (`id`, `fastingopen`, `fastingdate`, `commerceopen`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Sat, 23 October 2021', 0, '2021-10-10 08:10:56', '2021-10-24 10:25:56');

-- --------------------------------------------------------

--
-- Table structure for table `ctf_categories`
--

CREATE TABLE `ctf_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ctf_categories`
--

INSERT INTO `ctf_categories` (`id`, `name`) VALUES
(2, 'Forensics'),
(5, 'Malicious'),
(4, 'Programming'),
(3, 'Reverse Engineering'),
(1, 'Web');

-- --------------------------------------------------------

--
-- Table structure for table `ctf_challenges`
--

CREATE TABLE `ctf_challenges` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `point` int(11) NOT NULL,
  `flag` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `ctfCategoryId` int(11) DEFAULT NULL,
  `ctfLevelId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ctf_challenges`
--

INSERT INTO `ctf_challenges` (`id`, `title`, `description`, `point`, `flag`, `createdAt`, `updatedAt`, `ctfCategoryId`, `ctfLevelId`) VALUES
(2, 'Test', 'Just a test.', 1, '$2a$08$WtrwcyECsvcSIZ8R7cgDFO1Ex1Kx9mtobxnfRS6gikGKudPv8cUd6', '2021-10-19 01:39:34', '2021-10-23 06:07:00', 1, 1),
(4, 'My BOT', 'This is bot', 1, '$2a$08$83WFMrWUN/xLiDGaVW8bBOg6WUz.EzGQExz3Z1UeEDVAhVhGHw.E6', '2021-10-22 17:01:03', '2021-10-23 06:06:43', 3, 3),
(7, 'Puzzles', 'Puzzles', 1, '$2a$08$YGjd364gWqd2VgtjntF5ZOm5.mOj9hmYjeToPxoIyDheYG1n/Jl0S', '2021-10-23 06:07:39', '2021-10-23 06:07:39', 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `ctf_challenge_completions`
--

CREATE TABLE `ctf_challenge_completions` (
  `id` int(11) NOT NULL,
  `writeup` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `userNpm` varchar(255) DEFAULT NULL,
  `ctfChallengeId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ctf_challenge_completions`
--

INSERT INTO `ctf_challenge_completions` (`id`, `writeup`, `createdAt`, `updatedAt`, `userNpm`, `ctfChallengeId`) VALUES
(2, 'http://fakelink.example', '2021-10-19 02:20:58', '2021-10-19 02:20:58', '2019101648', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ctf_levels`
--

CREATE TABLE `ctf_levels` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ctf_levels`
--

INSERT INTO `ctf_levels` (`id`, `name`) VALUES
(1, 'ez'),
(3, 'frustating'),
(2, 'normal');

-- --------------------------------------------------------

--
-- Table structure for table `fasting_records`
--

CREATE TABLE `fasting_records` (
  `npm` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fasting_records`
--

INSERT INTO `fasting_records` (`npm`) VALUES
('1817101400'),
('1817101412'),
('1817101465'),
('1817101466'),
('1817101467'),
('1817101468'),
('2019101600'),
('2019101601'),
('2019101602'),
('2019101631');

-- --------------------------------------------------------

--
-- Table structure for table `fouls`
--

CREATE TABLE `fouls` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fouls`
--

INSERT INTO `fouls` (`id`, `name`) VALUES
(1, 'Hukuman Taruna Jaga'),
(5, 'Ketidakrapihan Kamar/Kelas'),
(4, 'Ketidakrapihan Pribadi'),
(6, 'Lainnya'),
(2, 'Terlambat Kegiatan'),
(3, 'Tidak Mengikuti Kegiatan');

-- --------------------------------------------------------

--
-- Table structure for table `foul_records`
--

CREATE TABLE `foul_records` (
  `id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `npm` varchar(255) DEFAULT NULL,
  `foulId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `foul_records`
--

INSERT INTO `foul_records` (`id`, `date`, `npm`, `foulId`) VALUES
(1, '2021-12-21', '2019101609', 2),
(2, '2021-12-21', '2019101648', 1);

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `stock` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `iconId` int(11) NOT NULL DEFAULT 1,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `name`, `stock`, `price`, `iconId`, `createdAt`, `updatedAt`) VALUES
(19, 'ABC BATTERY ALKALINE AAA/LR03/2\'S MILLENNIUM PWR PCK', 3, 19000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(20, 'ABC BATTERY ALKALINE AA-LR6/2\'S MILLENNIUM PWR PCK', 3, 18000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(21, 'BENG-BENG WAFER CHOCOLATE BOX 20x20g', 80, 27000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(22, 'CHARM PEMBALUT SAFE NIGHT 10\'S WING PCK 29cm', 10, 9500, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(23, 'CHITATO SNACK POTATO CHIPS SAPI PANGGANG PCK 68g', 30, 10000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(24, 'DAIA DETERGENT BUBUK SENSASI EXTRAK BUNGA BAG 850/900g', 3, 17500, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(25, 'DOWNY SOFTENER MYSTIQUE PCH 680/650mL', 3, 28000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(26, 'DOWNY SOFTENER MYSTIQUE SCT 19mL', 120, 1000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(27, 'DOWNY SOFTENER PASSION PCH 680/650mL', 3, 28000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(28, 'DOWNY SOFTENER PASSION SCT 20mL', 120, 1000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(29, 'ENERGEN CEREAL INSTANT JAGUNG RCG 10x25g', 3, 17000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(30, 'GOOD DAY KOPI INSTANT 3 IN 1 VANILLA LATTE BAG 50x20g', 5, 50000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(31, 'KAO ATTACK DETERGENT POWDER CLEAN MAXIMIZER BAG 800g', 3, 21000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(32, 'KAO ATTACK DETERGENT POWDER PLUS SOFTENER PCK 800g', 3, 21000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(33, 'KIWI PASTE BLACK KLG 17.5mL', 5, 8000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(34, 'LAURIER PEMBALUT RELAX NIGHT 16\'S WITH GATHERS WG PCK 35 cm', 10, 30000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(35, 'LUWAK WHITE KOFFIE ORIGINAL SCT 20g', 50, 1200, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(36, 'MIGELAS MIE INSTANT SEDUH PROTEVIT BASO SAPI PCK 28g', 120, 1000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(37, 'MIGELAS MIE INSTANT SEDUH PROTEVIT KARI AYAM PCK 28g', 120, 1000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(38, 'MULTI FACIAL TISSUE MP-01/250\'S NON PARFUMED BAG', 5, 18000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(39, 'POCARI SWEAT MINUMAN ISOTONIK BTL 350mL', 10, 7000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(40, 'POP MIE MI INSTAN AYAM CUP 75g', 120, 5000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(41, 'POP MIE MI INSTAN KARI AYAM CUP 75g', 7, 5000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(42, 'POP MIE MI INSTAN SOTO AYAM CUP 75g', 7, 5000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(43, 'QTELA KERIPIK SINGKONG ORIGINAL PCK 60g', 10, 7000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(44, 'ROMA BISCUIT (NEW) KELAPA PCK 300g', 280, 10000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(45, 'SO KLIN SOFTERGENT POWDER PINK COTTON BAG 770/800g', 3, 21000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(46, 'STELLA AIR FRESHENER ALL IN ONE APPLE PCK 70/42g', 5, 12000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(47, 'STELLA AIR FRESHENER ALL IN ONE CAFFE LATTE PCH 42g', 5, 11000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(48, 'STELLA AIR FRESHENER ALL IN ONE LEMON / LEMON FRESH PCK 70/42g', 10, 10000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11'),
(49, 'STELLA AIR FRESHENER ALL IN ONE ORNGE / ORANGE TWIST PCK 70/42g', 10, 10000, 1, '2021-10-24 10:47:11', '2021-10-24 10:47:11');

-- --------------------------------------------------------

--
-- Table structure for table `jdihs`
--

CREATE TABLE `jdihs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jdihs`
--

INSERT INTO `jdihs` (`id`, `name`, `file`, `createdAt`, `updatedAt`) VALUES
(1, 'test', 'korpstar.png', '2021-10-17 06:48:19', '2021-10-17 06:48:19');

-- --------------------------------------------------------

--
-- Table structure for table `locuses`
--

CREATE TABLE `locuses` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locuses`
--

INSERT INTO `locuses` (`id`, `name`) VALUES
(2, 'Kampus Bojongsari'),
(1, 'Kampus Ciseeng');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `itemId` int(11) DEFAULT NULL,
  `transactionRecordId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `researchs`
--

CREATE TABLE `researchs` (
  `id` int(11) NOT NULL,
  `researcher` varchar(255) NOT NULL,
  `conference` varchar(255) NOT NULL,
  `paper` varchar(255) NOT NULL,
  `date` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `researchs`
--

INSERT INTO `researchs` (`id`, `researcher`, `conference`, `paper`, `date`, `location`) VALUES
(1, 'Muhammad Rakha Rafi Bayhaqi', 'The 16th International Conference on Quality in Research 2019 (QIR)', 'Correcting Block Attack on Reduced NEEVA', '22-24 July 2019', 'Sumatera Barat'),
(2, 'Mohammad Heading Nor Ilah', 'The 16th International Conference on Quality in Research 2019 (QIR)', 'Collision Attack On 4 Secure PGV Hash Function Schemes based on 4-Round PRESENT-80 with Iterative Differential Approach', '22-24 July 2019', 'Sumatera Barat');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(3, 'COIN ADMIN'),
(2, 'COMMERCE ADMIN'),
(7, 'CTF ADMIN'),
(1, 'FASTING ADMIN'),
(4, 'FOUL ADMIN'),
(6, 'JDIH ADMIN'),
(5, 'RESEARCH ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `number` varchar(255) DEFAULT NULL,
  `buildingId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `number`, `buildingId`) VALUES
(1, '101', 1),
(2, '102', 1),
(3, '103', 1),
(4, '104', 1),
(5, '105', 1),
(6, '106', 1),
(7, '201', 1),
(8, '202', 1),
(9, '203', 1),
(10, '204', 1),
(11, '205', 1),
(12, '206', 1),
(13, '207', 1),
(14, '208', 1),
(15, '209', 1),
(16, '210', 1),
(17, '211', 1),
(18, '212', 1),
(19, '213', 1),
(20, '214', 1),
(21, '215', 1),
(22, '216', 1),
(23, '217', 1),
(24, '218', 1),
(25, '219', 1),
(26, '220', 1),
(27, '301', 1),
(28, '302', 1),
(29, '303', 1),
(30, '304', 1),
(31, '305', 1),
(32, '306', 1),
(33, '307', 1),
(34, '308', 1),
(35, '309', 1),
(36, '310', 1),
(37, '311', 1),
(38, '312', 1),
(39, '313', 1),
(40, '314', 1),
(41, '315', 1),
(42, '316', 1),
(43, '317', 1),
(44, '318', 1),
(45, '319', 1),
(46, '320', 1),
(47, '401', 1),
(48, '402', 1),
(49, '403', 1),
(50, '404', 1),
(51, '405', 1),
(52, '406', 1),
(53, '407', 1),
(54, '408', 1),
(55, '409', 1),
(56, '410', 1),
(57, '411', 1),
(58, '412', 1),
(59, '413', 1),
(60, '414', 1),
(61, '415', 1),
(62, '416', 1),
(63, '417', 1),
(64, '418', 1),
(65, '419', 1),
(66, '420', 1),
(67, '501', 1),
(68, '502', 1),
(69, '503', 1),
(70, '504', 1),
(71, '505', 1),
(72, '506', 1),
(73, '507', 1),
(74, '508', 1),
(75, '509', 1),
(76, '510', 1),
(77, '511', 1),
(78, '512', 1),
(79, '513', 1),
(80, '514', 1),
(81, '515', 1),
(82, '516', 1),
(83, '517', 1),
(84, '518', 1),
(85, '519', 1),
(86, '520', 1),
(87, '101', 2),
(88, '102', 2),
(89, '103', 2),
(90, '104', 2),
(91, '105', 2),
(92, '106', 2),
(93, '201', 2),
(94, '202', 2),
(95, '203', 2),
(96, '204', 2),
(97, '205', 2),
(98, '206', 2),
(99, '207', 2),
(100, '208', 2),
(101, '209', 2),
(102, '210', 2),
(103, '211', 2),
(104, '212', 2),
(105, '213', 2),
(106, '214', 2),
(107, '215', 2),
(108, '216', 2),
(109, '217', 2),
(110, '218', 2),
(111, '219', 2),
(112, '220', 2),
(113, '301', 2),
(114, '302', 2),
(115, '303', 2),
(116, '304', 2),
(117, '305', 2),
(118, '306', 2),
(119, '307', 2),
(120, '308', 2),
(121, '309', 2),
(122, '310', 2),
(123, '311', 2),
(124, '312', 2),
(125, '313', 2),
(126, '314', 2),
(127, '315', 2),
(128, '316', 2),
(129, '317', 2),
(130, '318', 2),
(131, '319', 2),
(132, '320', 2),
(133, '401', 2),
(134, '402', 2),
(135, '403', 2),
(136, '404', 2),
(137, '405', 2),
(138, '406', 2),
(139, '407', 2),
(140, '408', 2),
(141, '409', 2),
(142, '410', 2),
(143, '411', 2),
(144, '412', 2),
(145, '413', 2),
(146, '414', 2),
(147, '415', 2),
(148, '416', 2),
(149, '417', 2),
(150, '418', 2),
(151, '419', 2),
(152, '420', 2),
(153, '501', 2),
(154, '502', 2),
(155, '503', 2),
(156, '504', 2),
(157, '505', 2),
(158, '506', 2),
(159, '507', 2),
(160, '508', 2),
(161, '509', 2),
(162, '510', 2),
(163, '511', 2),
(164, '512', 2),
(165, '513', 2),
(166, '514', 2),
(167, '515', 2),
(168, '516', 2),
(169, '517', 2),
(170, '518', 2),
(171, '519', 2),
(172, '520', 2),
(173, '101', 3),
(174, '102', 3),
(175, '103', 3),
(176, '104', 3),
(177, '105', 3),
(178, '106', 3),
(179, '201', 3),
(180, '202', 3),
(181, '203', 3),
(182, '204', 3),
(183, '205', 3),
(184, '206', 3),
(185, '207', 3),
(186, '208', 3),
(187, '209', 3),
(188, '210', 3),
(189, '211', 3),
(190, '212', 3),
(191, '213', 3),
(192, '214', 3),
(193, '215', 3),
(194, '216', 3),
(195, '217', 3),
(196, '218', 3),
(197, '219', 3),
(198, '220', 3),
(199, '301', 3),
(200, '302', 3),
(201, '303', 3),
(202, '304', 3),
(203, '305', 3),
(204, '306', 3),
(205, '307', 3),
(206, '308', 3),
(207, '309', 3),
(208, '310', 3),
(209, '311', 3),
(210, '312', 3),
(211, '313', 3),
(212, '314', 3),
(213, '315', 3),
(214, '316', 3),
(215, '317', 3),
(216, '318', 3),
(217, '319', 3),
(218, '320', 3),
(219, '401', 3),
(220, '402', 3),
(221, '403', 3),
(222, '404', 3),
(223, '405', 3),
(224, '406', 3),
(225, '407', 3),
(226, '408', 3),
(227, '409', 3),
(228, '410', 3),
(229, '411', 3),
(230, '412', 3),
(231, '413', 3),
(232, '414', 3),
(233, '415', 3),
(234, '416', 3),
(235, '417', 3),
(236, '418', 3),
(237, '419', 3),
(238, '420', 3),
(239, '501', 3),
(240, '502', 3),
(241, '503', 3),
(242, '504', 3),
(243, '505', 3),
(244, '506', 3),
(245, '507', 3),
(246, '508', 3),
(247, '509', 3),
(248, '510', 3),
(249, '511', 3),
(250, '512', 3),
(251, '513', 3),
(252, '514', 3),
(253, '515', 3),
(254, '516', 3),
(255, '517', 3),
(256, '518', 3),
(257, '519', 3),
(258, '520', 3),
(259, '101', 4),
(260, '102', 4),
(261, '103', 4),
(262, '104', 4),
(263, '105', 4),
(264, '106', 4),
(265, '107', 4),
(266, '108', 4),
(267, '109', 4),
(268, '110', 4),
(269, '111', 4),
(270, '112', 4),
(271, '113', 4),
(272, '114', 4),
(273, '115', 4),
(274, '116', 4),
(275, '117', 4),
(276, '118', 4),
(277, '119', 4),
(278, '120', 4),
(279, '121', 4),
(280, '122', 4),
(281, '201', 4),
(282, '202', 4),
(283, '203', 4),
(284, '204', 4),
(285, '205', 4),
(286, '206', 4),
(287, '207', 4),
(288, '208', 4),
(289, '209', 4),
(290, '210', 4),
(291, '211', 4),
(292, '212', 4),
(293, '213', 4),
(294, '214', 4),
(295, '215', 4),
(296, '216', 4),
(297, '217', 4),
(298, '218', 4),
(299, '219', 4),
(300, '220', 4),
(301, '221', 4),
(302, '222', 4),
(303, '201', 5),
(304, '202', 5),
(305, '203', 5),
(306, '204', 5),
(307, '205', 5),
(308, '206', 5),
(309, '207', 5),
(310, '208', 5),
(311, '301', 5),
(312, '302', 5),
(313, '303', 5),
(314, '304', 5),
(315, '305', 5),
(316, '306', 5),
(317, '307', 5),
(318, '308', 5),
(319, '401', 5),
(320, '402', 5),
(321, '403', 5),
(322, '404', 5),
(323, '405', 5),
(324, '406', 5),
(325, '407', 5),
(326, '408', 5),
(327, '301', 6),
(328, '302', 6),
(329, '303', 6),
(330, '304', 6),
(331, '201', 7),
(332, '202', 7),
(333, '203', 7),
(334, '204', 7),
(335, '205', 7),
(336, '206', 7),
(337, '207', 7),
(338, '208', 7),
(339, '209', 7),
(340, '210', 7),
(341, '211', 7),
(342, '212', 7),
(343, '213', 7),
(344, '301', 7),
(345, '302', 7),
(346, '303', 7),
(347, '304', 7),
(348, '305', 7),
(349, '306', 7),
(350, '307', 7),
(351, '308', 7),
(352, '309', 7),
(353, '310', 7),
(354, '311', 7),
(355, '312', 7),
(356, '313', 7),
(357, '401', 7),
(358, '402', 7),
(359, '403', 7),
(360, '404', 7),
(361, '405', 7),
(362, '406', 7),
(363, '407', 7),
(364, '408', 7),
(365, '409', 7),
(366, '410', 7),
(367, '411', 7),
(368, '412', 7),
(369, '413', 7),
(370, '501', 7),
(371, '502', 7),
(372, '503', 7),
(373, '504', 7),
(374, '505', 7),
(375, '506', 7),
(376, '507', 7),
(377, '508', 7),
(378, '509', 7),
(379, '510', 7),
(380, '511', 7),
(381, '512', 7),
(382, '513', 7),
(383, '601', 7),
(384, '602', 7),
(385, '603', 7),
(386, '604', 7),
(387, '605', 7),
(388, '606', 7),
(389, '607', 7),
(390, '608', 7),
(391, '609', 7),
(392, '610', 7),
(393, '611', 7),
(394, '612', 7),
(395, '613', 7);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_records`
--

CREATE TABLE `transaction_records` (
  `id` int(11) NOT NULL,
  `npm` varchar(255) NOT NULL,
  `value` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `transfer_records`
--

CREATE TABLE `transfer_records` (
  `id` int(11) NOT NULL,
  `targetNpm` varchar(255) NOT NULL,
  `value` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `transactionRecordId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `npm` varchar(255) NOT NULL,
  `fullname` varchar(255) DEFAULT NULL,
  `class` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `roomId` int(11) DEFAULT NULL,
  `coin` int(11) NOT NULL DEFAULT 0,
  `password_digitalsignature` varchar(255) DEFAULT NULL,
  `ctf_point` int(11) NOT NULL DEFAULT 0,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`npm`, `fullname`, `class`, `password`, `createdAt`, `updatedAt`, `roomId`, `coin`, `password_digitalsignature`, `ctf_point`, `title`) VALUES
('1817101369', 'Achmad Husein Noor Faizi', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$MwW1MieZT0oj4Tb59rHe5eo1YG4yP6bt3L0Qd.ndGCZcaZkgVMqHu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MY98G', 0, NULL),
('1817101370', 'Adam Waluyo', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$.879nsctlNrTpFrKmFc2COOJ9w8mxWBPvsOwOBnbKGbY5zlha99UG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'EF58A', 0, NULL),
('1817101371', 'Adenta Rubian Qiyas Syahwidi', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$j3ncNxRWLLOvoo9xOl6ULOGur3RLXMT/hx3Qagt7KFk1gXaxA00di', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VA90V', 0, NULL),
('1817101372', 'Adit Candra Prayuda', 'IV Rekayasa Keamanan Siber Route', '$2a$08$jAB8fGWaegGZtSVkj70ate9q8pfq4fTZOxVVz1MLKWPep5nQrhVVa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'BX86G', 0, NULL),
('1817101373', 'Ageng Angelita Puti Anggini', 'IV Rekayasa Keamanan Siber Route', '$2a$08$Cvmfk7mtJxg5hOnOWYDADuHSB43GrnJtIP3Q0gwO3dZ/0r4o0UeX.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KL34B', 0, NULL),
('1817101374', 'Agung Maulana Putra', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$NJjHZYRjAGP3wvgOqOWEcuOEoVeefP.fcyBgVXbXPFc6xIUEc8IpK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OB75Y', 0, NULL),
('1817101375', 'Aidil Yusuf Priadi', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$XZbrVx56Ry5oYt9xm.S9YeLwsSmTCbpsQzeaJIMh4ENPbUMiDABXC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'NS24X', 0, NULL),
('1817101376', 'Aji Agung Sedayu', 'IV Rekayasa Sistem Kriptografi', '$2a$08$bWDa3ZzCRLwwK7BeyXCNTO2eZZ14qPx/81/EhATeaRbwDJ7QHrhWO', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OI76Y', 0, NULL),
('1817101377', 'Alfian Adi Saputra', 'IV Rekayasa Keamanan Siber Route', '$2a$08$L477l.PGlx5tH1osF5ZzzuLExdoNl1Nk9.kIxdn7Ytd9LRHWflkIO', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'ZI39M', 0, NULL),
('1817101378', 'Aljevon Yusuf Sulaiman', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$bfIJpQrw1pJ3hLYx.9aXJeuOu5ItdIqBr/qP86oi8HFBsbkwKEYZa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'EH12G', 0, NULL),
('1817101379', 'Alya Aiman Salsabila Arif', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$r/8u/hEpEFSZMmN.J1fK3uetzdL9k65m0gpibdUySKt2R7shz.fCS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'PM16D', 0, NULL),
('1817101380', 'Amerta Bian Kretarta', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$dn.j0mH5HOFsIMrP1Qd38eowofnGpLb22owo0ObFkK7GGVnyttilG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'NY86Z', 0, NULL),
('1817101381', 'Andhika Sigit Juliyanto', 'IV Rekayasa Keamanan Siber Route', '$2a$08$3IRMPin3mTDHf.cUqFCVCePNkxc4qKqTDR6W0hX2KRNn6gOfzXHoO', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AQ18P', 0, NULL),
('1817101382', 'Anjeli Lutfiani', 'IV Rekayasa Sistem Kriptografi', '$2a$08$weN5MEjebFC1ZJLmct.ApuFnWJb2mcR/oas60smYpTB07GI2P8KKa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AM51D', 0, NULL),
('1817101383', 'Antonius Alfari', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$vVE.cPbFwb/2SHP139zXy..Wv.UKFeG38CkMMS9TlbCMCBQ59QcPu', '2021-10-10 08:10:56', '2021-10-22 16:03:51', NULL, 0, 'KC66I', 0, NULL),
('1817101384', 'Aprizal Sahulecha Simanjuntak', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$Lgw6HCRDYZkindKVNLldjOBbSbwF2jHPD.Sb5.mumXzbNwcUFxuaS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'SN55C', 0, NULL),
('1817101385', 'Arbain Nur Prasetyo', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$qtpB317LzfegGktujCAy7uQLTsWa8ufxeZk7bqeV1Gq6x.EKmMwXS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'RS42G', 0, NULL),
('1817101386', 'Arni Yulia', 'IV Rekayasa Sistem Kriptografi', '$2a$08$sPZNlLD5dDHQFaJFQvSxzeZHfhYCDaXjuG.H/7pPESGpJb3CMPo3a', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'EB44N', 0, NULL),
('1817101387', 'Arsya Dyani Azzahra', 'IV Rekayasa Sistem Kriptografi', '$2a$08$/JaqVACm.Iv6y7neoBmGYu0ROI3m7.VXGEyNDT8eA8aomZ1ghezoK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GA39S', 0, NULL),
('1817101388', 'Asep Dadan Rifansyah', 'IV Rekayasa Keamanan Siber Route', '$2a$08$SqyuAxXI8r79F0KZOJbHPOOO4p1BdRA2iBTfoeCuCP0O3iSedovju', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'QC21Z', 0, NULL),
('1817101389', 'Asyraffi Adnil Ma\'ali', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$hzOU.IgQT24G8EtBMQAXlOEvtj6YQQwPmkH/wkEpeV/w8SmFJy62y', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CI41G', 0, NULL),
('1817101390', 'Atika Nurliana', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$wbD.VFLC8wsSAgSV3r9iOerTOxIq7GXG4w.X5JrBFtsMvIsSVVGO2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OS90Z', 0, NULL),
('1817101391', 'Balqis Tuffahati Permana', 'IV Rekayasa Sistem Kriptografi', '$2a$08$T06IF8h/wQSZ.Yn2WAuzAOlvBa94NJ2cl96s83o6ZhzarkMLkoagi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AN83F', 0, NULL),
('1817101392', 'Bernawan Ikhsan Syahputra', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$851m2ezR.xSHRMR0kQQZZeMDoVPfvpseHH0yz/GZVHM1RWgvLbOxG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IX97P', 0, NULL),
('1817101393', 'Bimo Setyo Husodo', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$22X0pTsnf.FKWNfzc1jlA.qydK59mZsC81IGxnt.1xpL7g6fOzcJ6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OO86X', 0, NULL),
('1817101394', 'Daffa Akbar Putra Yusa', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$p7ZGmqI.8y3V7EB6KUVDgOHFE5QLgdjQSXet2M4r5WKIT9oAtpeda', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'ZM30S', 0, NULL),
('1817101395', 'Dendi Risman Saputra', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$rBd9zMKFDwnkNT3VyPCcyetXTz6QiOgk1rMcdtKEOZ8FfqLF1AWH2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'FP25Y', 0, NULL),
('1817101396', 'Dewi Kartika Prasasti', 'IV Rekayasa Sistem Kriptografi', '$2a$08$ae30z4t0/bIBAIIcL8JFbuCLPTTYd1tgWJ78u61O8tu9qlo4iMCX.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DG81J', 0, NULL),
('1817101397', 'Dhea Septi Kurnia Dati', 'IV Rekayasa Sistem Kriptografi', '$2a$08$KmRl1gQGDUTgzSMsuvN.eubsXaMqQbYIVymJJISRA5F.sXt85Bv46', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KC98T', 0, NULL),
('1817101399', 'Dikka Aditya Satria Wibawa', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$zjq66WQlO3aCylchWG1.FuOqb9061pGr25U1binj4p3YD2MLhknDi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'EH96P', 0, NULL),
('1817101400', 'Donny Irwansyah', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$D24FS1AcG1pY5DXVZRggu.t/6yG6j9F3MKcCZ0ftzHA2XrJF8vbKS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CA17F', 0, NULL),
('1817101401', 'Ega Bagus Wibowo', 'IV Rekayasa Keamanan Siber Route', '$2a$08$tvSa.Ih2PzdLBmvT8F/.o.3HTbTwxLvBih2sD.GuJDJP5xZwr96ou', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'PD91E', 0, NULL),
('1817101402', 'Emmanuelle Sibarani', 'IV Rekayasa Sistem Kriptografi', '$2a$08$wK5yPjiWEwJLIqh3882GkOYmYPAFhxQ/Xv4aYvkLbkSgRPNZxp/TC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AAAAA', 0, NULL),
('1817101403', 'Fadhil Raditya', 'IV Rekayasa Keamanan Siber Route', '$2a$08$zqhsCJVa0O8CpPhITVlFVezybZDmEBwm.J6hRiW8JVek/7UUHvTUW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DK52I', 0, NULL),
('1817101404', 'Fahdel Achmad Purnama Putra', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$87WQ8Z2W/vj7urAMvMNwze/0K7Q8e.j74qahohcpDNTXxuZIgkCAy', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HW94D', 0, NULL),
('1817101405', 'Fahma Dieny Sumardi', 'IV Rekayasa Sistem Kriptografi', '$2a$08$0dJKEafU2IS6qXblPrUcDeOXZbt7O5fkrD9F6UAfjObGRVaNthchC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'PY45J', 0, NULL),
('1817101406', 'Farid Akram', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$gzkRngHRC2jci3wHtcpWWuV9Se2QHHPLdoDm6LAfxFQr2bW.6gHMu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DE12N', 0, NULL),
('1817101407', 'Farras Ahmad Naufal', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$PiI4LuHCQ90ukkkKMNy9q.jNt2T8rH9kGTXnnbJxmGm1gT0VZFvYC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'UR32Q', 0, NULL),
('1817101408', 'Fela Nadya Sari', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$3FE1fAsErqP5t.EZ1KI9reJqPplGc9MZXJ1tiHHngffgdyXFpQcBi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YK28K', 0, NULL),
('1817101409', 'Fendy Heryanto', 'IV Rekayasa Sistem Kriptografi', '$2a$08$fnEMSrwBOBzqA9U2fWUo9uPSEuqh4RckCNwE1no/x5sj5r/zn9a7e', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IF46B', 0, NULL),
('1817101410', 'Fika Dwi Rahmawati', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$lKcDmaf.xqTqnuhQb3JHqOMwyxFV59qmffgx1clytAGES3GZ2qihS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'PR33P', 0, NULL),
('1817101411', 'Fitra Hutomo', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$BlxlMYZ5fmAZC3bZ4V4.X.A9baDYooodHVbP.Rd915aY/FoZwbttC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LO13X', 0, NULL),
('1817101412', 'Galang Putra Nusantara', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$di9YFODhPZUgEeqJgO/F4uTInA5YdPvka8ls2Y3XeHfzn08FMpZNO', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XE60D', 0, NULL),
('1817101413', 'Ghiffari Adhe Permana', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$06HSATyaZ8GKdMK/QbwLJuUXh6M2mYcT3PQQUgbNrzgLyGELa/VaS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MG29F', 0, NULL),
('1817101414', 'Hafizh Ghozie Afiansyah', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$cZxss0Ad3HrErhiIX25pqebV7aFtEGtBdOfXXT1pZXNMwmMETlBkq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VR17O', 0, NULL),
('1817101415', 'Handhika Yanuar Pratama', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$S/BlZZhWWNkP9ScEcUnfEuFFZjKgnfqaleruHfANADf5KAjoTsM.W', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YJ32G', 0, NULL),
('1817101416', 'Hanifah Salsabila', 'IV Rekayasa Keamanan Siber Route', '$2a$08$BkaLrMAA2XjN6QIH.HI/z.ZrHW8X6yABJRwomK0opszlRNzTKz0gK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'BA56H', 0, NULL),
('1817101417', 'Hernowo Adi Nugroho', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$YUF3xu2KdKmxRcjQ36FSWOA5KzdrkwbU9vH1kiVE.CONeXqXwgnzW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'BN39J', 0, NULL),
('1817101418', 'I Wayan Ari Marjaya', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$rS6lzF1l5ZqhxidohdCqPO4R5BzJSTLk9HvB3NJJMs3gkvkPjS66S', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VA90R', 0, NULL),
('1817101420', 'Kamila Rizqina', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$xon8gYF3ymVEd6DO5/awVOFIBoAZdPUTXC/yj7i4BDht3PnmtEPDm', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CN79C', 0, NULL),
('1817101421', 'Khaerunnisa', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$5.R2uhhSxSDjvaJhZ9sAEelMFNKmN/1mIwlnWzHm3bjNqNFAFtPmS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JH83P', 0, NULL),
('1817101422', 'Kirana Larasati Dewi', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$KDu7NlNuc4nkCoBhXH0C2OaOnxlg5khz6V4el9sYthSsQElbpdWC.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'QJ88L', 0, NULL),
('1817101423', 'Latisha Safa Salsabilla Kirana', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$tcf.mfOMdlY2WMlibb62IeWSOZmC7K36Ly939s45E8NcXxJP6izUG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YR14W', 0, NULL),
('1817101424', 'Lisa Saputri', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$3SJslrBizV5ch21UI0AbWO.sZI0a00LP42rLTNFXK.VWQoRj0FbR2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LO12P', 0, NULL),
('1817101425', 'M Jaya Hadi Kusuma', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$dkhSFzbiz04LCXRL2A6WKeMl5FM/4OcG8TVzC6tVBi8QKi3plfbrq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'QX39P', 0, NULL),
('1817101427', 'Mahar Surya Malacca', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$v.IM/G49iNGpLzHqEVp4ruko.Tx/WVirc8IUHuDUJD1dZJdjuF2pG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'FP19H', 0, NULL),
('1817101428', 'Maulana Ihsan', 'IV Rekayasa Sistem Kriptografi', '$2a$08$QTbVqlVQwDSUdwN7YdsU2Oj1o5QlMm.uNB9ONvaS5DLBlfwRvcxw.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HW80E', 0, NULL),
('1817101429', 'Melandy Andriawan', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$WL8FEWV7F32AXmEscDtaROdKg08BIVEaaFYQxdUSbNEFCbo1fPtem', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'BI13V', 0, NULL),
('1817101430', 'Meta Mutia Permata Sari', 'IV Rekayasa Sistem Kriptografi', '$2a$08$3eHNY2LNE3aYYXSjrGCH7.vogr0mvPGfYAJak.J/cEynm3f2zWYn6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'FP13D', 0, NULL),
('1817101431', 'Muhamad Qolby Fawzan', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$Sm8i2ZNcseBEvrvmrQiqtOzHM/coblrwFaFaqicIKrFrKgDlv0cgu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AK14B', 0, NULL),
('1817101432', 'Muhamad Saleh', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$9VNnA2ZyWgrqOjWWjB7IueDN9qIy0K49QJlW1goiNH5TVe3E7IxKa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MK67D', 0, NULL),
('1817101434', 'Muhammad Fadilah Akbar Al Rosyid', 'IV Rekayasa Keamanan Siber Route', '$2a$08$a7Wt2262u6LF/AD/WcP8RuIWsRRWzW9oFwjEnfRgh09ruyPqP0lHG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'SI81Y', 0, NULL),
('1817101435', 'Muhammad Fahmi Ramadhani', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$a4a0/8yQl0WiVezBPup6h.xH3xJZC.JNvGnxX0pu5SnKsxmyJitGu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VL55Q', 0, NULL),
('1817101436', 'Muhammad Hadi', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$cKqhcK3yn3q0E.7omRP66uZTruOS784BqTG9vUPgq7FOv.v0j5csO', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AC17W', 0, NULL),
('1817101437', 'Muhammad Hilmi Amanullah', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$tBqdATGpAC./kNAD.PoUU.DsTaXtVA.L0lqFfj4Yy9S74ZqZ/..f6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HC29H', 0, NULL),
('1817101438', 'Muhammad Irfan Cahyanto', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$FeYhW0POQPXf3Yme9u.JUOmIsGPvQfwO0MN6tjOLcNKusN1zW5REG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AY91W', 0, NULL),
('1817101439', 'Muhammad Luthfi Rizki Chusam', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$8kkOhhYYwk7dWn2QzSK2UuODzcAxJZ3ykgx2ZlecvVxasT9dJjO9.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DW54C', 0, NULL),
('1817101440', 'Muhammad Novrizal Ghiffari', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$T4Y91XVkjmp/aeAdIz2rAu.w7U32xDQnIriz5eaXYwQyh/zrf4hFq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DC47M', 0, NULL),
('1817101441', 'Muhammad Rohmanur Rizqi', 'IV Rekayasa Keamanan Siber Route', '$2a$08$De5URd5E8k5ZugvbBPwgWui0o6nY6zI.hjSq56r7bkyQ2XJFkWtKi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'QH28F', 0, NULL),
('1817101442', 'Muhammad Syihaab Al-Faaruuq', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$qjPXFTbh4RgTyMjQWIMlZusjijwgNdE.ngQ5vnhPUXWvjQH90wT5W', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YO89S', 0, NULL),
('1817101443', 'Muhammad Zhilal Agrayasa', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$13FxPoHWBgO6IRFSfvnE2eSVWFvLDe1mF9qfcQpUmJdBKEU4vYGGa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OW20Z', 0, NULL),
('1817101444', 'Nafila Laili Ikrimah', 'IV Rekayasa Sistem Kriptografi', '$2a$08$PG9PuCHJSuwsgOMuEjCqrefqyiZhmzyx7NjhPSWYZ/9L4jAjgubKu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VZ63P', 0, NULL),
('1817101445', 'Naufal Hafiz', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$Khes0DVmq7g3cHQyPC7CsOBlQ8RCAYDwxc0HUNEbBhrvDcIhCJJVa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'NF89V', 0, NULL),
('1817101446', 'Naufal Hafiz Syahidan', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$tLeXOAAhik0/aHb9HN23s.7updHDTrMiaz8yShBApKD4yosyWIWq6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'QG85C', 0, NULL),
('1817101447', 'Naufal Madega Pratama', 'IV Rekayasa Keamanan Siber Route', '$2a$08$QFDnN2nYSx/uDzSMbmziQOIt1juftDGL6wMENrS8dGhPnfci5.YGq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'FV81H', 0, NULL),
('1817101448', 'Nazela Khairani Putri', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$6smgqlJ4//poRjVLed4AseUyB2.kFuFjD.NI32o4a.EgQ7BHdib92', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JN98T', 0, NULL),
('1817101449', 'Nizam Aditya Zuhayr', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$uACHk0HeywmAqlaQofDjwOtiqDXH.JP05zaih3eJypWxD3wisSGTu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'PF72K', 0, NULL),
('1817101450', 'Nofrisal Dwi Syahputra', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$emw9pv9KPvTsX4bRpMNWJeSqE/OyD1i9PNijmXK5W3DUgmejUoGG.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'ZE77F', 0, NULL),
('1817101451', 'Noni Fauziah Septianty', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$iQeBwmUWIAT5TZoyYVs5Xe.Lq7lY.1N79EQ2fnw3PjJsNeV5Oz1Pu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KE84W', 0, NULL),
('1817101452', 'Rakha Nadhifa Harmana', 'IV Rekayasa Keamanan Siber Route', '$2a$08$8DYW4iFnuT/o7asfu6OutuvVOFIf8Eaqsg2pAWJURl7tisTXUdbO.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OC65V', 0, NULL),
('1817101453', 'Regina Christanty Beatrix Hadjo', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$579q3q6nxjcuhPcF1V7/6eyyOXo.iuW4yWeZnfZChmMxGazUBgKWW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YA49S', 0, NULL),
('1817101454', 'Ridho Mauldiansyah', 'IV Rekayasa Keamanan Siber Route', '$2a$08$p9cJLQpAeE4YXMwhtv9r0Osw7TsZXstTTvbw7JesYL5ixeBgU7PXy', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'QK53F', 0, NULL),
('1817101455', 'Rizaldi Wahaz', 'IV Rekayasa Keamanan Siber Route', '$2a$08$ttv0KhngmKOe.pgyOVQ0qOotjAYptadWRMye7t1wJUokefwDJvt7y', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'PG13Q', 0, NULL),
('1817101456', 'Rizky Ainur Rofiq', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$RwlAORy.njZiMTs5WD6houwHpzyUHpqbFsUpI36SpkJF8YK2IBzC.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'TD49B', 0, NULL),
('1817101457', 'Saffana Tistiyani', 'IV Rekayasa Keamanan Siber Trace', '$2a$08$tAQ7SBMwd92SBrkmANolB.07k9.HCiMUJTX976GvR870Y8BPF27Ni', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XG82V', 0, NULL),
('1817101458', 'Salsa Alma\'ariz', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$Z2KMRDn6iEZcvnO.QqdAJ.8VznKhuErFzbQh8Xfbb5AqV4dPI7/rq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JO18X', 0, NULL),
('1817101459', 'Sari Putri Yanti', 'IV Rekayasa Keamanan Siber Route', '$2a$08$kCUC51GlnCKm64.tL9reWOdQcAqknWa4Gu2vkZeXh3O6T9R8pix2i', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VC36P', 0, NULL),
('1817101460', 'Sekar Mutiara', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$KUhLf1OBHQl4kl/fMu3yjuXnCzHkMsd4wNv1LFDeIazxAzkEmjIMi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AU53W', 0, NULL),
('1817101461', 'Shafira Dinda Ramadhini', 'IV Rekayasa Keamanan Siber Route', '$2a$08$Qf1cuYKYhTcVu4Soxq7Teu7vHVFW4BKBlLphrlR5XxuAdQ/NhMw3e', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'SX32F', 0, NULL),
('1817101462', 'Tri Hesti Damayanti', 'IV Rekayasa Keamanan Siber Route', '$2a$08$sHdVoVEaQkvlX4cnspNOO.IKIJmBs/9E2b6a6hTQTaOaB2LjgjGQm', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'WU24L', 0, NULL),
('1817101463', 'Wahyu Riski Aulia Putra', 'IV Rekayasa Keamanan Siber Route', '$2a$08$40nJ6vzNNkJsufmCPo2aq.ieD0fUP2HTnC6x8OyFMJfwTGda1IwaS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VW10D', 0, NULL),
('1817101464', 'Whisnu Yudha Aditama', 'IV Rekayasa Keamanan Siber Route', '$2a$08$YnHVj.AEDqlCUoh5U8eHqe4obhJHT05HjzvT3RI1m3UkNVEfUBJOa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'TA61N', 0, NULL),
('1817101465', 'Wisnu Irawan', 'IV Rekayasa Keamanan Siber Echo', '$2a$08$h2oI6EFFVzYxesZWfhMUG.Jc8kvXkLIMJXcJYboqzS9aDkVLqhDZ2', '2021-10-10 08:10:56', '2021-10-23 20:05:13', 93, 850000, 'WI32C', 0, NULL),
('1817101466', 'Zaidan Nuraga Chuldun', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$5IatM9Mg/YQ8K0XYXBkPhO9BZcfLzDvP.mitxcZ50KVOGvl.LRdHe', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JK47P', 0, NULL),
('1817101467', 'Zathalinny', 'IV Rekayasa Perangkat Keras Kriptografi Quantum', '$2a$08$Edy2VRrJdT/eP23OxOovae/P3BC8.DtaC62LSZftHqIqC57ZwnB2W', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'NL40A', 0, NULL),
('1817101468', 'Zidna Wildan Alfain', 'IV Rekayasa Perangkat Lunak Kripto', '$2a$08$uNgvTvDZ97.fQAzd68giN.2ai1hXKUH.Qp7ee.mrTU6Sik8oqsl8O', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AW65N', 0, NULL),
('1918101426', 'Abidah Salsabila Putri Sanita', 'III Rekayasa Keamanan Siber Python', '$2a$08$6CFxdUWFxhxYggeC5oarZOdgSLMLgv.2S28ApLiIqnwl5gxlSg7im', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KP99B', 0, NULL),
('1918101469', 'Adek Muhammad Zulkham Ristiawan Kertanegara', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$g09uSAvusKRcXpf2KmPoeuxlI1ojk/2o8kIPjW.z86wJJYbj1VMv6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KP82M', 0, NULL),
('1918101470', 'Adelia Putri Andriyani', 'III Rekayasa Keamanan Siber Python', '$2a$08$ETlpaTdikIBc6We6Y6gv6udQHymhFSQ4O6GV.lGKDvd3B5LBO1MFe', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MN80T', 0, NULL),
('1918101471', 'Adisha Hanifa Ewata', 'III Rekayasa Sistem Kriptografi', '$2a$08$qP.k/iisljkKg4MsDgDiT.ydpNo6Q2RKIyBne90S7wEZ/rfSffufe', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GN72J', 0, NULL),
('1918101472', 'Adiva Fiqri Nugraha', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$zWMKjhgtKwYcHWtWuV0uNuYhdN2sysPVMVvbHmh1bzNd6sSz.2RLO', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'EU29H', 0, NULL),
('1918101473', 'Akhmad Rizal Arifudin', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$mxk8GehL5N15yxhjDlef1eJ1XToNfjQRfj2hiCtv7R8nLWTxULI8y', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IV90G', 0, NULL),
('1918101474', 'Aldi Cahya Fajar Nugraha', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$eQcwLV0pZNE8jGHf5vIasuCl5yxb50S7nzo/CV3V/7OLCwwPpic/y', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XP62N', 0, NULL),
('1918101475', 'Alhafid Azhimullah', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$UtZ1eOrF6PznfxI7wgEKQef7.F9XhKdw4A57hGBzWkqIRZbyBAXpS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GW74F', 0, NULL),
('1918101476', 'Alifah Noor Islami', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$EL0S3CjB1hmZmdwmDbI8TO1.K.k1uYFLGtVHXeJXRY8PCaU91HDE2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'ZD69X', 0, NULL),
('1918101477', 'Almeera Shafa Rayyana', 'III Rekayasa Keamanan Siber Python', '$2a$08$u0QMgZCa.a.zGOhmKBRtnerIJT/3DLXBChr9BVuGIZIwC7EZnS8m6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XL32V', 0, NULL),
('1918101478', 'Alviana Juni Susanti', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$7ykh53gr0kXOXXnFkYiw4eZtdce8ZfZgC4fceADfzDy5MTBK0xspS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GC73W', 0, NULL),
('1918101479', 'Annisa Aulia Budianti Qurota\'aini', 'III Rekayasa Keamanan Siber Python', '$2a$08$EJc5Hkek6EJQwwrzRoVuA.Mj0arABmArLL3SFyx3GE69Kyatx9Uoq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HA92P', 0, NULL),
('1918101480', 'Arbi Nur\'aini Labibah', 'III Rekayasa Sistem Kriptografi', '$2a$08$Fvba/.FelhayIsuL0Ag5rOhZ/cRJJ8bv.eRp4AntqincY/FDk4Wxq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CJ11W', 0, NULL),
('1918101481', 'Arga Prayoga', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$Mmba53i0lxuUq3u72kDz5.YwtxL3oahBbeGbNHrCbK7hxh9FbBXO6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'UR88N', 0, NULL),
('1918101482', 'Arildha Rahma Savitri', 'III Rekayasa Sistem Kriptografi', '$2a$08$0qcYD7FE00M0FJhcUxoVQuTwmcPizdNKpe3iDKAC3d2yuhGo1xfsu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DJ17Q', 0, NULL),
('1918101483', 'Arion Sapril Tamba', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$t2QYiID1QZpNA5cmCPrVxuQh0jqMBoe9.359hQVNM/kUjVwGv5gj2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'QZ89M', 0, NULL),
('1918101484', 'Asep Setiawan', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$FiRlysjRW54zpSeKGddkeOWBNH.4bk.oUNCTh0zjk0iNrP1S2i5c.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JG30V', 0, NULL),
('1918101485', 'Asri Safira Priantana', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$bAtTJPOwJjIMeWz0139Ame9ZIMoYzDtHaT7CM1/EvWpfsCjc/AO16', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'RF49D', 0, NULL),
('1918101486', 'Asti Nuryani', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$sCSfdj/HAJjLa3/PMu3FqOBtNE5tO1eqSxoMG/sVD6uzOmGikBzr2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'UX69W', 0, NULL),
('1918101487', 'Aulia Rachmawati', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$zS7sRLZjj5PMfX5hYFIrfu1exZb2Nv0R/A/gFpVfj15tuijgYJWYa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IO34M', 0, NULL),
('1918101488', 'Aviva Javantio', 'III Rekayasa Keamanan Siber Python', '$2a$08$TDzEEbxCyetHrqJsdZ/EDeM7en0yXBtlf9XOHMmaCNtpujVCHbKX6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JG73N', 0, NULL),
('1918101489', 'Ayu Choiriyah', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$YdXWbK0V/ZSZDU2Ry0fRzuO1fuAOIfHvpZdd/9Huqn9bbN0FId6hu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HV70C', 0, NULL),
('1918101490', 'Ayu Ningtyas Nurfuadah', 'III Rekayasa Keamanan Siber Python', '$2a$08$fGmMYFWOM0KRXUuXOXRqlOOQ0sADwMRL2yx7kN4cdZBVgCOhKsb5y', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GC63G', 0, NULL),
('1918101491', 'Aziiza Ratnadewati Pratama', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$bEwJl5tCHlg4dZ0a7l69aelTb.LlAe4ZQ3kgF4nE5.Oxc2fN2MXn6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LE28L', 0, NULL),
('1918101492', 'Bagas Megantoro', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$x0fczx/QtmWaOTmx7k7Cs.EBj6hDmIyvpesTUp0LH055mPqnxYw0i', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'NC10Y', 0, NULL),
('1918101493', 'Bagus Pribadi', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$VBoKT5R0EKB7sEDAK2iAbuE.auFdHrkeeVoFMrWiKvVaBv0mNTQ3u', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OC93P', 0, NULL),
('1918101494', 'Bahrianto Prakoso', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$Z8SVzDX5KmICVGFjU4K5aeKgvIWythXxk.VUS74XXgHlxIMfNqb9G', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HX10I', 0, NULL),
('1918101495', 'Cantigi Kharisma Khairanisa', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$OiPYM0Q2lCo4DXv77s.lGufx6QD3NqSCM0lhttFnIJKGiz65C24HC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IC13S', 0, NULL),
('1918101496', 'Dhana Arvina Alwan', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$gnP3KROLwi4dLhONRq3KVeRnKHkeYCDa1EZhhNy29nrDQhoQQ/n.2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XU51B', 0, NULL),
('1918101497', 'Dian Dwi Karlina', 'III Rekayasa Sistem Kriptografi', '$2a$08$ipV9.osgCCcPX8SD3dVhMevdqfruQn4lJSxWL2/71BOXT1rXNMl2q', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JN24Q', 0, NULL),
('1918101498', 'Diky Artin Sitanggang', 'III Rekayasa Keamanan Siber Python', '$2a$08$9498e15znjtHEp1eZzz/k.Mk.DDnFVQF/oVG3bagDxKxOJ/HjEh3m', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MP32S', 0, NULL),
('1918101499', 'Dyah Rahmatannisa Zenina Alfiandini', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$c38l33F5Aby0O3CVcOup6.yfWA.dXdhJOaJ.xooyvcWb42Nk5UBUq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AY27T', 0, NULL),
('1918101500', 'El Fitrah Firdaus Fadhilah Akbar', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$Q4fidmStJui0BaQWxOO2..nFmmezxQRJpH0V6VpIjI45hxBuCEK4i', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JV97A', 0, NULL),
('1918101501', 'Ellisa Hani Nur Safitri', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$Y8R1fZvqLNfUq/AxsfZ8VOhSY8QKrqXj2MdycxTA/c9QZO1YPMs06', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'WB13T', 0, NULL),
('1918101502', 'Fahmi Ramadan', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$x6qNCDkEJAfZ94KQd7qy9ef.eXs1E.NgyfQwqFUZqtXFCW7HyAhHK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KB40G', 0, NULL),
('1918101503', 'Faris Firana Febrian', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$rWgZhCfypPrUEB2xTglH6euKIDqCV6lbpeAZHyhDA2/Pu.73M9EYy', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GH94S', 0, NULL),
('1918101504', 'Fathurrahman Rifqi Azzami', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$mO7WNczqnGjYtIBMh5lBUepQlYw5waCuLm5MHkzlmrbvlu3/ZCUYq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CM90J', 0, NULL),
('1918101505', 'Fatikho Kautsar', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$a3q88YxkMzJOwfIIu0pcgeL.D5NlazuQjfQkWEYPZUFChPuz377Qe', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LR79C', 0, NULL),
('1918101506', 'Febri Putra Sandhya Prawiranata', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$ENraMlsxm4KLmUTcAcbbl.J83PQlVDIpkwvVyuZP5ZXdSob45ogVG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'WI60E', 0, NULL),
('1918101507', 'Fiksal Ramadhan', 'III Rekayasa Keamanan Siber Python', '$2a$08$wM110/2cIwKo4f4TfUKWPe6Lclg5ASIOvvyWhytoX8.AexKXcT4fS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'RH96C', 0, NULL),
('1918101508', 'Fitri Ramadhanti', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$0Ia0EmEHl3W3dtjlXyI7buUrtkA2FFfkyg3/g4Ub2wunhqeHn6glW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'SY93P', 0, NULL),
('1918101509', 'Guntur Satria Ajie', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$KFAxNHMxuci30ADVQbOhRenXYy.mHA7p2GcK37KAemZcXy9G5hZW6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VC43O', 0, NULL),
('1918101510', 'Gusti Agung Ngurah Gde Kaba Teguh Darmawangsa', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$Qnd5wnGwwaOmIIXAZ5ureOOz22nHExykMb08bQBUM9PGS5UwEHpUa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KW30D', 0, NULL),
('1918101511', 'Hakiki Widya Ningrum', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$f7Zmtm.l/E286aOhunsVwetMGdeY6QgFd.kJGLLmhcVPQxjiPKhou', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'RA66W', 0, NULL),
('1918101512', 'Hedian Alif Hedfannaja', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$AHdVDkYN7tRlcSWmraZP0OeTxs.Cy0XG1ZWDC0tavlRDCUdUTcuiy', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XB44Y', 0, NULL),
('1918101513', 'Hendy Aulia Rahman', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$jOLgtSOBtBhNSs5Aq4FuJulrexYaMWsq2Z.oQANa76DksddigKhWi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'NW79F', 0, NULL),
('1918101514', 'Hilya Tazkia Kamalia', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$v/QjsFg8up6p7g5NOGA5mOUrHcSxAySs/vffMItxGBgyrfBMRMihS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'UD97W', 0, NULL),
('1918101515', 'I Gusti Putu Kanda Putra Yoga', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$bAuA6aKjaYc4Iuln9ljo0.Mfrsx939EUZ3h78RZ2LSNiTe72OZaui', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'BW86X', 0, NULL),
('1918101516', 'I Made Wisnu Bakti Saputra', 'III Rekayasa Keamanan Siber Python', '$2a$08$.aqgvuD/PUcduTiLJnVV4ezJSiSb0FdT.CLOWMYvby0iet6shW.Om', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AX29E', 0, NULL),
('1918101517', 'Ikhwanul Hakim Masri', 'III Rekayasa Sistem Kriptografi', '$2a$08$kgjPsB9aHtYrys65R0rryumxvlirtZLYZmwhH.PlL9wnVXYFwsm8e', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KD25U', 0, NULL),
('1918101518', 'Ilham Jati Kusuma', 'III Rekayasa Sistem Kriptografi', '$2a$08$PficxmoTJ73n1ZT/KQhTRe5bccqF7Lich0gRxnzP6zj/nxwmnLcsq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'UN66P', 0, NULL),
('1918101519', 'Johannes Simanjuntak', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$NItV7WxU/UAqNj4pUB0vQuf.zqSpf2SSK5cxleD9RaM6aIt5MAaby', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DG65U', 0, NULL),
('1918101520', 'Joko Nugroho Suryo Raharjo', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$1lYfVR8QRuOPbcA/IuCRNeCYRf2b9QdjO8o6XeYxdA/u1uEz21Tjq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VY12H', 0, NULL),
('1918101521', 'Kadhana Darma Tatya', 'III Rekayasa Keamanan Siber Python', '$2a$08$Ic4M0JvzSQzXoBpCJZytneNYhwRiDMC9l3gkP2nXTwl0k4QoxfJ9W', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YL82V', 0, NULL),
('1918101522', 'Kamila Amalia', 'III Rekayasa Sistem Kriptografi', '$2a$08$IjmKTyoGhmZM/0aBdNhq7O5Y74SkHhisaXOrOovyDdDcp6Qd./JU.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XD24D', 0, NULL),
('1918101523', 'Karina Puspa Listianing Ratri', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$IxIcBYcyJpIdeEZsSkXSFecX4lKSWCRYTs79d/2VBhntTMw1S2L/i', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AB59L', 0, NULL),
('1918101524', 'Kevin Yehezkiel Gurning', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$D0CT9RTLVUyc/93mn12oxOOIqs7dJlV9WS8VU1SoC42SmTu0guYLa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CA30I', 0, NULL),
('1918101525', 'Lazuardi Aulia Fahrensa', 'III Rekayasa Sistem Kriptografi', '$2a$08$cCbfFed9jLusmnOQRT1FPOJpEhKhMYl0Wg.bptj/H9MDhzTyuMgbu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CT49L', 0, NULL),
('1918101526', 'Marcella Risky Avianti', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$3IicbFft3YUNA5aiHSlJrOAKESZADlBnKL7YRZdmd3gcHFB7w531G', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'SC78P', 0, NULL),
('1918101527', 'Marwah Nur Azizah', 'III Rekayasa Sistem Kriptografi', '$2a$08$0gDQLOvsCa930WIr/AcDkua//as//CymGRAgqbujszmx9xe5XA6jq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LK88D', 0, NULL),
('1918101528', 'Mega Rosita', 'III Rekayasa Keamanan Siber Python', '$2a$08$qCShiNGFW2565zcg8VBltOWS0MKW5hKr01cJ2/BDzLprbvKCEezXS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DF23U', 0, NULL),
('1918101529', 'Meidy Fenti Salsabila', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$llinBm7JMeqXLqK.kuhJG.pOby9.PLHkf5wbnMbtZUGj74hmZ6bFW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JO52Y', 0, NULL),
('1918101530', 'Meme Indriana Putri', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$i2.EwPUD4/88.4h6pi9T3O3xwOjyiwE.Fn7omLO/BLvCH8Ybu7zja', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HX11P', 0, NULL),
('1918101531', 'Moh. Faishal', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$XPLiXxkqxVqo235y/tKWlOOFCEmPw0vzI0d3nsiJMya9hXvTENwwK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'ZO81K', 0, NULL),
('1918101532', 'Mohamad Rizal Fitrian', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$QrHVBNuUvPkRvy.NdON4Su0go9pykg4AIxibiZOw3P333wXwBtck.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MG56D', 0, NULL),
('1918101533', 'Monica Christy Natalia', 'III Rekayasa Keamanan Siber Python', '$2a$08$6.nepdoHpefVNuNLqsumIOi7oJ3XwmRXTMln1334EW8Mqi4mM2ECq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'ZP35X', 0, NULL),
('1918101534', 'Muh.Amirul Faruq', 'III Rekayasa Keamanan Siber Python', '$2a$08$RphNaykfvTSAM.agE2p0/OgKLJ.eFT1nqxRO.8.CCIBBao1.yOQsu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IR42S', 0, NULL),
('1918101535', 'Muhammad Dandi Pradana', 'III Rekayasa Sistem Kriptografi', '$2a$08$cpUJMytfCFdfKE7bobZ4Lu9XxK.hL93lP0DZ9hAfHf6lqV1Oosn.y', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YK59I', 0, NULL),
('1918101536', 'Muhammad Faiz Syukron', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$n.VcN4zsBtYn.bUrDMMxtuT.ah23HzrJsLBMRPaTx.xakbrNs.WtK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YZ78I', 0, NULL),
('1918101537', 'Muhammad Surya', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$WEk2yfZoS6U6l/79EAnlp.dUxDM9/dnIkv7/X7hk451mTPwO/1M5u', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XM10W', 0, NULL),
('1918101539', 'Nafiz Feizal Mahendra', 'III Rekayasa Keamanan Siber Python', '$2a$08$s5eeK.8jiaEA.pqkUF5mde8sFRNp6jpxMnM0It5yKODXxN3bbXdqy', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'EB82W', 0, NULL),
('1918101540', 'Ni Putu Tarisa Purnama Putri', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$9GRzzRT0mrgkpjk/dBf9Y.8Lg/vB8z2rNb4VcwBm4FpTGcl4G3AC.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'PY11X', 0, NULL),
('1918101541', 'Nindi Anansari', 'III Rekayasa Sistem Kriptografi', '$2a$08$sqRjws6EgdTpHZWjbelM.e0pyjSiY6lppFRwzzDWH2Aqxbw2/qBuy', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'ZJ74X', 0, NULL),
('1918101542', 'Nonie Krisnaningtyas', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$JsxJXcXpi1AVaSQnpnrcFuKS88pgZOH.9Pqw6HlFkoUddQGwz2upa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'FT50U', 0, NULL),
('1918101543', 'Nurul Amalia Rendreana', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$DHctwqDtJl48ZXEaRtsvg.T9CUB1rQBJFZ6GtQ4BhNMGL6kiE55im', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DK50E', 0, NULL),
('1918101544', 'Olga Geby Nabila', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$M31vM1BPcmCF4NZ.LVA3yuJ4SZLlmaLEjdWJWRjpH7AID21lo270e', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'TM71M', 0, NULL),
('1918101545', 'Pratama Aji Prisadi', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$RqK836nvNa4ATRnA.qVN4O7FRxSDvuDMVHNV.WNNweaul9nk5czpq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GY83W', 0, NULL),
('1918101546', 'Pratiwi Armita', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$BSRl8bummTN7PzW4B9lFMOt6EVwANmqpJ.pGsv1mX2d0xdo93BVyi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'RR54P', 0, NULL),
('1918101547', 'Putri Azizah Restu Bumi', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$vUxkeatx66Z9QRgz1eJaTet1V0Sz.LO/md7R74j7HQRmV3lWrWt8a', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HL49L', 0, NULL),
('1918101548', 'Putri Rusdwi Kusuma Astuti', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$x93Qs5Z/Z9Im1eY4FskqWuBrFuFiWaxfRg22mDDOwrJfuIjzYQngS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XC25F', 0, NULL),
('1918101549', 'Rico Rinando', 'III Rekayasa Keamanan Siber Python', '$2a$08$Uk3EdYQv5gqcIJTyhPb6PuMWQTiR5pbVNcGFvs/4hAg6VGbmwazYW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HE28W', 0, NULL),
('1918101550', 'Ridwan Tri Cahyo Utomo', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$DFbbeWWL8s8Ba.r2s/rVfux6gVn/PuIp1hWT.Z6QfgawcoI8r0VCi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LW65Z', 0, NULL),
('1918101551', 'Roihan Akbar', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$ZN9b7UCyDk9MRcS3t2CLYePtmVknZskIx/N6Sbtbm6PKDMWw6mHF.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'FL67W', 0, NULL),
('1918101552', 'Rossy Artanti Zulaiekha', 'III Rekayasa Keamanan Siber Python', '$2a$08$084/d6S7qikYwB4KkVOc5.fV0p5JgU/WcZ0xfFEk8vERWBvS2o4KG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'WW41G', 0, NULL),
('1918101553', 'Sabela Trisiana Oktavia', 'III Rekayasa Keamanan Siber Python', '$2a$08$ONHtk5bK0YSBm4WPxBIWxOEwMeBW3lxyHOapjRkz8BuH/mOfEeoP6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GC56S', 0, NULL),
('1918101554', 'Salsa Sabila Baladina', 'III Rekayasa Sistem Kriptografi', '$2a$08$jhUOF5fxnueaB9BPYddnG.u7sB5DPVKtYv4Ht2ZFD/1oyrWI5U0im', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XS62U', 0, NULL),
('1918101555', 'Salsabila Hadida Difanda', 'III Rekayasa Sistem Kriptografi', '$2a$08$1WIs0h1Pr9NjwwVbZYoPCO/koaP6.NQgUCuPsQANxRH/oPjcKpiE6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IK80Z', 0, NULL),
('1918101556', 'Shakira Putri Ayunda', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$PvERJAPrdPAuZ0OqfIbd8.cZDtdXE94LG0rNTJTA7nBbO.lc4OuWe', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'WQ22P', 0, NULL),
('1918101557', 'Shalihati Mutiara Rejki', 'III Rekayasa Perangkat Keras Kriptografi Elektron', '$2a$08$XoL3ys78NsYnihkbjRqWs.PssXDznmM3OLfOv4J/4jSdZ6nJzvHdG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'PO18N', 0, NULL),
('1918101559', 'Syafira Mardhiyah', 'III Rekayasa Sistem Kriptografi', '$2a$08$Ipc2Hwq6659oFz5sqNpfiedxct3yHyjvkBxTmxdMComoolBh3bBGi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'ED26R', 0, NULL),
('1918101560', 'Tsania Nur Hanifah', 'III Rekayasa Keamanan Siber Python', '$2a$08$RqP.9VEbn45fpRyupv8e/OmbElAw2FQgnCW5/XWsXBLxJ/N1Wq.jG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VF61W', 0, NULL),
('1918101561', 'Vandzani Farhan', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$qERNmeFwOUVyG8HcrVgnr.grCcbM2dT1TnBDHhtljsf5cYfr2T63K', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AK68P', 0, NULL),
('1918101562', 'Verent Regita Mapareza', 'III Rekayasa Sistem Kriptografi', '$2a$08$IzL9W8tO6KKK5Z1QRMsAcuaW0OjlZMQ6oVZ0a6jDrTtFZWlAyShE.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HM70Z', 0, NULL),
('1918101563', 'Wanodya Estithama', 'III Rekayasa Keamanan Siber Nexus', '$2a$08$clEqLhjD3HflG6Auh0rnBO/ISFht7MZihwIq8.znSNu1JGwgollDy', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DO48S', 0, NULL),
('1918101564', 'William Barkem', 'III Rekayasa Keamanan Siber Python', '$2a$08$I2aT/ijcK1N1fum9kzLMZuSuWGSdQf.Sl8kZGgALgYrP4sWU/4pJi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HJ83L', 0, NULL),
('1918101565', 'Yehezikha Beatrix Natasya Ully', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$Oj4d4M.b1G..T/0vEsJCWuI7zgpZk2s6dpOis68WVBt9nLgin5.V.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HL54Q', 0, NULL),
('1918101566', 'Yogi Ibnu Prasetya', 'III Rekayasa Perangkat Lunak Kripto', '$2a$08$jh7KccPJ0ydpZLBjnxJaG.80LFlC8FEoP5fEGQB08hyneL1n5wdLC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HZ81J', 0, NULL),
('1918101567', 'Yulyanti Hendriani', 'III Rekayasa Keamanan Siber Ruby', '$2a$08$ezrFaZ8BRw1J4.WHFUWJXOUWUInigANftyIoyIbNhKJltQZ9GQYDe', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KE34V', 0, NULL),
('2019101568', 'Aditya Hari Kurnia Putra', 'II Rekayasa Keamanan Siber Red', '$2a$08$5GSzc13Jhx1WqkFby2d4AOyxZn6torBOyDOsSDuVUX33pLZeNrZLW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MB71M', 0, NULL),
('2019101569', 'Aditya Sukhoi Lean Sumule', 'II Rekayasa Keamanan Siber Blue', '$2a$08$Yd8yNifYkSKCmzyOaXIYeeyUASEY/cEns8xgYFFDqTvZk.0zO9lmC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HT60I', 0, NULL),
('2019101570', 'Afrizal Ajuj Mudzakkar ', 'II Rekayasa Keamanan Siber Blue', '$2a$08$N//Eg/Qc5spTUqE6t3w6w.UFOMumljunhGzdwZj7Zky6eqPm9q4bS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OR23Q', 0, NULL),
('2019101571', 'Ahmad Pardan Mahpudin', 'II Rekayasa Keamanan Siber Blue', '$2a$08$P5hxWwXv9vz9pUPp5GmSLOyPPSosqDGtxnDn5nKIQ1MOabZ.5b0Ue', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KO27L', 0, NULL),
('2019101572', 'Ahmad Zainudin Mahfud', 'II Rekayasa Keamanan Siber Red', '$2a$08$1LenIEdw9RLEFPxKOIx4gOpkHnEDdBQF3PBgxtR5njZbxLF4rkU4a', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'RT53H', 0, NULL),
('2019101573', 'Alfian Putra Rakhmadani', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$TmScTZ5HS8OCW7byXNVyqOQxhCYZtMgFy6oNcVR8tUpW77qsRLzWy', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'BS39S', 0, NULL),
('2019101574', 'Alfido Osdie', 'II Rekayasa Keamanan Siber Blue', '$2a$08$FFIe88JUt8i.HFa292djNeDzcbkF7Ze/c5HqMjGaFvoyau.QTK71K', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'WK15W', 0, NULL),
('2019101575', 'Almalika Setya Widyanti', 'II Rekayasa Sistem Kriptografi', '$2a$08$70WzVYjI3VJ4t5XfeUtOYONWA4HlN8/Cat1dEjBIwYZXHIjf2ik3u', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MD86Q', 0, NULL),
('2019101576', 'Andre Irawan', 'II Rekayasa Sistem Kriptografi', '$2a$08$9wgvajeziotgT6UdOXxDZuN3Q2uJgRKgwOnYl6P/s.aG8xkUjgBx.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VJ93N', 0, NULL),
('2019101577', 'Angelita Salsabila Afifah', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$M3xRCW04LVqISmNwaUrpbebEvT2dUYPpMLvzoxrq4ZKG8zLfbvtTq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'TF36C', 0, NULL),
('2019101578', 'Anita Rizkiana Handiko', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$rYx6nZRGPwBwfn8RiMHCfeWG2gh7Ed4W6Qf55zRkqjrcZbmhIlSGS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'QK38G', 0, NULL),
('2019101579', 'Arga Yuda Prasetya', 'II Rekayasa Keamanan Siber Red', '$2a$08$xeGfje0iC70.ac4cortcnOCC0n2HHRKj8MZkqehYvzQCsQjVGiQnS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CD33S', 0, NULL),
('2019101580', 'Ariska Allamanda', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$58NmD/lNoxM3BYbKmK5QnuKDqj1uKBXijQGGLwUZGgAY9kNDH9Ir6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GO78L', 0, NULL),
('2019101581', 'As\'Ad El Hafidy', 'II Rekayasa Keamanan Siber Blue', '$2a$08$LdnagZgpXBOrTBa3g30rLeKKsp9xyN80kygV6pBmQY647TcvwU1nu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'SF50I', 0, NULL),
('2019101582', 'Aura Nirmala Puspabuana', 'II Rekayasa Keamanan Siber Blue', '$2a$08$VYZuhavySGfj9RaVTooTgOpQHNrRsWgA8NLYoEqW6lNgGIH7jAwY2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KM58O', 0, NULL),
('2019101583', 'Bagas Kurnadi', 'II Rekayasa Keamanan Siber Red', '$2a$08$1SOPMKq.RihchfMuSHGPrebtJITfSZW6oZm6skY3b1b0loS3.r8/y', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LZ24A', 0, NULL),
('2019101584', 'Bella Wulandari Hartejo', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$y9UyXufRFZo0atUen81No.SBekxdwHRSCDhziSPTw38xIPrSlM2Qu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KC83D', 0, NULL),
('2019101585', 'Brian Nasywa Rayhan', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$aeAw0/z36S.wbFwOB8ExgeGQJF32w7lPyuY9qpCYaar2ZrquR9uR6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OB63Z', 0, NULL),
('2019101586', 'Danang Enggar Risyaf Alam', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$4uqo0mo3Ud48O2olCjIhUObIrmZ7nsioCx1/5/V0gUiXZ/BglTN2q', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KN79X', 0, NULL),
('2019101587', 'David Relikson', 'II Rekayasa Keamanan Siber Blue', '$2a$08$sJA2yWRdVWYYVDetyukv7uDGEaKe6XAsDS3hoMkirWNY5jmMKaU5K', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'UF45I', 0, NULL),
('2019101588', 'Dawwas Arya Bahytsani', 'II Rekayasa Keamanan Siber Blue', '$2a$08$Sa/Ghh8FVs0SraCXDJ8VfedN2gQjMaTl89sgQ5Tdky9mpNaRCPz3e', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'TN27K', 0, NULL),
('2019101589', 'Dessy Ariami', 'II Rekayasa Keamanan Siber Red', '$2a$08$es85A5v8pjWqncMjPlqgium/Cr2lqtAaPGbKyQk4.oHR23bCh63nW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'NQ20K', 0, NULL),
('2019101590', 'Dika Agustian Akbar', 'II Rekayasa Sistem Kriptografi', '$2a$08$J9BRamHlqE2ns8S/Zo5PH.oqDBcA9O59KjX8g6Kk4RqIiZCm8l3/u', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JG48U', 0, NULL),
('2019101591', 'Dimas Wahyu Utomo', 'II Rekayasa Keamanan Siber Red', '$2a$08$5jIk2ofzxvQ1afu0tO0URuoH6BTvluos2wjhan377pXnQxXblLA5i', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CB23F', 0, NULL),
('2019101592', 'Dzakwan Al Dzaky Bewasana', 'II Rekayasa Keamanan Siber Red', '$2a$08$Oqi.eXLhmX4fsf9SZroTs.PhWbIA1PYaehMnWmuBGXb2UfuSWtYJu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LG98E', 0, NULL),
('2019101593', 'Efifania Sibagariang', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$BI2PbilEzv2ha3SR4jtyFeu2VBBkF10PV0I7SjQxhbhm7MEG4ppAO', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'RP17K', 0, NULL),
('2019101594', 'Erfandra Ramadhan Susanto', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$UFdXyzaqUyAyh/jQ0ej5OOfo2nbG4ceezuE6m7EmeLMBCNhEa6aTW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AQ96A', 0, NULL),
('2019101595', 'Erza Adira Mahatir Muhammad', 'II Rekayasa Sistem Kriptografi', '$2a$08$2k6tPkyvAtdTdJXrwPG9WeVvNRIiIHZLMm4CL1EeM1XDxusjHjx5q', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MN78M', 0, NULL),
('2019101596', 'Fachrul Ali Nurfadillah', 'II Rekayasa Keamanan Siber Red', '$2a$08$rq/I5WNkqgouxAxaDE/RvOSgHtHALCU5UUbEqx95Fol3VlfCDa7ky', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OQ26I', 0, NULL),
('2019101597', 'Fadel Azzahra', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$cfA5ROkcv.yVPaML9t6R6.NVM1riqlYwoqVNCcoC.hR4kr.wl.bCm', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HC12I', 0, NULL),
('2019101598', 'Faizal Gani Setyawan', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$jPhxNfQl1/gMTeizPWvxqeoNIN6pOL4CUI4sWalbXTRjHNy0AecB6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'PO46Z', 0, NULL),
('2019101599', 'Falinsa Salsabila Yursa', 'II Rekayasa Keamanan Siber Blue', '$2a$08$IKNa4zR04fibV6A0.stsq.Dc4W.xZdSf/zrK75jKoBCdaCG9pYWE2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IR17V', 0, NULL),
('2019101600', 'Faqih Rangga Wijaya', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$E9/OyVVD3NkcpVQ/IywiWO7bZac5tk.jY1YzVd5WHMF9IfwnTeUsS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CH16Y', 0, NULL),
('2019101601', 'Febrizky Dwi Restu Rini', 'II Rekayasa Keamanan Siber Blue', '$2a$08$rUEVG3bYsAK4/lYs8w3X7u6.wF..G6sF46iB44LVbchp1AuBuvujK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JY98W', 0, NULL),
('2019101602', 'Fikra Amalia Raihana', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$IiT66PDGa/drgvPOn4wkJOaMzzjhcqzRbBfbyQWyi7CO3gC9e5z2S', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'BJ52R', 0, NULL),
('2019101603', 'Galang Mardani', 'II Rekayasa Sistem Kriptografi', '$2a$08$tLGpAuXbkc5phr5CCP2/0.pK32uIOBB1nbDLC4DpXrkz022mtmCy6', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KQ83N', 0, NULL),
('2019101604', 'Grace Friscilla Margaretha Karo Karo', 'II Rekayasa Keamanan Siber Red', '$2a$08$W3iUgo.OQcbVM0pieMUke.mvhK0ViJTxoDP/N1i61SPH2kPdCmQ2i', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KN20X', 0, NULL),
('2019101605', 'Gustito Panatagama', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$byC1m7uagv2QTFrwtwEBGOojHiyB8uyEZKGpTuixnAQBFtun5VXki', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'PI36D', 0, NULL),
('2019101606', 'Habib Alfitrah S', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$dmCYLZ2gvdrf4IC7.ieFoOfkP8ifPQ.yyAKpR1.qQ9CrW2kh8GECm', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'WE97Y', 0, NULL);
INSERT INTO `users` (`npm`, `fullname`, `class`, `password`, `createdAt`, `updatedAt`, `roomId`, `coin`, `password_digitalsignature`, `ctf_point`, `title`) VALUES
('2019101607', 'Happy Sandhiyadini Rosari', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$qWWicY0LWfoRXrBoJfycxO/ONp5Sm4FqGShnagK2j2/rjNy1LiicC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'NO99F', 0, NULL),
('2019101608', 'Herisa Pratama Nur Baeti', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$BX/3rbmo5fLNai24ObNmdu8xDzwZ/W1PeOAD0IxLXp4wNur9AVO.O', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'ON84B', 0, NULL),
('2019101609', 'Herlambang Rafli Wicaksono', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$aUqMQciurPbMeu9/pZvodecdu/8ocHhjU6O2uKbvUUSCt03Pks0Wy', '2021-10-10 08:10:56', '2021-10-22 21:37:07', 64, 0, 'XE49F', 0, NULL),
('2019101610', 'I Gede Gilang Dharma Suputra', 'II Rekayasa Keamanan Siber Red', '$2a$08$bP1Y6q2RVlD/4hcqBc74BOdLgjTEejlEf8gKB7LVuzKCOr3PtyOum', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OV29J', 0, NULL),
('2019101611', 'Ihsan Fadli Tampati', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$CBB3iKUcOsQYqc3uKhlDbujTTakkLJm6aUgnu2W/e7XBtLB02sKO2', '2021-10-10 08:10:56', '2021-10-22 21:37:03', NULL, 0, 'XP85X', 0, NULL),
('2019101612', 'Ilham Dwi Harjanto', 'II Rekayasa Keamanan Siber Red', '$2a$08$Thlk7pDROrGKgXVRa3Lfu.acwZSTh1EQkRaKRp5NF3fbN5zLCrVpa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'WN41M', 0, NULL),
('2019101613', 'Irman Yunal Wirdani ', 'II Rekayasa Keamanan Siber Red', '$2a$08$n.qiTlM.iFmtJ7SjyCPb8.OlpqCfMj50lAOL1rtF7Dle9.6GQaQay', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OE57B', 0, NULL),
('2019101614', 'Khirsa Inayatul Aini', 'II Rekayasa Keamanan Siber Red', '$2a$08$pnHLzCW9PoLBBVH8HTrSJOwp6wfzlXEqTuM4WlFziRRj1DU8rrcLu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'QW23A', 0, NULL),
('2019101615', 'Khoerina Sa\'Adah', 'II Rekayasa Keamanan Siber Red', '$2a$08$VgULohxysj4cYVXpp9cN1.6JFxQUIWcyAg4P7EiYqMtUrM.mrtuHa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'BN56C', 0, NULL),
('2019101616', 'Laila Nur Khofifah', 'II Rekayasa Sistem Kriptografi', '$2a$08$0xOkjFbXXmP7bmfF5WOeF.OsgPBulQ/X01cdfgKzHYdoZgUnYNxYe', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GZ27P', 0, NULL),
('2019101617', 'Lintang Listyowati', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$X5ooBcY7NZtuWw2mMHguauuNY0RXdG/BtTN2uaVKh323jm4N1ZTGK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'WV16B', 0, NULL),
('2019101618', 'Luthfi Hakim Panuntun', 'II Rekayasa Sistem Kriptografi', '$2a$08$H/hLMxLVAl3H.3BAH.8F2.Ugwe2FKTnYYsQTNEWvo0yjbnA1Arziy', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DL57Z', 0, NULL),
('2019101619', 'M. Firdaus Yusuf', 'II Rekayasa Keamanan Siber Red', '$2a$08$wjG/H726dkG/hfkzeIo8j.DMIq.UFxggMM3g2WEFtZaVflVVHdHcq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CF83L', 0, NULL),
('2019101620', 'M. Ikhsan Mubarok', 'II Rekayasa Sistem Kriptografi', '$2a$08$goMmKOcqKU4D1/K.NuPEt.Cgiuc0/LcovL3HEBMSFtN549GCLqeo2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LZ53Z', 0, NULL),
('2019101621', 'M. Luthfan Putra Sopian', 'II Rekayasa Keamanan Siber Blue', '$2a$08$9w3Ye8OMcmctdETv/LIdYuJC8h0dvSBoY4ZqXqMhj2sfsdIA3ZAhW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LV30W', 0, NULL),
('2019101622', 'Madany Awang Bintara', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$iW2IOppTCRJ.PgxR2P66LuiQKaVwfH77NXS8hTmavIRGvSTcs6dXO', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IC91E', 0, NULL),
('2019101623', 'Mahaputra Giovani Muhammad', 'II Rekayasa Keamanan Siber Blue', '$2a$08$4wJabfx50.cdYP8tOjInr.ctVu2bAe7Ujt/aOJspxlUndV6hI/XIW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VX43D', 0, NULL),
('2019101624', 'Malika Ayunasari', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$YMpDd.PMCkMfveLo2RO9veGw6LB/035vCXCeUPjqW/FUa/dNfgLYy', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CB46D', 0, NULL),
('2019101625', 'Mazaya Vanisa Putri ', 'II Rekayasa Sistem Kriptografi', '$2a$08$smE4YjnLr4BPNWXJ9WmVLe8.mtodOnI5PBuQGy0gOEGCTecVodgHO', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YY63X', 0, NULL),
('2019101626', 'Melanisa Nur Harin Prandya', 'II Rekayasa Sistem Kriptografi', '$2a$08$wGPFNN7WO61TFawfjNJHVeo2KThp.1P0JNoN0mVRglZxBk6ZM8hBu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MT58G', 0, NULL),
('2019101627', 'Mirza Uliartha Simanjuntak', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$ZcAC58QshCz.74c3gDCgJ.DPPIvhvd6QoamlfN9/TIu0i6Y.erYvC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KV34C', 0, NULL),
('2019101628', 'Mohammad Raffli Firmansyah', 'II Rekayasa Keamanan Siber Blue', '$2a$08$MKZ.kJ.yYpMmFn.boJM7guRzGkDAfE18N.kLAYMX2rU//Vyj1Yo1G', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'UH95I', 0, NULL),
('2019101629', 'Muhamad Nadhif Zulfikar', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$zpUu8RuKJhRlJlYUeWWYSOnfqzrXEFA/eUYnmV21wcsv/4QmovLDK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'LO77F', 0, NULL),
('2019101630', 'Muhamad Tegar Sabila ', 'II Rekayasa Keamanan Siber Red', '$2a$08$BD1Od5IfKaQbMWuEMSnAUeVHt43AYP.EOa//c0d2NF8sl05eeyWAW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'RE39C', 0, NULL),
('2019101631', 'Muhammad Faturrohman Sugiyarto', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$f8/lQeVFe4BQ90i16p4HXOCMCs80f4QBhSKiFxyTgokrlSr9rT5PK', '2021-10-10 08:10:56', '2021-10-23 20:14:06', 86, 571704, 'RZ73P', 0, NULL),
('2019101632', 'Muhammad Habibi Motaain Nirbaya', 'II Rekayasa Keamanan Siber Red', '$2a$08$Rrx/kbLhZg0ouHG1Om4Z7u6VR0IQugzEGd4lmmQScgV5T9Y5NNJUO', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'DV52F', 0, NULL),
('2019101633', 'Muhammad Haidar Wijaya', 'II Rekayasa Sistem Kriptografi', '$2a$08$d12yC/YxPHFDYUjJEQH6B.GbsOv3UWqo5P3Iu4E9of7A3iHZfcNmW', '2021-10-10 08:10:56', '2021-10-22 21:37:01', NULL, 0, 'MJ62V', 0, NULL),
('2019101634', 'Muhammad Hilal', 'II Rekayasa Keamanan Siber Red', '$2a$08$gF0j7BM6pBRdDwpWZXbhrOOlEadaTGaRfhJReshmnVrxwWUKtASIa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'HA39N', 0, NULL),
('2019101636', 'Muhammad Rahdian Ega Kurnia', 'II Rekayasa Keamanan Siber Blue', '$2a$08$NYfhFDV9QGhzLbofAK2PzuMa2e3dGW.eUyuy4GkbS4cte8toM0Vla', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'UU86E', 0, NULL),
('2019101637', 'Muhammad Sofyan Arif Harumnanda', 'II Rekayasa Keamanan Siber Red', '$2a$08$ysmXxDdRXf4c4vKKqQPO9OoeKlyuzAal0BaL2Bu607l2xchej1kvK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'OS68M', 0, NULL),
('2019101638', 'Muhammad Syaibani Al Hakim', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$UpyAc8FPIi2ww9AWEUU8U.mNrrGak3e5PaEMh.tFYeMNOoSjGKyaS', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'NE90B', 0, NULL),
('2019101639', 'Mukti Wibowo', 'II Rekayasa Keamanan Siber Blue', '$2a$08$/.2JVW1nVvF1JPOye8pqAOp6D2y7ZBLEEuKqDdxSxIXWAf9/nSI4.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'XZ50K', 0, NULL),
('2019101640', 'Nabilah Nawang Amaranggani', 'II Rekayasa Sistem Kriptografi', '$2a$08$.o4FFJdbJwGLzXhgrS2Cq.bmBgZDsDQtPu5Sf3uCvLGnig.MA9xDC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'KZ75R', 0, NULL),
('2019101641', 'Nathanael Berliano Novanka Putra', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$NGtanNEjrO3V7S.bt.WGZeN69Ftu/hfKe8TNhTM1NQ9JN2zoe36Um', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'WV19Z', 0, NULL),
('2019101642', 'Nila Ardhya Lintang Pramesty ', 'II Rekayasa Sistem Kriptografi', '$2a$08$zjQ4RskNwhFLXp9mh7o5Pu8f05a2l9mdToLyd1SNKg29sfGcOLtiq', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'TJ62Z', 0, NULL),
('2019101643', 'Nugroho Adi Wibowo', 'II Rekayasa Keamanan Siber Red', '$2a$08$7PwujYcw8QAxfuvTnwKZ8ur0zRTN7uknPS.9z8rjftsrL71A6xJKC', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'GT61X', 0, NULL),
('2019101644', 'Nurfarida Sekar Andzani', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$/4T3kP4PqCtCFvH0Gp2VLezk5Z4Qs6ZMV5NsFnefjHXShRO04lAXe', '2021-10-10 08:10:56', '2021-10-23 08:42:10', NULL, 25000, 'IE94Q', 0, NULL),
('2019101645', 'Patar Wiyardhana Marbun', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$ET6CiT1sv0DCyNzp88wKHeOjvA7qJBmmqucx1srJuQxFlzG6Bu0wK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IB59H', 0, NULL),
('2019101646', 'R.M. Genggam Satoe Bintang', 'II Rekayasa Keamanan Siber Blue', '$2a$08$lknmBIdw5Mn3OS9YyykLlOrNw2CIK.0G0F0yUjW0n1C3fp0EfeoHa', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YL64E', 0, NULL),
('2019101647', 'Rakha Wilis', 'II Rekayasa Keamanan Siber Blue', '$2a$08$6qnfWLPxjO5swXMDuCGMwu0x8zzCvPSqnuOniR5UJG7UmBD80eUi2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'BF34N', 0, NULL),
('2019101648', 'Rayhan Ramdhany Hanaputra', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$yzQPQQxhVYOtZrmRoS72BuEKxGGrMLqIWD9GVL7aXhaWGFbHfPlh6', '2021-10-10 08:10:56', '2021-10-23 20:34:17', 50, 120956, 'FF84U', 1, NULL),
('2019101649', 'Rey Maulana Faridh', 'II Rekayasa Keamanan Siber Blue', '$2a$08$AhPqgHG28RrKWZLTIo3egOjM1Pit0MVSkU19Gfr/RPXr3N/XXJGI2', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AY29O', 0, NULL),
('2019101650', 'Reza Aulia Wildana', 'II Rekayasa Keamanan Siber Blue', '$2a$08$4OEfUM6kxGlSA05wgzadR.liuRzTzOxskmzI39bFPxkgN4YMGjtmi', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MA38K', 0, NULL),
('2019101651', 'Rezar Surya Efendi', 'II Rekayasa Keamanan Siber Blue', '$2a$08$3iaU6ihEgygbafs79NRGduPNE5R8vBDQj8a31z8Bk0rNGbPS2Om72', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'SQ44T', 0, NULL),
('2019101652', 'Robet Edi Prasetyo', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$jPcmcIEJGo63FFPzVvbLyu2rlAarWSVUycG.QQT.dIwYILwyjj4X.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'MO83N', 0, NULL),
('2019101653', 'Rudolf Paris Parlindungan Sihombing', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$ntzoSoXlyo3yGZtinsYgxOWu/W4z.u0jDJkheD8OnYIwlMakuLBy.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'QB93Z', 0, NULL),
('2019101654', 'Ryan Muhammad Azizulfiqar Kamajaya', 'II Rekayasa Keamanan Siber Blue', '$2a$08$ig.TC08NacId1ohWFy8LJOCmmviNsKA0at1xI4No00RqdgW8Gg4de', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'SM49Y', 0, NULL),
('2019101655', 'Saca Ilmare Dinbiru', 'II Rekayasa Keamanan Siber Red', '$2a$08$1Vsh1EmWjHgTCMVHe2sz0uv4F9aEBycjUz73uRl0pO.6j1tOO0SEW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JX93L', 0, NULL),
('2019101656', 'Saddam Hidayatulloh', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$oZg6nnotPNDvnJtNuK3JpONA9tj8ctr/fwAeDRJrIYr131R40hoP.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'AN58O', 0, NULL),
('2019101657', 'Sandy Tri Kurnia', 'II Rekayasa Sistem Kriptografi', '$2a$08$1hfXWXKKHfr7lztUsE1.Ou1QioMaGPu8rQEBe5VXFRjRl06VA.aoe', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'IN38M', 0, NULL),
('2019101658', 'Siti Manayra Sabiya', 'II Rekayasa Perangkat Keras Kriptografi ', '$2a$08$3peN/uQ.8GW2wWFkvan5f.xSIcRrhUGLoGq/sy7jLob0COFbWEyTm', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'VN13H', 0, NULL),
('2019101659', 'Socrates Anugrah Mendrofa', 'II Rekayasa Sistem Kriptografi', '$2a$08$wXkSUGvO3CtFm251NwZcZu758xk40YI6M78mA./TT3GHaVGl8twBK', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'CJ35R', 0, NULL),
('2019101660', 'Syafrizal Ananta Adhitya', 'II Rekayasa Sistem Kriptografi', '$2a$08$nYeCfkbeMpE9gus7WBRvruJmKXWC/oqibW/VwXfB46pPuKCsWmwcW', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'JT62E', 0, NULL),
('2019101661', 'Tsamara Khadijah Slim', 'II Rekayasa Sistem Kriptografi', '$2a$08$EJ9ue2FV2V0u/Hn6Y3XrEO2YBe23lDvNBZij9tzKqFY7jEQ2Iqf0y', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'RU33J', 0, NULL),
('2019101662', 'Willem Michael Albert Mondong', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$fImNUHTG2Yb9fR9ZQKaRvepJb2TDu29k7pcsOd9qLiJrogag8D4TG', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'ZQ56M', 0, NULL),
('2019101663', 'Wiyar Wilujengning Sejati', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$Z7x1wbwoLJG.NucESYoKJedvl5eVEJb9tuSVZrwsl2Q5wtyKyKQGm', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'RZ70O', 0, NULL),
('2019101664', 'Yafi\' Athallah Yuwandana', 'II Rekayasa Sistem Kriptografi', '$2a$08$T4JE8RqMTpEDcWSlRX4w4e0Hi3XlRsiuG/KIuoS6.rO.2n3vWShY.', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'FW68Y', 0, NULL),
('2019101665', 'Yasmin Putri Salma', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$IaP8lvheBo34HFx4u6TwUuVHOIUOXOaywMlYGN7mx2VCCzVP/POAu', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'UU58R', 0, NULL),
('2019101666', 'Yudhistira', 'II Rekayasa Keamanan Siber Red', '$2a$08$BuySjEyLTY.GoUOuamC3/uGpoaBI/..MHth9oJ.qHA2VGr39DNSju', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'YK55V', 0, NULL),
('2019101667', 'Zulma Mardiah', 'II Rekayasa Perangkat Lunak Kripto', '$2a$08$7XVLM7hf0PQqc7FTqMuaQOppO/x0FX8tJbkGHez8J24mqzKZdj/2q', '2021-10-10 08:10:56', '2021-10-20 21:46:08', NULL, 0, 'FA69B', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `userNpm` varchar(255) NOT NULL,
  `roleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`userNpm`, `roleId`) VALUES
('1817101383', 2),
('1817101465', 1),
('1817101465', 2),
('1817101465', 3),
('1918101473', 7),
('1918101507', 3),
('2019101609', 4),
('2019101609', 5),
('2019101609', 6),
('2019101631', 2),
('2019101648', 1),
('2019101648', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `locuseId` (`locuseId`);

--
-- Indexes for table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ctf_categories`
--
ALTER TABLE `ctf_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `ctf_challenges`
--
ALTER TABLE `ctf_challenges`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ctf_levels_ibfk_1` (`ctfLevelId`),
  ADD KEY `ctf_challenges_ibfk_1` (`ctfCategoryId`);

--
-- Indexes for table `ctf_challenge_completions`
--
ALTER TABLE `ctf_challenge_completions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userNpm` (`userNpm`),
  ADD KEY `ctfChallengeId` (`ctfChallengeId`);

--
-- Indexes for table `ctf_levels`
--
ALTER TABLE `ctf_levels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `fasting_records`
--
ALTER TABLE `fasting_records`
  ADD PRIMARY KEY (`npm`),
  ADD UNIQUE KEY `npm` (`npm`);

--
-- Indexes for table `fouls`
--
ALTER TABLE `fouls`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `foul_records`
--
ALTER TABLE `foul_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `npm` (`npm`),
  ADD KEY `foulId` (`foulId`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jdihs`
--
ALTER TABLE `jdihs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locuses`
--
ALTER TABLE `locuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `itemId` (`itemId`),
  ADD KEY `transactionRecordId` (`transactionRecordId`);

--
-- Indexes for table `researchs`
--
ALTER TABLE `researchs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `buildingId` (`buildingId`);

--
-- Indexes for table `transaction_records`
--
ALTER TABLE `transaction_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `npm` (`npm`);

--
-- Indexes for table `transfer_records`
--
ALTER TABLE `transfer_records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `targetNpm` (`targetNpm`),
  ADD KEY `transactionRecordId` (`transactionRecordId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`npm`),
  ADD UNIQUE KEY `npm` (`npm`),
  ADD KEY `roomId` (`roomId`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`userNpm`,`roleId`),
  ADD KEY `roleId` (`roleId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `buildings`
--
ALTER TABLE `buildings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `ctf_categories`
--
ALTER TABLE `ctf_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `ctf_challenges`
--
ALTER TABLE `ctf_challenges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `ctf_challenge_completions`
--
ALTER TABLE `ctf_challenge_completions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `ctf_levels`
--
ALTER TABLE `ctf_levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `fouls`
--
ALTER TABLE `fouls`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `foul_records`
--
ALTER TABLE `foul_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `jdihs`
--
ALTER TABLE `jdihs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `locuses`
--
ALTER TABLE `locuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `researchs`
--
ALTER TABLE `researchs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=396;

--
-- AUTO_INCREMENT for table `transaction_records`
--
ALTER TABLE `transaction_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=224;

--
-- AUTO_INCREMENT for table `transfer_records`
--
ALTER TABLE `transfer_records`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `buildings`
--
ALTER TABLE `buildings`
  ADD CONSTRAINT `buildings_ibfk_1` FOREIGN KEY (`locuseId`) REFERENCES `locuses` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `ctf_challenges`
--
ALTER TABLE `ctf_challenges`
  ADD CONSTRAINT `ctf_challenges_ibfk_1` FOREIGN KEY (`ctfCategoryId`) REFERENCES `ctf_categories` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `ctf_levels_ibfk_1` FOREIGN KEY (`ctfLevelId`) REFERENCES `ctf_levels` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `ctf_challenge_completions`
--
ALTER TABLE `ctf_challenge_completions`
  ADD CONSTRAINT `ctf_challenge_completions_ibfk_1` FOREIGN KEY (`userNpm`) REFERENCES `users` (`npm`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `ctf_challenge_completions_ibfk_2` FOREIGN KEY (`ctfChallengeId`) REFERENCES `ctf_challenges` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `fasting_records`
--
ALTER TABLE `fasting_records`
  ADD CONSTRAINT `fasting_records_ibfk_1` FOREIGN KEY (`npm`) REFERENCES `users` (`npm`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `foul_records`
--
ALTER TABLE `foul_records`
  ADD CONSTRAINT `foul_records_ibfk_1` FOREIGN KEY (`npm`) REFERENCES `users` (`npm`) ON DELETE SET NULL ON UPDATE CASCADE,
  ADD CONSTRAINT `foul_records_ibfk_2` FOREIGN KEY (`foulId`) REFERENCES `fouls` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`itemId`) REFERENCES `items` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`transactionRecordId`) REFERENCES `transaction_records` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `rooms_ibfk_1` FOREIGN KEY (`buildingId`) REFERENCES `buildings` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `transaction_records`
--
ALTER TABLE `transaction_records`
  ADD CONSTRAINT `transaction_records_ibfk_1` FOREIGN KEY (`npm`) REFERENCES `users` (`npm`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transfer_records`
--
ALTER TABLE `transfer_records`
  ADD CONSTRAINT `transfer_records_ibfk_1` FOREIGN KEY (`targetNpm`) REFERENCES `users` (`npm`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transfer_records_ibfk_2` FOREIGN KEY (`transactionRecordId`) REFERENCES `transaction_records` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`roomId`) REFERENCES `rooms` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_ibfk_1` FOREIGN KEY (`userNpm`) REFERENCES `users` (`npm`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`roleId`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
