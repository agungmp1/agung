const db = require("../models");
var admin = require("firebase-admin");
const FCMReToken = db.fcmretoken;
const Sequelize = db.Sequelize;

var serviceAccount = require('../config/fcm_privkey.json');

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    // databaseURL: "https://prismappfcm.firebaseio.com"
});

const options = {
    priority: 'high',
    timeToLive: 60 * 60 * 24
}

// Send a message to devices subscribed to the provided topic.
exports.sendNotification = (tokens, message) => {
    admin.messaging().sendToDevice(tokens, message, options)
        .then((response) => {
            console.log('Successfully sent message:', response.successCount);
        })
        .catch((error) => {
            console.log('fcm error:', new Date(), error);
        });
}

exports.sendNotificationToAll = (message) => {
    FCMReToken.findAll({
        attributes: ['token']
    }).then(FCMtokens => {
        tokens = []
        FCMtokens.forEach(token => {
            tokens.push(token.token)
        });
        admin.messaging().sendToDevice(tokens, message, options)
            .then((response) => {
                console.log('Successfully sent message:', response.successCount);
            })
            .catch((error) => {
                console.log('fcm error:', new Date(), error);
            });
    }).catch((error) => {
        console.log('fcm error:', new Date(), error);
    });

}