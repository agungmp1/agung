const controller = require("../controllers/user.controller");
const { authJwt } = require("../middleware");
const { body, query } = require('express-validator')
const multer  = require('multer')
var upload = multer({ dest: `${__dirname}/../export/static/profile/` })

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/user/profile", [
        authJwt.verifyToken,
        query('npm').optional({ checkFalsy: true }).isNumeric()
    ], controller.getprofile);

    app.get("/api/user/myhealth", [
        authJwt.verifyToken
    ], controller.gethealth);

    app.get("/api/user/traits", [
        authJwt.verifyToken
    ], controller.gettraits);

    app.get("/api/user/names", [
        authJwt.verifyToken,
        query('samelevel').optional({ checkFalsy: true }).custom(inp => inp == 'true' || inp == 'false'),
        query('withtrait').optional({ checkFalsy: true }).custom(inp => inp == 'true' || inp == 'false'),
        query('traitId').optional({ checkFalsy: true }).isNumeric()
    ], controller.namelist);

    app.post("/api/user/update", [
        authJwt.verifyToken,
        body('bio').optional({ checkFalsy: true }).isLength({max: 2500}),
        body('room').optional({ checkFalsy: true }).isNumeric(),
        body('traitIds').optional({ checkFalsy: true }).whitelist(['ids','0123456789','\'\",[]{}'])
    ], controller.update);

    app.post("/api/user/setprofilepicture", upload.single("file"), [
        authJwt.verifyToken
    ], controller.setprofilepicture);

    // app.get("/api/user/digitalsignaturefile", controller.digitalsignaturefile);
    app.get("/api/user/digitalsignaturefile", [authJwt.verifyToken], controller.digitalsignaturefile);

    app.get("/api/user/digitalsignaturefile/:token", controller.digitalsignaturefiletest);

    app.get("/api/user/digitalsignaturepassword", [authJwt.verifyToken], controller.digitalsignaturepassword);
};
