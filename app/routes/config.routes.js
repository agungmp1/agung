const controller = require("../controllers/config.controller");
const { authJwt, admin, filter } = require("../middleware");
const { body } = require('express-validator')

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/api/config/openfasting", [
        authJwt.verifyToken,
        body('fastingdate').isLength({ min: 1, max: 29 }),
        admin.isFastingAdmin
    ], controller.openfasting);

    app.post("/api/config/closefasting", [
        authJwt.verifyToken,
        admin.isFastingAdmin
    ], controller.closefasting);

    app.post("/api/config/opendayoff", [
        authJwt.verifyToken,
        // body('date').isLength({min: 1, max: 29}),
        admin.isDayOffAdmin
    ], controller.opendayoff);

    app.post("/api/config/closedayoff", [
        authJwt.verifyToken,
        admin.isDayOffAdmin
    ], controller.closedayoff);

    app.post("/api/config/opencommerce", [
        authJwt.verifyToken,
        filter.dormCheck,
        admin.isCommerceAdmin
    ], controller.opencommerce);

    app.post("/api/config/closecommerce", [
        authJwt.verifyToken,
        filter.dormCheck,
        admin.isCommerceAdmin
    ], controller.closecommerce);

    app.post("/api/config/openroomchecking", [
        authJwt.verifyToken,
        admin.isRoomAdmin
    ], controller.openroomchecking);

    app.post("/api/config/closeroomchecking", [
        authJwt.verifyToken,
        admin.isRoomAdmin
    ], controller.closeroomchecking);

    app.get("/api/config/getroompassword", [
        authJwt.verifyToken,
        admin.isRoomAdmin
    ], controller.getroompassword);

    app.post("/api/config/setroompassword", [
        authJwt.verifyToken,
        admin.isRoomAdmin,
        body('password').isLength({ min: 1, max: 29 })
    ], controller.setroompassword);

    app.post("/api/config/checkroompassword", [
        authJwt.verifyToken,
        body('password').isLength({ min: 1, max: 29 })
    ], controller.checkroompassword);

    app.get("/api/config/getfoulpassword", [
        authJwt.verifyToken,
        admin.isFoulAdmin
    ], controller.getfoulpassword);

    app.post("/api/config/setfoulpassword", [
        authJwt.verifyToken,
        admin.isFoulAdmin,
        body('password').isLength({ min: 1, max: 29 })
    ], controller.setfoulpassword);

    app.post("/api/config/checkfoulpassword", [
        authJwt.verifyToken,
        body('password').isLength({ min: 1, max: 29 })
    ], controller.checkfoulpassword);

    app.get("/api/config/getpamongoffset", [
        authJwt.verifyToken,
    ], controller.getpamongoffset);
};
