const controller = require("../controllers/zoom.controller");
const { authJwt, admin } = require("../middleware");
const { body, query } = require('express-validator')

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/api/zoom/meeting", [
        authJwt.verifyToken,
    ], controller.newmeeting);

    app.get("/api/zoom/meeting", [
        authJwt.verifyToken,
    ], controller.allmeetings);

    app.post("/api/zoom/updatemeeting", [
        authJwt.verifyToken,
    ], controller.updatemeeting);

    app.post("/api/zoom/deletemeeting", [
        authJwt.verifyToken,
    ], controller.deletemeeting);

    app.get("/api/zoom/meetinginfo", [
        authJwt.verifyToken,
    ], controller.meetinginfo);

};
