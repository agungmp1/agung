var path = require('path');
const fs = require('fs-extra')
const { authJwt, admin } = require("../middleware");

var dir = path.join(__dirname, '/../export/static');

var mime = {
    html: 'text/html',
    txt: 'text/plain',
    css: 'text/css',
    gif: 'image/gif',
    jpg: 'image/jpeg',
    mp3: 'audio/mpeg',
    png: 'image/png',
    svg: 'image/svg+xml',
    js: 'application/javascript'
};

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get('/static/*', function (req, res) {
        let filepath = req.path.substring(8)
        var file = path.join(dir, filepath.replace(/\/$/, '/index.html'));
        if (file.indexOf(dir + path.sep) !== 0) {
            return res.status(403).end('mau ngapain kamu');
        }
        var type = mime[path.extname(file).slice(1)] || 'text/plain';
        var s = fs.createReadStream(file);
        s.on('open', function () {
            res.set('Content-Type', type);
            s.pipe(res);
        });
        s.on('error', function () {
            res.set('Content-Type', 'text/plain');
            res.status(404).end('Not found');
        });
    });

    app.get('/profile/:npm', function (req, res) {
        let npm = req.params.npm + '.png'
        var file = path.join(dir + '/profile', npm.replace(/\/$/, '/index.html'));
        if (file.indexOf(dir + path.sep) !== 0) {
            return res.status(403).end('mau ngapain kamu');
        }
        var s = fs.createReadStream(file);
        s.on('open', function () {
            res.set('Content-Type', 'image/png');
            s.pipe(res);
        });
        s.on('error', function () {
            res.set('Content-Type', 'text/plain');
            res.status(404).end('Not found');
        });
    });

    app.get('/bookpicture/:id', function (req, res) {
        let npm = req.params.id + '.png'
        var file = path.join(dir + '/profile', npm.replace(/\/$/, '/index.html'));
        if (file.indexOf(dir + path.sep) !== 0) {
            return res.status(403).end('mau ngapain kamu');
        }
        var s = fs.createReadStream(file);
        s.on('open', function () {
            res.set('Content-Type', 'image/png');
            s.pipe(res);
        });
        s.on('error', function () {
            res.set('Content-Type', 'text/plain');
            res.status(404).end('Not found');
        });
    });
};
