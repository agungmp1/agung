const controller = require("../controllers/library.controller");
const { authJwt, filter, admin } = require("../middleware");
const { body, query } = require('express-validator')
const multer = require('multer')
var upload = multer({ dest: `${__dirname}/../export/static/library/` })

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/library/library", [
        authJwt.verifyToken,
        filter.dormCheck
    ], controller.get);

    app.post("/api/library/book", [
        authJwt.verifyToken,
        body('bookId').isLength({ min: 1, max: 100 }),
        filter.dormCheck
    ], controller.setbooked);

    app.post("/api/library/cancel", [
        authJwt.verifyToken,
        body('id').isNumeric()
    ], controller.cancel);

    app.post("/api/library/setongoing", [
        authJwt.verifyToken,
        body('id').isNumeric()
    ], controller.setongoing);

    app.post("/api/library/setdone", [
        authJwt.verifyToken,
        body('id').isNumeric()
    ], controller.setdone);
}
