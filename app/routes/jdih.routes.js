const controller = require("../controllers/jdih.controller");
const { authJwt, admin } = require("../middleware");
const { body, query } = require('express-validator')
const multer  = require('multer')
var upload = multer({ dest: `${__dirname}/../export/jdih/` })

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/jdih/all", [authJwt.verifyToken], controller.all);

    app.get("/api/jdih/download", [
        authJwt.verifyToken,
        query('id').isNumeric()
    ], controller.download);

    app.post("/api/jdih/new", upload.single("file"), [
        authJwt.verifyToken,
        body('name').isLength({ min: 1, max:160 }),
        admin.isJDIHAdmin
    ], controller.new);

    app.post("/api/jdih/delete", [
        authJwt.verifyToken,
        admin.isJDIHAdmin,
        body('id').isNumeric()
    ], controller.destroy);
};
