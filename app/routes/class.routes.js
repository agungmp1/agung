const controller = require("../controllers/class.controller");
const { authJwt, admin } = require("../middleware");
const { body, query } = require('express-validator')

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/myclass/myclassmate", [
        authJwt.verifyToken,
    ], controller.myclassmate);

    app.get("/api/myclass/get", [
        authJwt.verifyToken,
    ], controller.get);

    app.post("/api/myclass/notes", [
        authJwt.verifyToken,
        body('notes').isLength({min: 1})
    ], controller.setnotes);

    app.get("/api/myclass/meetings", [
        authJwt.verifyToken,
    ], controller.getmeetings);

    app.post("/api/myclass/meeting", [
        authJwt.verifyToken,
        body('lecture').isLength({min: 1, max: 255}),
        body('date').whitelist(['-','0123456789']),
        body('meetingnumber').isNumeric(),
        body('resume').isLength({min: 1, max: 255}),
        body('coursework').isLength({min: 1, max: 255})
    ], controller.newmeeting);

    app.post("/api/myclass/editmeeting", [
        authJwt.verifyToken,
        body('meetingId').isNumeric(),
        body('lecture').isLength({min: 1, max: 255}),
        body('date').whitelist(['-','0123456789']),
        body('meetingnumber').isNumeric(),
        body('resume').isLength({min: 1, max: 255}),
        body('coursework').isLength({min: 1, max: 255})
    ], controller.editmeeting);

    app.post("/api/myclass/deletemeeting", [
        authJwt.verifyToken,
        body('meetingId').isNumeric()
    ], controller.deletemeeting);

    app.get("/api/myclass/meetingreport", [
        authJwt.verifyToken,
        admin.isClassAdmin,
    ], controller.meetingreport);

    app.post("/api/myclass/selfstudy", [
        authJwt.verifyToken,
        body('homework_percentage').isNumeric(),
        body('resume').isLength({min: 1, max: 255}),
    ], controller.newselfstudy);

    app.get("/api/myclass/selfstudyreport", [
        authJwt.verifyToken,
        admin.isClassAdmin,
        query('date').whitelist(['-','0123456789']),
    ], controller.selfstudyreport);
};
