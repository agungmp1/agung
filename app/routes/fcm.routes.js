const controller = require("../controllers/fcm.controller");
const { authJwt, admin, config } = require("../middleware");
const { body } = require('express-validator');

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/api/fcm/register", [
        authJwt.verifyToken,
        body('token').isLength({min: 1}),
    ], controller.create);

};
