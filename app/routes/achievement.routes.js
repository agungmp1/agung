const controller = require("../controllers/achievement.controller");
const { authJwt, admin } = require("../middleware");
const { body } = require('express-validator')

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/achievement/list", [
        /*
        #swagger.tags = ['Achievement']
        #swagger.description = 'Get user achievements'
        */
        authJwt.verifyToken], controller.myachievements);

    app.get("/api/achievement/issued", [
        /*
        #swagger.tags = ['Achievement']
        #swagger.description = 'Get issued achievements'
        */
        authJwt.verifyToken,
        admin.isAchievementAdmin
    ], controller.issuedachievements);

    app.post("/api/achievement/create", [
        /*
        #swagger.tags = ['Achievement']
        #swagger.description = 'Insert new achievements'
        #swagger.parameters['Insert new achievements body'] = {
            in: 'body', required: true, type: 'string',
            description: 'user npm',
            schema: { $ref: "#/definitions/InsertAchievementBody" }
        }
        */
        authJwt.verifyToken,
        admin.isAchievementAdmin,
        body('name').isLength({ min: 1, max:80 }),
        body('userNpm').isNumeric(),
        body('type').isLength({ min: 1, max:15 }),
        body('iconId').isNumeric(),
        body('bureauId').isNumeric()
    ], controller.newachievement);

    app.post("/api/achievement/destroy", [
        /*
        #swagger.tags = ['Achievement']
        #swagger.description = 'Delete achievements'
        #swagger.parameters['Delete achievements body'] = {
            in: 'body', required: true, type: 'string',
            description: 'user npm',
            schema: { $ref: "#/definitions/DeleteAchievementBody" }
        }
        */
        authJwt.verifyToken,
        admin.isAchievementAdmin,
        body('achievementId').isNumeric()
    ], controller.destroy);

    
    app.post("/api/achievement/edit", [
        /*
        #swagger.tags = ['Achievement']
        #swagger.description = 'Insert new achievements'
        #swagger.parameters['Insert new achievements body'] = {
            in: 'body', required: true, type: 'string',
            description: 'user npm',
            schema: { $ref: "#/definitions/InsertAchievementBody" }
        }
        */
        authJwt.verifyToken,
        admin.isAchievementAdmin,
        body('name').isLength({ min: 1, max:80 }),
        body('userNpm').isNumeric(),
        body('type').isLength({ min: 1, max:15 }),
        body('bureauId').isNumeric(),
        body('iconId').isNumeric(),
        body('achievementId').isNumeric()
    ], controller.edit);

    app.get("/api/achievement/icons", authJwt.verifyToken, controller.geticons);
};
