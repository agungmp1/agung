const controller = require("../controllers/auth.controller");
const { authJwt, admin, config } = require("../middleware");
const { body } = require('express-validator');

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/api/auth/signin", [
        /*
        #swagger.tags = ['Auth']
        #swagger.description = 'Sign in'
        #swagger.parameters['Sign in body'] = {
            in: 'body', required: true, type: 'object',
            description: 'user npm and password sha1 digest',
            schema: { $ref: "#/definitions/SignInBody" }
        }
        */
        config.versionCheck,
        body('npm').isNumeric(),
        body('version').isLength({min: 1, max: 15}),
        body('password').whitelist(['abcdef','0123456789']),
    ], controller.signin);

    app.post("/api/auth/refreshtoken", [
        config.versionCheck,
        body('version').isLength({min: 1, max: 15})
    ], controller.refreshToken);

    app.post("/api/auth/changePassword", [
        /*
        #swagger.tags = ['Auth']
        #swagger.description = 'Change user password'
        #swagger.parameters['Change user password body'] = {
            in: 'body', required: true, type: 'string',
            description: 'user old and new password sha1 digest',
            schema: { $ref: "#/definitions/PasswordChangeBody" }
        }
        #swagger.security = [{
            "bearerAuth": []
        }]
        */
        authJwt.verifyToken,
        body('password').whitelist(['abcdef','0123456789']),
        body('newpassword').whitelist(['abcdef','0123456789'])
    ], controller.changePassword);

    app.post("/api/auth/resetPassword", [
        /*
        #swagger.tags = ['Auth']
        #swagger.description = 'Reset user password'
        #swagger.parameters['Reset user password body'] = {
            in: 'body', required: true, type: 'string',
            description: 'user npm',
            schema: { $ref: "#/definitions/PasswordResetBody" }
        }
        */
        authJwt.verifyToken,
        body('npm').isNumeric()
    ], controller.resetPassword);
};
