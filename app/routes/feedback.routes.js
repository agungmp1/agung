const controller = require("../controllers/feedback.controller");
const { authJwt, admin } = require("../middleware");
const { body, query } = require('express-validator')

module.exports = function (app) {
    app.use(function (req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.post("/api/feedback/create", [
        authJwt.verifyToken,
        body('body').isLength({ min: 1 }),
        body('targetNpm').isNumeric(),
        body('titleId').isNumeric(),
        body('rating').isFloat(),
    ], controller.create);

    app.get("/api/feedback/all", [
        authJwt.verifyToken,
        admin.isFeedbackAdmin
    ], controller.all);

    app.post("/api/feedback/clear", [
        authJwt.verifyToken,
        admin.isFeedbackAdmin
    ], controller.clear);

    app.get("/api/feedback/download", [
        authJwt.verifyToken,
        admin.isFeedbackAdmin
    ], controller.download);
};
