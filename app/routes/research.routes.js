const controller = require("../controllers/research.controller");
const { authJwt, admin } = require("../middleware");
const { body, query } = require('express-validator')

module.exports = function(app) {
    app.use(function(req, res, next) {
        res.header(
            "Access-Control-Allow-Headers",
            "x-access-token, Origin, Content-Type, Accept"
        );
        next();
    });

    app.get("/api/research/all", [authJwt.verifyToken], controller.all);

    app.get("/api/research/get", [
        authJwt.verifyToken,
        admin.isResearchAdmin,
        query('id').isNumeric()
    ], controller.get);

    app.post("/api/research/create", [
        authJwt.verifyToken,
        admin.isResearchAdmin,
        body('title').isLength({ min: 1, max:255}),
        body('conference').isLength({ min: 1, max:255 }),
        body('researcher').isLength({ min: 1, max:255 }),
        body('year').isNumeric(),
        body('type').isLength({ min: 1, max:120 }),
        body('publisher').isLength({ min: 1, max:140 })
    ], controller.create);

    app.post("/api/research/edit", [
        authJwt.verifyToken,
        admin.isResearchAdmin,
        body('id').isNumeric(),
        body('title').isLength({ min: 1, max:100 }),
        body('conference').isLength({ min: 1, max:100 }),
        body('researcher').isLength({ min: 1, max:140 }),
        body('year').isNumeric(),
        body('type').isLength({ min: 1, max:120 }),
        body('publisher').isLength({ min: 1, max:140 })
    ], controller.edit);

    app.post("/api/research/destroy", [
        authJwt.verifyToken,
        admin.isResearchAdmin,
        body('id').isNumeric()
    ], controller.destroy);

    app.post("/api/research/clear", [
        authJwt.verifyToken,
        admin.isResearchAdmin
    ], controller.clear);
};
