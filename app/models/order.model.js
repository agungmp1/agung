module.exports = (sequelize, Sequelize) => {
    const order = sequelize.define("orders", {
        subtotal: {
            type: Sequelize.INTEGER,
            allowNull: false
        },
        quantity: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    });

    return order;
};
