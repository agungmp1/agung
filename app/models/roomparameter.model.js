module.exports = (sequelize, Sequelize) => {
    const RoomParameter = sequelize.define("room_parameters", {
        name: {
            type: Sequelize.STRING
        },
        point: {
            type: Sequelize.INTEGER
        }
    }, { timestamps: false });

    return RoomParameter;
};
