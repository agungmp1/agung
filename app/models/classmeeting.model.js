module.exports = (sequelize, Sequelize) => {
    const ClassMeeting = sequelize.define("class_meetings", {
        lecture: {
            type: Sequelize.STRING
        },
        date: {
            type: Sequelize.DATEONLY
        },
        meetingnumber: {
            type: Sequelize.INTEGER
        },
        resume: {
            type: Sequelize.STRING
        },
        coursework: {
            type: Sequelize.STRING
        }
    });

    return ClassMeeting;
};
