module.exports = (sequelize, Sequelize) => {
    const User = sequelize.define("users", {
        npm: {
            type: Sequelize.STRING,
            unique: true,
            primaryKey: true
        },
        fullname: {
            type: Sequelize.STRING
        },
        classname: {
            type: Sequelize.STRING
        },
        bio: {
            type: Sequelize.TEXT
        },
        nickname: {
            type: Sequelize.STRING
        },
        sportclub: {
            type: Sequelize.STRING
        },
        artclub: {
            type: Sequelize.STRING
        },
        techclub: {
            type: Sequelize.STRING
        },
        highschool: {
            type: Sequelize.STRING
        },
        birthdate: {
            type: Sequelize.STRING
        },
        sanapati: {
            type: Sequelize.STRING
        },
        password: {
            type: Sequelize.STRING
        },
        coin: {
            type: Sequelize.INTEGER,
            defaultValue: 0,
            allowNull: false
        },
        health_point: {
            type: Sequelize.INTEGER,
            defaultValue: 10,
            allowNull: false
        },
        hp_lastupdate: {
            type: Sequelize.DATE
        },
        password_digitalsignature: {
            type: Sequelize.STRING
        },
        ctf_point: {
            type: Sequelize.INTEGER,
            defaultValue: 0,
            allowNull: false
        }
    });

    return User;
};
