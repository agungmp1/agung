module.exports = (sequelize, Sequelize) => {
    const FCMReToken = sequelize.define("fcmregistrationtokens", {
        token: {
            type: Sequelize.STRING,
            unique: true
        },
    });
    return FCMReToken;
};
