module.exports = (sequelize, Sequelize) => {
    const Bureau = sequelize.define("bureaus", {
        name: {
            type: Sequelize.STRING
        }
    }, { timestamps: false });

    return Bureau;
};
