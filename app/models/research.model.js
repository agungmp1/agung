module.exports = (sequelize, Sequelize) => {
    const Research = sequelize.define("researchs", {
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        conference: {
            type: Sequelize.STRING,
            allowNull: false
        },
        researcher: {
            type: Sequelize.STRING,
            allowNull: false
        },
        year: {
            type: Sequelize.INTEGER,
            allowNull: true
        },
        type: {
            type: Sequelize.STRING,
            allowNull: true
        },
        publisher: {
            type: Sequelize.STRING,
            allowNull: true
        }
    }, { timestamps: false });

    return Research;
};
