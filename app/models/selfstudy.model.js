module.exports = (sequelize, Sequelize) => {
    const SelfStudy = sequelize.define("selfstudies", {
        has_homework: {
            type: Sequelize.BOOLEAN
        },
        homework_percentage: {
            type: Sequelize.INTEGER
        },
        resume: {
            type: Sequelize.STRING
        },
    });

    return SelfStudy;
};
