module.exports = (sequelize, Sequelize) => {
    const BookLending = sequelize.define("book_lendings", {
        status: {
            type: Sequelize.ENUM(['BOOKED', 'ON GOING', 'DONE']),
            allowNull: false,
            defaultValue: 'BOOKED'
        }
    });

    return BookLending;
};
