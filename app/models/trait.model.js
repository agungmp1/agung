module.exports = (sequelize, Sequelize) => {
    const Trait = sequelize.define("traits", {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        }
    }, { timestamps: false });

    return Trait;
};
