module.exports = (sequelize, Sequelize) => {
    const Feedback = sequelize.define("feedbacks", {
        body: {
            type: Sequelize.TEXT,
            allowNull: false
        },
        rating: {
            type: Sequelize.DOUBLE,
            allowNull: false
        },
        userNpm: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        targetNpm: {
            type: Sequelize.STRING,
            allowNull: false,
        }
    });

    return Feedback;
};
