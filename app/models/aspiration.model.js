module.exports = (sequelize, Sequelize) => {
    const Aspiration = sequelize.define("aspirations", {
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        argument: {
            type: Sequelize.STRING,
            allowNull: false
        },
        suggestion: {
            type: Sequelize.STRING,
            allowNull: false
        },
        proof: {
            type: Sequelize.STRING,
            allowNull: false
        },
        target: {
            type: Sequelize.ENUM(['Penyelenggara', 'Biro Perencanaan Ketarunaan Humas dan Protokol', 'Biro Umum', 'Biro Rohani', 'Biro Akademik', 'Biro Olahraga', 'Biro Kesenian', 'Biro Perniagaan', 'Biro Kesejahteraan Taruna', 'Biro Sosial dan Lingkungan', 'Biro Asrama Putra', 'Biro Asrama Putri', 'Biro Pengembangan Teknologi', 'Biro Siber', 'Seksi Hukum dan Tata Usaha', 'Seksi Keamanan dan Ketertiban', 'Seksi Pembinaan Mental dan Kepribadian', 'Seksi Protokol dan Tradisi', 'Biro Administrasi dan Keuangan', 'Biro Operasional dan Media Informasi', 'Komisi I Aspirasi', 'Komisi II Pengawasan Kinerja', 'Komisi III Pengawasan Program Kerja', 'Komisi IV Audit']),
            allowNull: false
        },
        isAnonymous: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        isAccepted: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        },
        isActive: {
            type: Sequelize.BOOLEAN,
            defaultValue: false,
            allowNull: false
        }
    });

    return Aspiration;
};
