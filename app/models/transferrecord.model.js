module.exports = (sequelize, Sequelize) => {
    const TransferRecord = sequelize.define("transfer_records", {
        targetNpm: {
            type: Sequelize.STRING,
            allowNull: false
        },
        value: {
            type: Sequelize.INTEGER,
            allowNull: false
        }
    });

    return TransferRecord;
};
