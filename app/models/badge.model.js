module.exports = (sequelize, Sequelize) => {
    const Badge = sequelize.define("badges", {
        name: {
            type: Sequelize.STRING(126).BINARY,
            unique: true
        },
        iconId: {
            type: Sequelize.INTEGER,
            allowNull: false,
            defaultValue: 1
        }
    }, { timestamps: false });
    return Badge;
};
