module.exports = (sequelize, Sequelize) => {
    const Class = sequelize.define("classes", {
        name: {
            type: Sequelize.STRING
        },
        shortname: {
            type: Sequelize.STRING
        },
        notes: {
            type: Sequelize.STRING
        },
    });

    return Class;
};
