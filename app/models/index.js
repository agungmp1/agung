const config = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  config.DB,
  config.USER,
  config.PASSWORD, {
  host: config.HOST,
  dialect: config.dialect,
  dialectOptions: config.dialectOptions,
  timezone: config.timezone,
  logging: false,
  define: {
    collate: config.collate,
    charset: config.charset,
  }
}
);

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.config = require("../models/config.model.js")(sequelize, Sequelize);
db.user = require("../models/user.model.js")(sequelize, Sequelize);
db.trait = require("../models/trait.model.js")(sequelize, Sequelize);
db.role = require("../models/role.model.js")(sequelize, Sequelize);
db.item = require("../models/item.model.js")(sequelize, Sequelize);
db.order = require("../models/order.model.js")(sequelize, Sequelize);
db.transactionrecord = require("../models/transactionrecord.model.js")(sequelize, Sequelize);
db.transferrecord = require("../models/transferrecord.model.js")(sequelize, Sequelize);
db.topuprequest = require("../models/topuprequest.model.js")(sequelize, Sequelize);
db.pendingtoken = require("../models/pendingtoken.model.js")(sequelize, Sequelize);
db.title = require("../models/title.model.js")(sequelize, Sequelize);
db.bureau = require("../models/bureau.model.js")(sequelize, Sequelize);
db.organization = require("../models/organization.model.js")(sequelize, Sequelize);
db.room = require("../models/room.model.js")(sequelize, Sequelize);
db.roomparameter = require("../models/roomparameter.model.js")(sequelize, Sequelize);
db.building = require("../models/building.model.js")(sequelize, Sequelize);
db.locus = require("../models/locus.model.js")(sequelize, Sequelize);
db.fastingrecord = require("../models/fastingrecord.model.js")(sequelize, Sequelize);
db.dayoffrecord = require("../models/dayoffrecord.model.js")(sequelize, Sequelize);
db.achievementrecord = require("../models/achievementrecord.model.js")(sequelize, Sequelize);
db.icon = require("../models/icon.model.js")(sequelize, Sequelize);
db.foul = require("../models/foul.model.js")(sequelize, Sequelize);
db.foulrecord = require("../models/foulrecord.model.js")(sequelize, Sequelize);
db.research = require("../models/research.model.js")(sequelize, Sequelize);
db.peleton = require("../models/peleton.model.js")(sequelize, Sequelize);
db.class = require("../models/class.model.js")(sequelize, Sequelize);
db.classmeeting = require("../models/classmeeting.model.js")(sequelize, Sequelize);
db.selfstudy = require("../models/selfstudy.model.js")(sequelize, Sequelize);
db.jdih = require("../models/jdih.model.js")(sequelize, Sequelize);
db.aspiration = require("../models/aspiration.model.js")(sequelize, Sequelize);
db.vote = require("../models/vote.model.js")(sequelize, Sequelize);
db.ctfchallenge = require("../models/ctfchallenge.model.js")(sequelize, Sequelize);
db.ctfcategory = require("../models/ctfcategory.model.js")(sequelize, Sequelize);
db.ctflevel = require("../models/ctflevel.model.js")(sequelize, Sequelize);
db.ctfchallengecompletion = require("../models/ctfchallengecompletion.model.js")(sequelize, Sequelize);
db.fcmretoken = require("../models/fcmretoken.model.js")(sequelize, Sequelize);
db.feedback = require("../models/feedback.model.js")(sequelize, Sequelize);
db.book = require("../models/book.model.js")(sequelize, Sequelize);
db.booklending = require("../models/booklending.model.js")(sequelize, Sequelize);

const UserTitle = sequelize.define('user_titles', {}, { timestamps: false });
db.usertitle = UserTitle;
db.user.belongsToMany(db.title, { through: UserTitle });
db.title.belongsToMany(db.user, { through: UserTitle });

const UserTrait = sequelize.define('user_traits', {}, { timestamps: false });
db.usertrait = UserTrait;
db.user.belongsToMany(db.trait, { through: UserTrait });
db.trait.belongsToMany(db.user, { through: UserTrait });

db.bureau.hasMany(db.title);
db.title.belongsTo(db.bureau);

db.organization.hasMany(db.bureau);
db.bureau.belongsTo(db.organization);

db.room.hasMany(db.user);
db.user.belongsTo(db.room);

db.building.hasMany(db.room);
db.room.belongsTo(db.building);

db.locus.hasMany(db.building);
db.building.belongsTo(db.locus);

const RoomViolation = sequelize.define('room_violations', {}, { timestamps: false });
db.roomviolation = RoomViolation;
db.room.belongsToMany(db.roomparameter, { through: RoomViolation });
db.roomparameter.belongsToMany(db.room, { through: RoomViolation });

db.user.hasOne(db.fastingrecord);
db.fastingrecord.belongsTo(db.user);

db.item.hasMany(db.order);
db.order.belongsTo(db.item);

db.transactionrecord.hasMany(db.order);
db.order.belongsTo(db.transactionrecord);

db.user.hasMany(db.transactionrecord, {
  foreignKey: 'npm'
});
db.transactionrecord.belongsTo(db.user, {
  foreignKey: 'npm'
});

db.user.hasMany(db.transferrecord, {
  foreignKey: 'targetNpm'
});
db.transferrecord.belongsTo(db.user, {
  foreignKey: 'targetNpm'
});

db.transactionrecord.hasOne(db.transferrecord);
db.transferrecord.belongsTo(db.transactionrecord);

db.transactionrecord.hasOne(db.topuprequest);
db.topuprequest.belongsTo(db.transactionrecord);

db.transactionrecord.hasOne(db.pendingtoken);
db.pendingtoken.belongsTo(db.transactionrecord);

const User_Roles = sequelize.define('user_roles', {}, { timestamps: false });
db.user.belongsToMany(db.role, { through: User_Roles });
db.role.belongsToMany(db.user, { through: User_Roles });

db.bureau.hasMany(db.achievementrecord);
db.achievementrecord.belongsTo(db.bureau);

db.user.hasMany(db.achievementrecord);
db.achievementrecord.belongsTo(db.user);

db.user.hasMany(db.foulrecord, {
  foreignKey: 'npm'
});
db.foulrecord.belongsTo(db.user, {
  foreignKey: 'npm'
});

db.foul.hasMany(db.foulrecord);
db.foulrecord.belongsTo(db.foul);

db.ctfcategory.hasMany(db.ctfchallenge);
db.ctfchallenge.belongsTo(db.ctfcategory);

db.ctflevel.hasMany(db.ctfchallenge);
db.ctfchallenge.belongsTo(db.ctflevel);

db.user.hasMany(db.ctfchallengecompletion);
db.ctfchallengecompletion.belongsTo(db.user);

db.ctfchallenge.hasMany(db.ctfchallengecompletion);
db.ctfchallengecompletion.belongsTo(db.ctfchallenge);

db.user.hasMany(db.aspiration);
db.aspiration.belongsTo(db.user);

db.aspiration.hasMany(db.vote);
db.vote.belongsTo(db.aspiration);

db.user.hasMany(db.vote);
db.vote.belongsTo(db.user);

db.icon.hasMany(db.achievementrecord);
db.achievementrecord.belongsTo(db.icon);

db.class.hasMany(db.user);
db.user.belongsTo(db.class);

db.peleton.hasMany(db.user);
db.user.belongsTo(db.peleton);

db.class.hasMany(db.classmeeting);
db.classmeeting.belongsTo(db.class);

db.user.hasMany(db.selfstudy);
db.selfstudy.belongsTo(db.user);

db.user.hasOne(db.dayoffrecord);
db.dayoffrecord.belongsTo(db.user);

db.user.hasMany(db.fcmretoken);
db.fcmretoken.belongsTo(db.user);

db.feedback.belongsTo(db.user, { as: 'sender', foreignKey: 'userNpm' });
db.feedback.belongsTo(db.user, { as: 'target', foreignKey: 'targetNpm' });
db.feedback.belongsTo(db.title);

db.user.hasMany(db.booklending);
db.booklending.belongsTo(db.user);

db.book.hasMany(db.booklending);
db.booklending.belongsTo(db.book);

db.building.hasMany(db.book);
db.book.belongsTo(db.building);

module.exports = db;
