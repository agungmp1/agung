module.exports = (sequelize, Sequelize) => {
    const JDIH = sequelize.define("jdihs", {
        name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        file: {
            type: Sequelize.STRING,
            allowNull: false
        }
    });

    return JDIH;
};
