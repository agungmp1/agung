const db = require("../models");
const User = db.user;
const Room = db.room;
const Building = db.building;

dormCheck = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: { model: Room, include: Building }
    }).then(user => {
        if (!user.roomId) {
            return res.status(403).send({
                message: "Please register your room!"
            });
        }
        req.buildingId = user.room.buildingId
        req.building = user.room.building
        next();
    });
};

const filter = {
    dormCheck: dormCheck
};
module.exports = filter;
