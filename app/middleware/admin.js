const db = require("../models");
const User = db.user;
const Role = db.role;
const Config = db.config;

isSuperAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 8) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isFastingAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 1) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isCommerceAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 2) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isCoinAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 3) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isFoulAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 4) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

hasFoulPassword = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        Config.findOne({ where: { id: 1 } }).then(config => {
            let isAdmin = false;
            user.roles.forEach((role) => {
                if (role.id == 4) {
                    isAdmin = true;
                }
            });
            passwordInp = (req.body.password ? req.body.password : (req.query.password ? req.query.password : ''))
            if (!isAdmin && passwordInp !== config.foulpassword) {
                return res.status(403).send({
                    message: "Unsufficient privilege!"
                });
            }
            next();
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    });
};

isResearchAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 5) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isJDIHAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 6) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isCTFAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 7) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isAspirationAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 9) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isRoomAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 10) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isAchievementAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 11) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isClassAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 12) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isDayOffAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 13) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isFeedbackAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 14) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

isLibraryAdmin = (req, res, next) => {
    User.findOne({
        where: {
            npm: req.npm
        },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 15) {
                isAdmin = true;
            }
        });

        if (!isAdmin) {
            return res.status(403).send({
                message: "Unsufficient privilege!"
            });
        }
        next();
    });
};

const admin = {
    isSuperAdmin: isSuperAdmin,
    isFastingAdmin: isFastingAdmin,
    isCommerceAdmin: isCommerceAdmin,
    isFoulAdmin: isFoulAdmin,
    hasFoulPassword: hasFoulPassword,
    isResearchAdmin: isResearchAdmin,
    isJDIHAdmin: isJDIHAdmin,
    isCTFAdmin: isCTFAdmin,
    isAspirationAdmin: isAspirationAdmin,
    isRoomAdmin: isRoomAdmin,
    isAchievementAdmin: isAchievementAdmin,
    isClassAdmin: isClassAdmin,
    isFeedbackAdmin: isFeedbackAdmin,
    isDayOffAdmin: isDayOffAdmin,
    isLibraryAdmin: isLibraryAdmin,
    isCoinAdmin: isCoinAdmin
};
module.exports = admin;
