const db = require("../models");
const Config = db.config;

fastingIsOpen = (req, res, next) => {
    Config.findOne({ where: { id: 1 } }).then(config => {
        if (!config.fastingopen) {
            return res.status(403).send({
                message: "Route is closed!"
            });
        }
        next();
    });
};

dayoffIsOpen = (req, res, next) => {
    Config.findOne({ where: { id: 1 } }).then(config => {
        if (!config.dayoffopen) {
            return res.status(403).send({
                message: "Route is closed!"
            });
        }
        next();
    });
};

commerceIsOpen = (req, res, next) => {
    let field = ([1, 2, 3].indexOf(req.buildingId) > -1) ? 'malecommerceopen' : 'femalecommerceopen';
    Config.findOne({ where: { id: 1 } }).then(config => {
        if (!config[field]) {
            return res.status(403).send({
                message: "Route is closed!"
            });
        }
        next();
    });
};

roomcheckingIsOpen = (req, res, next) => {
    Config.findOne({ where: { id: 1 } }).then(config => {
        if (!config.roomcheckingopen) {
            return res.status(403).send({
                message: "Route is closed!"
            });
        }
        next();
    });
};

versionCheck = (req, res, next) => {
    Config.findOne({ where: { id: 1 } }).then(config => {
        if (config.version != req.body.version) {
            return res.status(426).send({
                message: "Invalid version"
            });
        }
        next();
    });
};

const config = {
    fastingIsOpen: fastingIsOpen,
    commerceIsOpen: commerceIsOpen,
    roomcheckingIsOpen: roomcheckingIsOpen,
    dayoffIsOpen: dayoffIsOpen,
    versionCheck: versionCheck,
};
module.exports = config;
