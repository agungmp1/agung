module.exports = {
    HOST: "localhost",
    USER: "root",
    PASSWORD: "",
    DB: "db_karya",
    collate: "latin1_swedish_ci",
    charset: "latin1",
    dialect: "mysql",
    dialectOptions: {
        dateStrings: true,
        typeCast: true
    },
    timezone: '+07:00' //for writing to database
};