// const { Server } = require("socket.io
const socketIO = require('socket.io');

const db = require("../models");
// const jwt = require("jsonwebtoken");
// const config = require("../config/auth.config.js");

const User = db.user;
const Role = db.role;
const Item = db.item;
const Aspiration = db.aspiration;

let commerceio;

exports.attach = (server) => {
    commerceio = socketIO(server,  {
        path: '/socket/commerce',
        cors: {
            orign: 'http://localhost',
            methods: ["GET", "POST"],
            allowedHeaders: ["Access-Control-Allow-Origin", 'authorization']
        }
    });

    commerceio.on('connection', (socket) => {
        let ip = '[hidden ip]'
        if (socket.conn.remoteAddress) {
            if (socket.conn.remoteAddress.split(':').length==4) {
                ip = socket.conn.remoteAddress.split(':')[3];
            }
        }
        console.log(`[${new Date()}] connection request on commerce socket from ${ip}`);
        Item.findAll({ attributes: ['id', 'name', 'price', 'stock'] }).then( items => {
            console.log(`[${new Date()}] user connected to commerce socket from ${ip}`);
            socket.emit('connected', {status: 200, data: items, message: 'you are listening for commerce events'})
            socket.join("commerce")
        }).catch(err => {
            console.log(err);
            socket.emit('connected', {status: 500, message: err});
        });
    })
}

exports.emitdata = (eventname, data) =>{
    commerceio.sockets.in("commerce").emit(eventname, data)
}
