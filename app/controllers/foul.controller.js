const db = require("../models");
const User = db.user;
const Class = db.class;
const Foul = db.foul;
const FoulRecord = db.foulrecord;
const Role = db.role;
const sequelize = db.sequelize;
const Sequelize = db.Sequelize;

const moment = require('moment')
const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

exports.recap = (req, res) => { //monthly recap
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    const month = req.query.monthyear.substring(5, 7)
    const year = parseInt(req.query.monthyear.substring(0, 4))
    const date = (['01', '03', '05', '07', '08', '10', '12'].indexOf(month) > -1) ? '31' : (month == '02' ? (year % 4 == 0 ? '29' : '28') : '30')
    // console.log(month, (month in ['01','03','05','07','08','10','12']), date);

    FoulRecord.findAll({
        where: {
            date: {
                [Op.gte]: `${req.query.monthyear}-01`,
                [Op.lte]: `${req.query.monthyear}-${date}`
            }
        }
    }).then(userfouls => {
        let recap = {
            satria: {
                foul1: 0, foul2: 0, foul3: 0, foul4: 0, foul5: 0, foul6: 0
            },
            madya: {
                foul1: 0, foul2: 0, foul3: 0, foul4: 0, foul5: 0, foul6: 0
            },
            muda: {
                foul1: 0, foul2: 0, foul3: 0, foul4: 0, foul5: 0, foul6: 0
            },
            pratama: {
                foul1: 0, foul2: 0, foul3: 0, foul4: 0, foul5: 0, foul6: 0
            }
        }
        userfouls.forEach((record) => {
            let year = record.npm.substring(0, 2);
            level = year == '18' ? 'satria' : (year == '19' ? 'madya' : (year == '20' ? 'muda' : 'pratama'))
            let foulId = record.foulId;
            if (foulId > 6 && foulId < 13) {
                foulId = 5
            }
            recap[level][`foul${foulId}`]++;
        });
        res.status(200).send({ data: recap });
        // res.status(200).send({ data: recap, debug: userfouls, date: date, month: month });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.linegraph = (req, res) => { //monthly recap
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    const now = new Date();
    const year = now.getMonth() > 10 ? now.getFullYear() : now.getFullYear() - 1;

    FoulRecord.findAll({
        where: {
            date: {
                [Op.gte]: `${year}-10-01`,
                [Op.lte]: `${year + 1}-09-30`
            }
        }
    }).then(fouls => {
        let recap = {
            satria: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            madya: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            muda: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            pratama: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        }
        fouls.forEach((record) => {
            let year = record.npm.substring(0, 2);
            level = year == '18' ? 'satria' : (year == '19' ? 'madya' : (year == '20' ? 'muda' : 'pratama'))
            recap[level][parseInt(record.date.substring(5, 7)) - 1]++;
        });
        res.status(200).send({ data: recap });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.all = (req, res) => {
    FoulRecord.findAll({
        include: [{
            model: User,
            attributes: ['npm', 'fullname'],
            include: { model: Class }
        }, {
            model: Foul,
            attributes: ['id', 'name', 'point']
        }],
        order: [['date', 'DESC']]
    }).then(userfouls => {
        let toSend = []
        userfouls.forEach((record) => {
            record.user.class = record.user.class.name
            if (record.npm.substring(0, 2) >= req.npm.substring(0, 2)) {
                toSend.push(record);
            }
        });

        res.status(200).send({ data: toSend });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.getmyfouls = (req, res) => {
    FoulRecord.findAll({
        where: { npm: req.npm },
        include: {
            model: Foul,
            attributes: ['id', 'name']
        },
        order: [['date', 'DESC']]
    }).then(records => {
        let recap = {
            foul1: 0, foul2: 0, foul3: 0, foul4: 0, foul5: 0, foul6: 0
        }
        records.forEach((record) => {
            let foulId = record.foulId;
            if (foulId > 6 && foulId < 13) {
                foulId = 5
            }
            recap[`foul${foulId}`]++;
        });
        User.findOne({
            where: { npm: req.npm },
            include: {
                model: Role,
                attributes: ['id']
            }
        }).then(user => {
            let isAdmin = false;
            user.roles.forEach((role) => {
                if (role.id == 4) {
                    isAdmin = true;
                }
            });
            res.status(200).send({ data: records, recap: recap, isAdmin: isAdmin });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

const bot = require('../utils/telebot.js')

exports.add = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    FoulRecord.create({
        npm: req.body.npm,
        foulId: req.body.foulId,
        date: req.body.date
    }).then(x => {
        Foul.findOne({ where: { id: req.body.foulId } }).then(foul => {
            User.update({
                health_point: Sequelize.literal(`(\`health_point\` - ${foul.point})`),
                hp_lastupdate: req.body.date
            }, { where: { npm: req.body.npm } }).then(y => {
                console.log(`[foul input][${new Date()}] ${req.npm} input ${req.body.npm}:${req.body.foulId}:${req.body.date}, -${foul.point}`);
                res.status(200).send({ message: "Fouls added" });
                User.findOne({ where: { npm: req.body.npm } }).then(user => {
                    if (user.health_point <= 0) {
                        console.log(`[user death][${new Date()}] ${user.npm} is dead`);
                        bot.sendDeathMessage(user.fullname)
                    }
                }).catch(err => {
                    console.log(err);
                    res.status(500).send({ message: err.message });
                });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.destroy = (req, res) => {
    FoulRecord.destroy({ where: { id: req.body.id } }).then(data => {
        console.log(`[foul delete][${new Date()}] ${req.npm} delete foul record#${req.body.id}`);
        res.status(200).send({ message: "Destroyed successfully!" });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

const zip = require('express-zip');
const csvwriter = require("csv-writer");
const createCsvWriter = csvwriter.createObjectCsvWriter;
exports.download = (req, res) => { //monthly recap
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    const month = parseInt(req.query.monthyear.substring(5, 7))
    const year = parseInt(req.query.monthyear.substring(0, 4))
    const date = month in [1, 3, 5, 7, 8, 10, 12] ? '31' : (month == 2 ? (year % 4 == 0 ? '29' : '28') : '30')

    FoulRecord.findAll({
        where: {
            date: {
                [Op.gte]: `${req.query.monthyear}-01`,
                [Op.lte]: `${req.query.monthyear}-${date}`
            }
        },
        include: {
            model: Foul,
            attributes: ['id', 'name']
        }
    }).then(userfouls => {
        let recap = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]]
        let toSend = []
        userfouls.forEach((record) => {
            let year = record.npm.substring(0, 2);
            level = year == '18' ? 0 : (year == '19' ? 1 : (year == '20' ? 2 : 3))
            recap[level][parseInt(record.foulId) - 1]++;
        });
        recap.forEach((level, index) => {
            levelString = index == 0 ? 'Taruna Satria' : (index == 1 ? 'Taruna Madya' : (index == 2 ? 'Taruna Muda' : 'Taruna Pratama'))
            level.forEach((foul, index2) => {
                toSend.push({
                    level: levelString, foulId: index2 + 1, count: recap[index][index2]
                })
            })
        })
        const csvWriter = createCsvWriter({
            path: `${__dirname}/../export/foul/datapelanggaran_${req.query.monthyear}.csv`,
            header: [
                { id: "level", title: "level" },
                { id: "foulId", title: "foulId" },
                { id: "count", title: "count" },
            ]
        });
        csvWriter.writeRecords(toSend).then(() => {
            const file = `${__dirname}/../export/foul/datapelanggaran_${req.query.monthyear}.csv`;
            console.log(`[foul download][${new Date()}] ${req.npm} download foul record of${req.query.monthyear}`);
            res.zip([{
                path: file,
                name: `datapelanggaran_${req.query.monthyear}.csv`
            }]);
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.getdeadusers = (req, res) => {
    User.findAll({
        where: { health_point: { [Op.lte]: 0 } },
        attributes: ['npm', 'fullname'],
        include: Class
    }).then(users => {
        let data = users.map(user => {
            return {
                npm: user.npm,
                fullname: user.fullname,
                class: user.class.name,
            }
        })
        res.status(200).send({ data: data });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.revive = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.update({
        health_point: 10
    }, { where: { npm: req.body.npm } }).then(data => {
        console.log(`[user revive][${new Date()}] ${req.npm} revive ${req.body.npm}`);
        res.status(200).send({ message: "User revived successfully!" });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.autoheal = (req, res) => {
    User.update({
        health_point: Sequelize.literal(`(\`health_point\` + 1)`),
        hp_lastupdate: moment().toDate()
    }, {
        where: {
            [Op.and]: [{
                hp_lastupdate: {
                    [Op.lte]: moment().subtract(14, 'days').toDate()
                }
            }, {
                health_point: {
                    [Op.lte]: 9
                }
            }]
        }
    }).then(data => {
        console.log(`[user auto heal][${new Date()}]`);
    }).catch(err => {
        console.log(err);
    });
}

exports.cancelfoul = (req, res) => {
    FoulRecord.findAll({
        where: {
            [Op.and]: [{
                foulId: [7, 8, 9]
            }, {
                date: '2022-03-26'
            }]
        }, include: {
            model: Foul
        }
    }).then(records => {
        records.forEach((record) => {
            User.update({
                health_point: Sequelize.literal(`(\`health_point\` + ${record.foul.point})`)
            }, {
                where: {
                    npm: record.npm
                }
            }).then(data => {
                FoulRecord.destroy({ where: { id: record.id } }).then(data => {
                    console.log(`[foul cancel][${new Date()}] cancel foul record#${record.foulId}:${record.npm}:${record.date}`);
                }).catch(err => {
                    console.log(err);
                });
            }).catch(err => {
                console.log(err);
            });
        });
    }).catch(err => {
        console.log(err);
    });
}
