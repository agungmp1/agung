const db = require("../models");
const User = db.user;
const Research = db.research;
const Role = db.role;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

exports.all = (req, res) => {
    Research.findAll().then( researchs => {
        User.findOne({
            where: {npm: req.npm},
            include: {
                model: Role,
                attributes: ['id']
            }
        }).then( user => {
            let isAdmin = false;
            user.roles.forEach((role) => {
                if (role.id == 5){
                    isAdmin = true;
                }
            });
            res.status(200).send({
                data: researchs,
                isAdmin: isAdmin
            });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.get = (req, res) => {
    Research.findOne({
        where: {id: req.query.id}
    }).then( research => {
        res.status(200).send(research);
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.create = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Research.create({
        title: req.body.title,
        conference: req.body.conference,
        researcher: req.body.researcher,
        year: req.body.year,
        type: req.body.type,
        publisher: req.body.publisher
    }).then(research => {
        console.log(`[research add][${new Date()}] ${req.npm} add new research#${research.id}`);
        res.status(200).send({ message: "Research created successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.edit = (req, res) => {
    Research.update({
        title: req.body.title,
        conference: req.body.conference,
        researcher: req.body.researcher,
        year: req.body.year,
        type: req.body.type,
        publisher: req.body.publisher
    },{ where: { id: req.body.id } }).then( research => {
        console.log(`[research edit][${new Date()}] ${req.npm} edit research#${req.body.id}`);
        res.status(200).send({ message: 'Updated successfully'});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.destroy = (req, res) => {
    Research.destroy({ where: {id: req.body.id}}).then( data => {
        console.log(`[research delete][${new Date()}] ${req.npm} delete research#${req.body.id}`);
        res.status(200).send({ message: "Destroyed successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}

exports.clear = (req, res) => {
    Research.destroy({ where: {}}).then( data => {
        console.log(`[research clear][${new Date()}] ${req.npm} clear research data`);
        res.status(200).send({ message: "Cleared successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}
