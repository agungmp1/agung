const db = require("../models");
const { sendNotificationToAll } = require('../utils/fcm.js')
const Config = db.config;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

exports.openfasting = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Config.update({
        fastingopen: true,
        fastingdate: req.body.fastingdate
    }, { where: { id: 1 } }).then(user => {
        console.log(`[fasting open][${new Date()}] ${req.npm} open fasting regist`);
        sendNotificationToAll({
            notification: {
                title: 'Fasting Opened!',
                body: 'Fasting registration is opened! Go register yourself'
            }
        })
        res.status(200).send({ message: "Opened successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.closefasting = (req, res) => {
    Config.update({
        fastingopen: false
    }, { where: { id: 1 } }).then(user => {
        console.log(`[fasting close][${new Date()}] ${req.npm} close fasting regist`);
        sendNotificationToAll({
            notification: {
                title: 'Fasting Closed!',
                body: 'Fasting registration is closed!'
            }
        })
        res.status(200).send({ message: "Closed successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.opencommerce = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    let gender = ([1, 2, 3].indexOf(req.buildingId) > -1) ? 'm' : 'f';
    Config.update(gender == 'm' ? {
        malecommerceopen: true
    } : {
        femalecommerceopen: true
    }, { where: { id: 1 } }).then(user => {
        console.log(`[commerce open][${new Date()}] ${req.npm} open commerce service (${gender})`);
        sendNotificationToAll({
            notification: {
                title: `${gender == 'm' ? 'Male' : 'Female'} Commerce Opened!`,
                body: `Go buy your needs!`
            }
        })
        res.status(200).send({ message: "Opened successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.closecommerce = (req, res) => {
    let gender = ([1, 2, 3].indexOf(req.buildingId) > -1) ? 'm' : 'f';
    Config.update(gender == 'm' ? {
        malecommerceopen: false
    } : {
        femalecommerceopen: false
    }, { where: { id: 1 } }).then(user => {
        console.log(`[commerce close][${new Date()}] ${req.npm} close commerce service (${gender})`);
        sendNotificationToAll({
            notification: {
                title: `${gender == 'm' ? 'Male' : 'Female'} Commerce Closed!`,
                body: 'Ouch, see you later'
            }
        })
        res.status(200).send({ message: "Closed successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.openroomchecking = (req, res) => {
    Config.update({
        roomcheckingopen: true,
        // roomcheckingdate: req.body.checkingdate
    }, { where: { id: 1 } }).then(user => {
        console.log(`[room checking open][${new Date()}] ${req.npm} open room checking`);
        sendNotificationToAll({
            notification: {
                title: 'Fasting Opened!',
                body: 'Fasting registration is opened! Go register yourself'
            }
        })
        res.status(200).send({ message: "Opened successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.closeroomchecking = (req, res) => {
    Config.update({
        roomcheckingopen: false
    }, { where: { id: 1 } }).then(user => {
        console.log(`[room checking close][${new Date()}] ${req.npm} close room checking`);
        sendNotificationToAll({
            notification: {
                title: 'Fasting Opened!',
                body: 'Fasting registration is opened! Go register yourself'
            }
        })
        res.status(200).send({ message: "Closed successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.setroompassword = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Config.update({
        roomcheckingpassword: req.body.password
    }, { where: { id: 1 } }).then(user => {
        console.log(`[room checking password][${new Date()}] ${req.npm} change room checking password`);
        res.status(200).send({ message: "Password changed successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.getroompassword = (req, res) => {
    Config.findOne({ where: { id: 1 } }).then(config => {
        res.status(200).send({ password: config.roomcheckingpassword });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.checkroompassword = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Config.findOne({ where: { id: 1 } }).then(config => {
        res.status(200).send({ isTrue: req.body.password == config.roomcheckingpassword });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.setfoulpassword = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Config.update({
        foulpassword: req.body.password
    }, { where: { id: 1 } }).then(user => {
        console.log(`[foul password][${new Date()}] ${req.npm} change foul password`);
        res.status(200).send({ message: "Password changed successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.getfoulpassword = (req, res) => {
    Config.findOne({ where: { id: 1 } }).then(config => {
        res.status(200).send({ password: config.foulpassword });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.checkfoulpassword = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Config.findOne({ where: { id: 1 } }).then(config => {
        res.status(200).send({ isTrue: req.body.password == config.foulpassword });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.opendayoff = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Config.update({
        dayoffopen: true,
        // dayoffdate: req.body.date
    }, { where: { id: 1 } }).then(user => {
        console.log(`[dayoff open][${new Date()}] ${req.npm} open dayoff regist`);
        res.status(200).send({ message: "Opened successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.closedayoff = (req, res) => {
    Config.update({
        dayoffopen: false
    }, { where: { id: 1 } }).then(user => {
        console.log(`[dayoff close][${new Date()}] ${req.npm} close dayoff regist`);
        res.status(200).send({ message: "Closed successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.getpamongoffset = (req, res) => {
    Config.findOne({ where: { id: 1 } }).then(config => {
        res.status(200).send({ offset: config.pamongoffset });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};