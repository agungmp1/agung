const db = require("../models");
const User = db.user;
const Class = db.class;
const Trait = db.trait;
const Role = db.role;
const Bureau = db.bureau;
const Title = db.title;
const AchievementRecord = db.achievementrecord;
const Room = db.room;
const Building = db.building;
const Locus = db.locus;
const Icon = db.icon;
const Peleton = db.peleton;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

exports.getprofile = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    let npm = req.npm
    let myProfile = true
    if (req.query.npm && req.query.npm != req.npm) {
        npm = req.query.npm
        myProfile = false
    }
    User.findOne({
        where: { npm: npm },
        include: [{
            model: Room,
            attributes: ['number'],
            include: {
                model: Building,
                attributes: ['name'],
                include: {
                    model: Locus,
                    attributes: ['name']
                }
            }
        }, {
            model: Title,
            attributes: ['name']
        }, {
            model: Role,
            attributes: ['id']
        }, {
            model: AchievementRecord,
            attributes: ['id', 'name', 'bureauId', 'type', 'createdAt'],
            include: [{
                model: Bureau,
                attributes: ['name']
            }, {
                model: Icon,
                attributes: ['filename']
            }]
        }, {
            model: Trait
        }, {
            model: Class
        }, {
            model: Peleton
        }]
    }).then(user => {
        let isAchievementAdmin = false;
        let isFeedbackAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 11) {
                isAchievementAdmin = true;
            } else if (role.id == 14) {
                isFeedbackAdmin = true;
            }
        });
        let birthdate = '';
        try {
            birthdate = user.birthdate.split('/')
            birthdate = `${months[parseInt(birthdate[1]) - 1]} ${parseInt(birthdate[0])}`
        } catch (e) {
            birthdate = ''
        }
        let sanapati = []
        try {
            sanapati = user.sanapati.split(',')
            sanapati.pop()
        } catch (e) {
            sanapati = []
        }
        res.status(200).send({
            data: {
                npm: user.npm,
                fullname: user.fullname,
                class: user.class.name,
                peleton: (user.peleton) ? user.peleton.name : null,
                bio: user.bio,
                nickname: user.nickname,
                sportclub: user.sportclub,
                techclub: user.techclub,
                artclub: user.artclub,
                birthdate: birthdate,
                sanapati: sanapati,
                highschool: user.highschool,
                ctf_point: user.ctf_point,
                health_point: user.health_point,
                room: !user.room ? null : {
                    string: (user.room.building.name + ' ' + user.room.number),
                    number: user.room.number,
                    building: user.room.building.name,
                    locus: user.room.building.locuse.name
                },
                titles: user.titles,
                achievements: user.achievement_records,
                traits: user.traits,
                isAchievementAdmin: isAchievementAdmin,
                isFeedbackAdmin: isFeedbackAdmin,
                isMyProfile: myProfile
            }
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
};

exports.gethealth = (req, res) => {
    User.findOne({
        where: { npm: req.npm },
        attributes: ['healthpoint']
    }).then(user => {
        res.status(200).send({ health_point: user.health_point });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.namelist = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    let where = (req.query.samelevel == 'true') ? { npm: { [Op.like]: `${req.npm.substring(0, 2)}%` } } : null
    let include = (req.query.withtrait == 'true') ? [{ model: Trait, attributes: ['id', 'name'] }, { model: Class, attributes: ['id', 'name'] }, { model: Peleton, attributes: ['id', 'name'] }] : [{ model: Class, attributes: ['id', 'name'] }, { model: Peleton, attributes: ['id', 'name'] }]
    if (req.query.traitId) {
        Trait.findOne({ where: { id: req.query.traitId } }).then(trait => {
            trait.getUsers({ attributes: ['npm', 'fullname'] }).then(users => {
                res.status(200).send({ data: users });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    } else {
        User.findAll({
            attributes: ['npm', 'fullname', 'nickname'],
            where: where,
            include: include
        }).then(users => {
            let data = []
            users.forEach((user, i) => {
                data.push({
                    npm: user.npm,
                    fullname: user.fullname,
                    nickname: user.nickname,
                    peleton: (user.peleton) ? user.peleton.name : null,
                    class: user.class.name
                })
            });
            res.status(200).send({ data: data });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }
};

exports.update = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.update({
        roomId: req.body.room,
        bio: req.body.bio
    }, { where: { npm: req.npm } }).then(x => {
        if (req.body.traitIds) {
            traitIds = [];
            try {
                traitIds = JSON.parse(req.body.traitIds);
            } catch (error) {
                return res.status(422).send({ message: "Invalid input", error: error })
            }
            User.findOne({
                where: { npm: req.npm },
                attributes: ['npm']
            }).then(user => {
                user.setTraits(traitIds).then(x => {
                    res.status(200).send({ message: "Updated successfully!" });
                }).catch(err => {
                    console.log(err);
                    res.status(500).send({ message: err.message });
                })
            }).catch(err => {
                res.status(500).send({ message: err.message });
            });

        } else
            res.status(200).send({ message: "Updated successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

const fs = require('fs-extra');
exports.setprofilepicture = (req, res) => {
    let ext = '';
    if (req.file.mimetype == 'image/png') {
        ext = 'png'
    } else {
        return res.status(422).send({ message: "Invalid file type" })
    }
    const newfilename = `${req.npm}.${ext}`
    var src = fs.createReadStream(req.file.path);
    var dest = fs.createWriteStream(`${__dirname}/../export/static/profile/${newfilename}`);
    src.pipe(dest);
    src.on('end', function () {
        fs.unlinkSync(req.file.path);
        res.status(200).send({ message: "Updated successfully!" });
    });
    src.on('error', function (err) {
        console.log(err);
        res.status(500).send({ err: err });
    });
};

exports.digitalsignaturefile = (req, res) => {
    console.log(req.headers);
    User.findOne({
        where: { npm: req.npm },
        attributes: ['fullname']
    }).then(user => {
        let grade = '';
        if (req.npm.substring(0, 4) == '1817') {
            grade = 'satria'
        } else if (req.npm.substring(0, 4) == '1918') {
            grade = 'madya'
        } else if (req.npm.substring(0, 4) == '2019') {
            grade = 'muda';
        } else {
            grade = 'pratama';
        }
        const name = user.fullname.replace(/ /g, '_');
        const file = `${__dirname}/../export/digitalsignature/${grade}/${name}.p12`;
        console.log(file);
        res.download(file);
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

const config = require("../config/auth.config.js");
const jwt = require("jsonwebtoken");
exports.digitalsignaturefiletest = (req, res) => {
    let token = req.params.token;
    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({
                message: "Unauthorized!"
            });
        }
        User.findOne({
            where: { npm: decoded.npm },
            attributes: ['fullname']
        }).then(user => {
            let grade = '';
            if (decoded.npm.substring(0, 4) == '1817') {
                grade = 'satria'
            } else if (decoded.npm.substring(0, 4) == '1918') {
                grade = 'madya'
            } else if (decoded.npm.substring(0, 4) == '2019') {
                grade = 'muda';
            } else {
                grade = 'pratama';
            }
            const name = user.fullname.replace(/ /g, '_');
            const file = `${__dirname}/../export/digitalsignature/${grade}/${name}.p12`;
            console.log(file);
            res.download(file);
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    });
};

exports.digitalsignaturepassword = (req, res) => {
    User.findOne({
        where: { npm: req.npm },
        attributes: ['password_digitalsignature']
    }).then(user => {
        res.status(200).send({ password: user.password_digitalsignature });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.gettraits = (req, res) => {
    Trait.findAll().then(traits => {
        res.status(200).send({ data: traits });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};
