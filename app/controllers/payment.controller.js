const db = require("../models");
const User = db.user;
const Room = db.room;
const Role = db.role;
const TransactionRecord = db.transactionrecord;
const Order = db.order;
const Item = db.item;

const Op = db.Sequelize.Op;
const Sequelize = db.Sequelize;
const { validationResult } = require('express-validator');

exports.allpayment = (req, res) => {
    let buildingIdArray = ([1, 2, 3].indexOf(req.buildingId) > -1) ? [1, 2, 3] : [4, 8, 9];
    TransactionRecord.findAll({
        where: { type: "PAYMENT" },
        include: [{
            model: User,
            attributes: ['npm', 'fullname'],
            include: {
                model: Room
            }
        }, {
            model: Order,
            attributes: ['quantity', 'subtotal'],
            include: {
                model: Item,
                attributes: ['name']
            }
        }],
        order: [['createdAt', 'DESC']],
        // limit: 5,
        // offset: (req.query.limit & req.query.page)?req.query.limit * req.query.page - 1:null,
        limit: req.query.limit ? parseInt(req.query.limit) : null,
        offset: (req.query.limit & req.query.page) ? req.query.limit * (req.query.page - 1) : null
    }).then(records => {
        let data = []
        records.forEach((record) => {
            if (buildingIdArray.indexOf(record.user.room.buildingId) > -1) {
                data.push(record)
            }
        })
        res.status(200).send({ data: data });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
};

exports.accept = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    TransactionRecord.findOne({ where: { id: req.body.id } }).then(record => {
        if (record.status !== "PENDING") {
            return res.status(422).send({ message: "Rule violation!" });
        }
        TransactionRecord.update({
            status: "SUCCESS"
        }, { where: { id: req.body.id } }).then(x => {
            console.log(`[commerce payment accept][${new Date()}] ${req.npm} accepted payment request from ${record.npm}, ${record.value} coin with id ${record.id}`);
            res.status(200).send({ message: "Transaction accepted!" });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
};

exports.deny = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    TransactionRecord.findOne({
        where: { id: req.body.id },
        include: {
            model: Order,
            attributes: ['id', 'quantity'],
            include: {
                model: Item,
                attributes: ['id', 'name', 'price', 'stock']
            }
        }
    }).then(record => {
        if (record.status !== "PENDING") {
            return res.status(422).send({ message: "Rule violation!" });
        }
        let orderIds = [];
        let itemsUpdateObjects = [];
        record.orders.forEach((order) => {
            orderIds.push(order.id);
            itemsUpdateObjects.push({
                id: order.item.id,
                name: order.item.name,
                price: order.item.price,
                stock: order.item.stock + order.quantity
            });
        });
        Item.bulkCreate(itemsUpdateObjects, {
            updateOnDuplicate: ['name', 'price', 'stock']
        }).then(x => {
            Order.destroy({ where: { id: orderIds } }).then(y => {
                TransactionRecord.update({
                    status: "DECLINED"
                }, { where: { id: req.body.id } }).then(x => {
                    console.log(`[commerce payment deny][${new Date()}] ${req.npm} declined payment request from ${record.npm}, ${record.value} coin with id ${record.id}`);
                    res.status(200).send({ message: "Transaction denied!" });
                }).catch(err => {
                    res.status(500).send({ message: err.message });
                });
                // User.update({
                //     coin: Sequelize.literal(`coin + ${record.value}`)
                // },{where: {npm:record.npm}}).then(user => {
                // }).catch(err => {
                //     res.status(500).send({ message: err.message });
                // });
            }).catch(err => {
                res.status(500).send({ message: err.message });
                console.log(err);
            });
        }).catch(err => {
            res.status(500).send({ message: err.message });
            console.log(err);
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
};

exports.setdelivered = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    TransactionRecord.findOne({ where: { id: req.body.id } }).then(record => {
        if (record.status !== "SUCCESS") {
            return res.status(422).send({ message: "Rule violation!" });
        }
        TransactionRecord.update({
            status: "DELIVERED"
        }, { where: { id: req.body.id } }).then(x => {
            console.log(`[commerce item delivered][${new Date()}] ${req.npm} set order from ${record.npm}, ${record.value} coin with id ${record.id} as taken`);
            res.status(200).send({ message: "Transaction accepted!" });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
};

const csvwriter = require("csv-writer");
const createCsvWriter = csvwriter.createObjectCsvWriter
const zip = require('express-zip');
const config = require("../config/auth.config.js");

const jwt = require("jsonwebtoken");
exports.download = (req, res) => {
    TransactionRecord.findAll({
        where: {
            [Op.and]: [{
                type: "PAYMENT"
            }, {
                [Op.or]: [{
                    status: "SUCCESS"
                }, {
                    status: "DELIVERED"
                }]
            }, {
                createdAt: {
                    [Op.gte]: new Date(new Date().setDate(new Date().getDate() - 7)),
                }
            }]
        },
        include: [{
            model: User,
            attributes: ['npm', 'fullname'],
            include: Room
        }, {
            model: Order,
            attributes: ['id', 'quantity', 'subtotal'],
            include: {
                model: Item,
                attributes: ['id', 'name', 'price']
            }
        }],
        order: [['createdAt', 'DESC']]
    }).then(transactionrecords => {
        transactions = []
        orders = []
        let buildingIdArray = ([1, 2, 3].indexOf(req.buildingId) > -1) ? [1, 2, 3] : [4, 8, 9];
        transactionrecords.forEach((record) => {
            if (buildingIdArray.indexOf(record.user.room.buildingId) > -1) {
                transactions.push({
                    transaction_id: record.id,
                    npm: record.npm,
                    fullname: record.user.fullname,
                    value: record.value,
                    date: record.createdAt,
                    status: record.status
                })
                record.orders.forEach((order) => {
                    orders.push({
                        order_id: order.id,
                        npm: record.npm,
                        fullname: record.user.fullname,
                        item_id: order.item.id,
                        item_name: order.item.name,
                        item_price: order.item.price,
                        quantity: order.quantity,
                        subtotal: order.subtotal,
                        transaction_id: record.id
                    })
                });
            }
        });
        const date = Date.now().toString();
        const csvWriter_transactions = createCsvWriter({
            path: `${__dirname}/../export/commercerecord/datapembayaran_${date}.csv`,
            header: [
                { id: "transaction_id", title: "transaction id" },
                { id: "npm", title: "npm" },
                { id: "fullname", title: "fullname" },
                { id: "value", title: "value" },
                { id: "date", title: "date" },
                { id: "status", title: "status" }
            ]
        });
        csvWriter_transactions.writeRecords(transactions).then(() => {
            const csvWriter_orders = createCsvWriter({
                path: `${__dirname}/../export/commercerecord/datapesanan_${date}.csv`,
                header: [
                    { id: "order_id", title: "order id" },
                    { id: "npm", title: "npm" },
                    { id: "fullname", title: "fullname" },
                    { id: "item_id", title: "item id" },
                    { id: "item_name", title: "item name" },
                    { id: "item_price", title: "item price" },
                    { id: "quantity", title: "quantity" },
                    { id: "subtotal", title: "subtotal" },
                    { id: "transaction_id", title: "transaction id" },
                ]
            });
            csvWriter_orders.writeRecords(orders).then(() => {
                const toDownload = [
                    { path: `${__dirname}/../export/commercerecord/datapembayaran_${date}.csv`, name: `datapembayaran_${date}.csv` },
                    { path: `${__dirname}/../export/commercerecord/datapesanan_${date}.csv`, name: `datapesanan_${date}.csv` }
                ]
                console.log(`[payment download][${new Date()}] ${req.npm} download paymemt data`);
                res.zip(toDownload);
            });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
};

exports.reset = (req, res) => {
    Order.destroy({ where: {} }).then(data => {
        TransactionRecord.destroy({ where: {} }).then(data => {
            console.log(`[commerce data reset][${new Date()}] ${req.npm} clear all transaction data`);
            res.status(200).send({ message: "Transaction accepted!" });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
};
