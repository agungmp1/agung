const db = require("../models");
const User = db.user;
const TransactionRecord = db.transactionrecord;
const TransferRecord = db.transferrecord;
const TopUpRequest = db.topuprequest;
const PendingToken = db.pendingtoken;
const Order = db.order;
const Item = db.item;

const Op = db.Sequelize.Op;
const Sequelize = db.Sequelize;
const { validationResult } = require('express-validator');
const fs = require('fs-extra');

exports.getbalance = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({
        where: {npm:req.npm},
        attributes: ['coin']
    }).then(user => {
        res.status(200).send({ balance:user.coin });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.getmoneyspent = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    TransactionRecord.findAll({
        where: {npm:req.npm, type: "PAYMENT", status:["SUCCESS","PENDING"]},
        attributes: [[Sequelize.fn("SUM", Sequelize.col("value")), "moneyspent"]],
        group: ['npm']
    }).then(record => {
        // console.log(`[get coin data][${new Date()}] ${req.npm} request for get money spent`);
        res.status(200).send((record.length>0)?(record[0]) : {moneyspent: 0});
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.transfer = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({
        where: {npm:req.npm},
        attributes: ['coin']
    }).then(user => {
        if (user.coin < req.body.value) {
            return res.status(422).send({ message: "Not enough coin" })
        }
        if (req.body.value < 1 || req.body.value > 1000000) {
            return res.status(422).send({ message: "Invalid transfer value" })
        }
        if (req.body.targetNpm == req.npm) {
            return res.status(422).send({ message: "Cannot transfer to yourself" })
        }
        User.update({
            coin: Sequelize.literal(`(\`coin\` + ${req.body.value})`)
        },{where: {npm: req.body.targetNpm}}).then(user => {
            User.update({
                coin: Sequelize.literal(`(\`coin\` - ${req.body.value})`)
            },{where: {npm: req.npm}}).then(user => {
                TransactionRecord.create({
                    npm: req.npm,
                    value: req.body.value,
                    type: "TRANSFER",
                    status: "SUCCESS"
                }).then(transaction => {
                    TransferRecord.create({
                        targetNpm: req.body.targetNpm,
                        value: req.body.value,
                        transactionRecordId: transaction.id
                    }).then(record => {
                        console.log(`[coin transfer][${new Date()}] ${req.npm} send ${req.body.value} coin to ${req.body.targetNpm}`);
                        res.status(200).send({ message: "Coin transferred!" });
                    }).catch(err => {
                        res.status(500).send({ message: err.message });
                    });
                }).catch(err => {
                    res.status(500).send({ message: err.message });
                });
            }).catch(err => {
                res.status(500).send({ message: err.message });
            });        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.mytransfers = (req, res) => {
    TransferRecord.findAll({
        where: {targetNpm: req.npm},
        include: {
            model: TransactionRecord,
            attributes: ['npm', 'createdAt'],
            include: {
                model: User,
                attributes: ['fullname']
            }
        }, order: [['createdAt','DESC']]
    }).then(recievedrecords => {
        let recieved = [];
        recievedrecords.forEach((record) => {
            recieved.push({
                sender: {
                    npm: record.transaction_record.npm,
                    fullname: record.transaction_record.user.fullname
                },
                value: record.value,
                date: record.transaction_record.createdAt
            });
        });
        TransactionRecord.findAll({
            where: {npm: req.npm, type: "TRANSFER"},
            include: {
                model: TransferRecord,
                attributes: ['targetNpm'],
                include: {
                    model: User,
                    attributes: ['fullname']
                }
            }, order: [['createdAt','DESC']]
        }).then( sentrecords => {
            let sent = [];
            sentrecords.forEach((record) => {
                sent.push({
                    reciever: {
                        npm: record.transfer_record.targetNpm,
                        fullname: record.transfer_record.user.fullname
                    },
                    value: record.value,
                    date: record.createdAt
                });
            });
            // console.log(`[get coin data][${new Date()}] ${req.npm} request for get my tranfers`);
            res.status(200).send({
                data: {
                     sent: sent,
                     recieved: recieved
                 }
             });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

const midtransClient = require('midtrans-client');
const midtransConfig = require("../config/midtrans.config.js");

exports.midtranscreatetoken = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    if (req.body.value < 10000 || req.body.value > 1500000) {
        return res.status(422).send({ message: "Invalid value", errors: errors.array() })
    }
    if (req.body.payment_type != "gopay" && req.body.payment_type != "bank_transfer") {
        return res.status(422).send({ message: "Invalid payment type", errors: errors.array() })
    }
    // Create Snap API instance
    let snap = new midtransClient.Snap({
        isProduction : true,
        serverKey : midtransConfig.midtransserverkey,
        clientKey : midtransConfig.midtransclientkey
    });

    TransactionRecord.create({
        npm: req.npm,
        value: req.body.value,
        type: "TOP UP",
        status: "PENDING"
    }).then(record => {
        User.findOne({
            attributes: ['fullname','class'],
            where: {npm: req.npm}
        }).then(user => {
            let timestamp = new Date().getTime();
            let parameter = {
                "transaction_details": {
                    "order_id": `transaction#${record.id}#${timestamp}`,
                    "gross_amount": (req.body.payment_type == "bank_transfer")?(parseInt(req.body.value) + 4400):Math.ceil(parseInt(req.body.value)*1.0077)
                }, "enabled_payments":[
                    `${req.body.payment_type}`
                ], "credit_card":{
                    "secure" : true
                },
                "customer_details": {
                    "first_name": req.npm,
                    "last_name": user.fullname
                }
            };
            snap.createTransaction(parameter)
            .then((transaction)=>{
                let transactionToken = transaction.token;
                PendingToken.create({
                    transactionRecordId: record.id,
                    transactiontoken: transactionToken
                }).then(x => {
                    console.log(`[coin top up request][${new Date()}] top up request from ${req.npm}, ${req.body.value} coin with id ${record.id}`);
                    res.status(200).send({ transactionToken: transactionToken });
                }).catch(err => {
                    console.log(`[ERROR][${new Date()}]`);
                    console.log(err);
                    res.status(500).send({ message: err.message });
                });
            }).catch(err => {
                console.log(`[ERROR][${new Date()}]`);
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(`[ERROR][${new Date()}]`);
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(`[ERROR][${new Date()}]`);
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.midtransrecordtransaction = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    if (!req.body.order_id) {
        console.log('order id not provided');
        return res.status(422).send({message: "bad"});
    }
    if (!req.body.order_id.split('#')[1]) {
        console.log(`order id bad format:${req.body.order_id}`);
        return res.status(422).send({message: "bad"});
    }
    const transactionId = req.body.order_id.split('#')[1];
    if (req.body.status_code==200) {
        TransactionRecord.update({
            status: "SUCCESS"
        },{where: {id: transactionId}, returning: true}).then( x => {
            TransactionRecord.findOne({
                attributes: ['npm','value'],
                where: {id: transactionId}
            }).then(record => {
                User.update({
                    coin: Sequelize.literal(`(\`coin\` + ${record.value})`)
                },{where: {npm:record.npm}}).then(user => {
                    TopUpRequest.create({
                        transactionRecordId: transactionId,
                        midtranstransactionid: req.body.transaction_id,
                        midtranspaymenttype: req.body.payment_type
                    }).then(x => {
                        PendingToken.destroy({ where: {
                            transactionRecordId: transactionId
                        }}).then(x => {
                            console.log(`[coin top up success][${new Date()}] top up success from ${record.npm}, ${record.value} coin with id ${transactionId}`);
                            res.status(200).send({ message: "ok" });
                        }).catch(err => {
                            console.log(`[ERROR][${new Date()}]`);
                            console.log(err);
                            res.status(500).send({ message: err.message });
                        });
                    }).catch(err => {
                        console.log(`[ERROR][${new Date()}]`);
                        console.log(err);
                        res.status(500).send({ message: err.message });
                    });
                }).catch(err => {
                    console.log(`[ERROR][${new Date()}]`);
                    console.log(err);
                    res.status(500).send({ message: err.message });
                });
            }).catch(err => {
                console.log(`[ERROR][${new Date()}]`);
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    } else if (req.body.status_code==202) {
        TransactionRecord.update({
            status: "DECLINED"
        },{where: {id: transactionId}, returning: true}).then( x => {
            TransactionRecord.findOne({
                attributes: ['npm','value'],
                where: {id: transactionId}
            }).then(record => {
                TopUpRequest.create({
                    transactionRecordId: transactionId,
                    midtranstransactionid: req.body.transaction_id,
                    midtranspaymenttype: req.body.payment_type
                }).then(x => {
                    PendingToken.destroy({ where: {
                        transactionRecordId: transactionId
                    }}).then(x => {
                        console.log(`[coin top up failed][${new Date()}] top up failed from ${record.npm}, ${record.value} coin with id ${transactionId}`);
                        res.status(200).send({ message: "ok" });
                    }).catch(err => {
                        console.log(err);
                        console.log(`[ERROR][${new Date()}]`);
                        res.status(500).send({ message: err.message });
                    });
                }).catch(err => {
                    console.log(err);
                    console.log(`[ERROR][${new Date()}]`);
                    res.status(500).send({ message: err.message });
                });
            }).catch(err => {
                console.log(err);
                console.log(`[ERROR][${new Date()}]`);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            console.log(`[ERROR][${new Date()}]`);
            res.status(500).send({ message: err.message });
        });
    }
};

function dynamicSort(property) {
    var sortOrder = 1;
    if(property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a,b) {
        /* next line works with strings and numbers,
         * and you may want to customize it to your needs
         */
        var result = (a[property] > b[property]) ? -1 : (a[property] < b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

const Role = db.role;

exports.history = (req, res) => {
    User.findOne({
        where: {npm: req.npm},
        attributes: ['fullname','coin'],
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then( user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 3){
                isAdmin = true;
            }
        });
        TransactionRecord.findAll({where: {npm: req.npm}, order: [['createdAt','DESC']]}).then(recordout => {
            recordout.forEach((record) => {
                if (record.type == "TRANSFER") { record.type = "TRANSFER OUT"; }
            });
            TransactionRecord.findAll({
                where: {'$transfer_record.targetNpm$': req.npm},
                include: [{
                    model: TransferRecord,
                    as: 'transfer_record'
                },{
                    model: User,
                    attributes: ['npm','fullname']
                }],
                order: [['createdAt','DESC']]
            }).then(recordin => {
                recordin.forEach((record) => {
                    record.type = "TRANSFER IN";
                });
                record = [...recordout, ...recordin].sort(dynamicSort("createdAt"));
                // console.log(`[get coin data][${new Date()}] ${req.npm} request for its transasction data`);
                res.status(200).send({
                    data: record,
                    balance: user.coin,
                    isAdmin: isAdmin
                });
            }).catch(err => {
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    });
};

exports.allhistory = (req, res) => {
    TransactionRecord.findAll({order: [['createdAt','DESC']]}).then(record => {
        // console.log(`[get coin data][${new Date()}] ${req.npm} request for all transaction data`);
        res.status(200).send({ data: record });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.transactiondetail = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    TransactionRecord.findOne({
        where: {id: req.query.id},
        attributes: ['value','createdAt','updatedAt','type','status'],
        include: [{
            model: User,
            attributes: ['npm','fullname']
        },{
            model: PendingToken,
            attributes: ['transactiontoken']
        },{
            model: Order,
            attributes: ['quantity','subtotal'],
            include: {
                model: Item,
                attributes: ['id', 'name']
            }
        },{
            model: TransferRecord,
            attributes: ['targetNpm'],
            include: {
                model: User,
                attributes: ['npm','fullname']
            }
        }]
    }).then(record => {
        if (record.user.npm != req.npm) {
            return res.status(422).send({ message: "Mau ngapain kamu", errors: errors.array() })
        }
        // console.log(`[get coin data][${new Date()}] ${req.npm} request for details of transaction from ${req.npm} with id ${req.query.id}`);
        res.status(200).send({ data: {
            id: parseInt(req.query.id),
            type: record.type,
            value: record.value,
            status: record.status,
            createdAt: record.createdAt,
            updatedAt: record.updatedAt,
            user: record.user,
            reciever: (record.transfer_record==null)?null:record.transfer_record.user,
            pendingtoken: (record.pending_token==null)?null:record.pending_token.transactiontoken,
            orders: record.orders
        } });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

const csvwriter = require("csv-writer");
const createCsvWriter = csvwriter.createObjectCsvWriter
const config = require("../config/auth.config.js");

const jwt = require("jsonwebtoken");

exports.download = (req, res) => {
    let token = req.params.token;
    jwt.verify(token, config.secret, (err, decoded) => {
        if (err) {
            return res.status(401).send({
                message: "Unauthorized!"
            });
        }
        User.findOne({
                where: {
                    npm: decoded.npm
                },
                include: {
                    model: Role,
                    attributes: ['id']
                }
        }).then(user => {
            let isAdmin = false;
            user.roles.forEach((role) => {
                if (role.id == 3) {
                    isAdmin = true;
                }
            });
            if (!isAdmin){
                return res.status(403).send({
                    message: "Unsufficient privilege!"
                });
            }
        });
        TransactionRecord.findAll({
            where: { type: "TOP UP", status: "SUCCESS", createdAt: {
                [Op.gte]: new Date(new Date().setDate(new Date().getDate() - 7)),
            }},
            include: [{
                model: User,
                attributes: ['npm', 'fullname']
            },{
                model: TopUpRequest,
                attributes: ['midtranstransactionid']
            }],
            order: [['createdAt','DESC']]
        }).then(transactionrecords => {
            const date = Date.now().toString();
            transactions = []
            transactionrecords.forEach((record) => {
                transactions.push({
                    transaction_id: record.id,
                    npm: record.npm,
                    fullname: record.user.fullname,
                    value: record.value,
                    midtranstransactionid: record.topup_request.midtranstransactionid,
                    date: record.createdAt
                })
            });
            const csvWriter_transactions = createCsvWriter({
                path: `${__dirname}/../export/topuprecord/datatopup_${date}.csv`,
                header: [
                    { id: "transaction_id", title: "transaction id" },
                    { id: "npm", title: "npm" },
                    { id: "fullname", title: "fullname" },
                    { id: "value", title: "value" },
                    { id: "midtranstransactionid", title: "midtranstransactionid" },
                    { id: "date", title: "date" }
                ]
            });
            csvWriter_transactions.writeRecords(transactions).then(() => {
                const file = `${__dirname}/../export/topuprecord/datatopup_${date}.csv`;
                console.log(`[coin report download][${new Date()}] from ${req.npm}`);
                res.download(file);
            });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    });
};
