const db = require("../models");
const User = db.user;
const Book = db.book;
const BookLending = db.booklending;
const Building = db.building;
const Role = db.role;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

exports.list = (req, res) => {
    let where = (req.query.samebuilding == 'true') ? { buildingId: req.buildingId } : null
    Book.findAll({
        where: where,
        include: [BookLending, Building]
    }).then(books => {
        data = []
        books.forEach(book => {
            available = true
            book.book_lendings.forEach(lending => {
                if (lending.status != 'DONE') available = false;
            });
            data.push({
                title: book.title,
                pages: book.pages,
                author: book.author,
                id: book.id,
                available: available,
                building: book.building
            })
        });
        res.send({ data: data })
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.get = (req, res) => {
    Book.findOne({
        where: { id: req.query.bookId },
        include: [BookLending, Building]
    }).then(book => {
        res.send({ data: book })
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.create = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Book.create({
        id: req.body.id,
        title: req.body.title,
        pages: req.body.pages,
        author: req.body.author,
        description: req.body.description,
        buildingId: req.body.buildingId,
    }).then(x => {
        console.log(`[book create][${new Date()}] ${req.npm} input new book #${x.id}`);
        res.send({ message: "New book created" })
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

const fs = require('fs-extra');
exports.setpicture = (req, res) => {
    let ext = '';
    if (req.file.mimetype == 'image/png') {
        ext = 'png'
    } else {
        return res.status(422).send({ message: "Invalid file type" })
    }
    const newfilename = `${req.param.id}.${ext}`
    var src = fs.createReadStream(req.file.path);
    var dest = fs.createWriteStream(`${__dirname}/../export/static/library/${newfilename}`);
    src.pipe(dest);
    src.on('end', function () {
        fs.unlinkSync(req.file.path);
        res.status(200).send({ message: "Updated successfully!" });
    });
    src.on('error', function (err) {
        console.log(err);
        res.status(500).send({ err: err });
    });
};

exports.edit = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Book.update({
        title: req.body.title,
        pages: req.body.pages,
        author: req.body.author,
        description: req.body.description,
        buildingId: req.body.buildingId,
    }, { where: { id: req.body.bookId } }).then(x => {
        console.log(`[book edit][${new Date()}] ${req.npm} edit book #${x.id}`);
        res.status(200).send({ message: 'Updated successfully' });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.destroy = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    Book.destroy({ where: { id: req.body.bookId } }).then(data => {
        console.log(`[book delete][${new Date()}] ${req.npm} delete book #${x.id}`);
        res.status(200).send({ message: "Destroyed successfully!" });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
}
