const db = require("../models");
const User = db.user;
const Item = db.item;
const Order = db.order;
const TransactionRecord = db.transactionrecord;
const Sequelize = db.Sequelize;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

exports.all = (req, res) => {
    let where = {};
    if (req.query.pendingOnly == true) {
        where = { status: "PENDING" }
    }
    Order.findAll({ where: where }).then(orders => {
        res.status(200).send({ data: orders });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.get = (req, res) => {
    Order.findOne({
        where: { id: req.query.id }
    }).then(order => {
        res.status(200).send(order);
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

exports.order = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    reqOrders = [];
    try {
        reqOrders = JSON.parse(req.body.orders);
    } catch (error) {
        return res.status(422).send({ message: "Invalid input", error: error })
    }
    itemIds = [];
    reqOrders.forEach((order) => {
        itemIds.push(order.itemId);
    });
    if (itemIds.length == 0) {
        return res.status(422).send({ message: "No input recieved", errors: errors.array() })
    }
    Item.findAll({ where: { id: itemIds }, attributes: ['id', 'name', 'price', 'stock'] }).then(items => {
        itemDict = [];
        itemsUpdateObjects = [];
        items.forEach((a) => {
            itemDict[a.id] = { name: a.name, price: a.price, stock: a.stock };
        });
        let safe = true;
        orders = [];
        total = 0;
        totalQuantity = 0;
        reqOrders.forEach((order) => {
            order.itemName = itemDict[order.itemId].name;
            if (order.quantity < 1 || order.quantity > 5 || order.quantity > itemDict[order.itemId].stock) {
                safe = false;
                return res.status(422).send({ message: "Invalid quantity" });
            }
            totalQuantity += order.quantity;
            itemsUpdateObjects.push({
                id: order.itemId,
                name: itemDict[order.itemId].name,
                price: itemDict[order.itemId].price,
                stock: itemDict[order.itemId].stock - order.quantity
            })
            orders.push({
                itemId: order.itemId,
                quantity: order.quantity,
                subtotal: itemDict[order.itemId].price * order.quantity
            });
            order.subtotal = itemDict[order.itemId].price * order.quantity;
            total += order.subtotal;
        });
        if (totalQuantity > 5) {
            safe = false;
            return res.status(422).send({ message: "Invalid quantity" });
        }
        if (safe) {
            Item.bulkCreate(itemsUpdateObjects, {
                updateOnDuplicate: ['name', 'price', 'stock']
            }).then(y => {
                TransactionRecord.create({
                    npm: req.npm,
                    value: total,
                    type: "PAYMENT",
                    status: "PENDING"
                }).then(record => {
                    orders.forEach((order) => {
                        order.transactionRecordId = record.id;
                    });
                    Order.bulkCreate(orders).then(x => {
                        ids = []; //get new;y created order records
                        x.forEach((y) => {
                            ids.push(y.id);
                        });
                        User.findOne({
                            where: { npm: req.npm },
                            attributes: ['fullname']
                        }).then(user => {
                            TransactionRecord.update({
                                status: "PENDING"
                            }, { where: { id: record.id } }).then(y => {
                                console.log(`[order request][${new Date()}] ${req.npm} request payment of order#${record.id} ${total} coin`);
                                res.status(200).send({
                                    npm: req.npm,
                                    fullname: user.fullname,
                                    orders: reqOrders,
                                    transaction: {
                                        time: record.createdAt,
                                        total: total,
                                        id: record.id
                                    },
                                    message: "Order request success!"
                                });
                            }).catch(err => {
                                console.log(err);
                                res.status(500).send({ message: err.message });
                            });
                        }).catch(err => {
                            console.log(err);
                            res.status(500).send({ message: err.message });
                        });
                    }).catch(err => {
                        console.log(err);
                        res.status(500).send({ message: err.message });
                    });
                }).catch(err => {
                    console.log(err);
                    res.status(500).send({ message: err.message });
                });
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.cancel = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    TransactionRecord.findOne({
        where: { id: req.body.id },
        include: {
            model: Order,
            attributes: ['id', 'quantity'],
            include: {
                model: Item,
                attributes: ['id', 'name', 'price', 'stock']
            }
        }
    }).then(record => {
        if (record.status !== "PENDING" || record.npm !== req.npm) {
            return res.status(422).send({ message: "Rule violation!" });
        }
        let orderIds = [];
        let itemsUpdateObjects = [];
        record.orders.forEach((order) => {
            orderIds.push(order.id);
            itemsUpdateObjects.push({
                id: order.item.id,
                name: order.item.name,
                price: order.item.price,
                stock: order.item.stock + order.quantity
            });
        });
        Item.bulkCreate(itemsUpdateObjects, {
            updateOnDuplicate: ['name', 'price', 'stock']
        }).then(x => {
            Order.destroy({ where: { id: orderIds } }).then(y => {
                TransactionRecord.destroy({ where: { id: req.body.id } }).then(x => {
                    console.log(`[commerce payment cancel][${new Date()}] ${req.npm} canccled payment request #${record.id} with ${record.value} coin`);
                    res.status(200).send({ message: "Transaction cancelled!" });
                }).catch(err => {
                    res.status(500).send({ message: err.message });
                });
            }).catch(err => {
                res.status(500).send({ message: err.message });
                console.log(err);
            });
        }).catch(err => {
            res.status(500).send({ message: err.message });
            console.log(err);
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
    });
};

// exports.order = (req, res) => {
//     const errors = validationResult(req)
//     if (!errors.isEmpty()) {
//         return res.status(422).send({ message: "Invalid input", errors: errors.array() })
//     }
//     reqOrders = [];
//     try {
//         reqOrders = JSON.parse(req.body.orders);
//     } catch (error) {
//         return res.status(422).send({ message: "Invalid input", error: error })
//     }
//     itemIds = [];
//     reqOrders.forEach((order) => {
//         itemIds.push(order.itemId);
//     });
//     if (itemIds.length == 0) {
//         return res.status(422).send({ message: "No input recieved", errors: errors.array() })
//     }
//     Item.findAll({where:{id:itemIds}, attributes: ['id','name','price','stock']}).then(items => {
//         itemDict = [];
//         itemsUpdateObjects = [];
//         items.forEach((a) => {
//             console.log(a.id);
//             itemDict[a.id] = {name: a.name, price: a.price, stock: a.stock};
//         });
//         let safe = true;
//         orders = [];
//         total = 0;
//         reqOrders.forEach((order) => {
//             order.itemName = itemDict[order.itemId].name;
//             if (order.quantity < 1 || order.quantity > 5 || order.quantity > itemDict[order.itemId].stock) {
//                 safe = false;
//                 return res.status(422).send({ message: "Invalid quantity" });
//             }
//             itemsUpdateObjects.push({
//                 id: order.itemId,
//                 name: itemDict[order.itemId].name,
//                 price: itemDict[order.itemId].price,
//                 stock: itemDict[order.itemId].stock - order.quantity
//             })
//             orders.push({
//                 itemId: order.itemId,
//                 quantity: order.quantity,
//                 subtotal: itemDict[order.itemId].price * order.quantity
//             });
//             order.subtotal = itemDict[order.itemId].price * order.quantity;
//             total += order.subtotal;
//         });
//         if (safe) {
//             Item.bulkCreate(itemsUpdateObjects, {
//                 updateOnDuplicate: ['name', 'price', 'stock']
//             }).then( y => {
//                 TransactionRecord.create({
//                     npm: req.npm,
//                     value: total,
//                     type: "PAYMENT",
//                     status: "LOADING"
//                 }).then(record => {
//                     orders.forEach((order) => {
//                         order.transactionRecordId = record.id;
//                     });
//                     Order.bulkCreate(orders).then( x => {
//                         ids = []; //get new;y created order records
//                         x.forEach((y) => {
//                             ids.push(y.id);
//                         });
//                         User.findOne({
//                             where: {npm:req.npm},
//                             attributes: ['coin', 'fullname']
//                         }).then(user => {
//                             if(user.coin < total){
//                                 Order.destroy({where: {id:ids}}).then( y => {
//                                     TransactionRecord.destroy({where: {id:record.id}}).then( x => {
//                                         res.status(422).send({ message: "Not enough coin" });
//                                     });
//                                 });
//                             } else {
//                                 User.update({
//                                     coin: Sequelize.literal(`coin - ${total}`)
//                                 },{where: {npm:req.npm}}).then( z => {
//                                     TransactionRecord.update({
//                                         status: "PENDING"
//                                     },{where: {id:record.id}}).then( y => {
//                                         console.log(`[order request][${new Date()}] ${req.npm} request payment of order#${record.id} ${total} coin`);
//                                         res.status(200).send({
//                                             npm: req.npm,
//                                             fullname: user.fullname,
//                                             orders: reqOrders,
//                                             transaction: {
//                                                 time: record.createdAt,
//                                                 total: total,
//                                                 id: record.id
//                                             },
//                                             message: "Order request success!"
//                                         });
//                                     }).catch(err => {
//                                         res.status(500).send({ message: err.message });
//                                     });
//                                 }).catch(err => {
//                                     res.status(500).send({ message: err.message });
//                                 });
//                             }
//                         }).catch(err => {
//                             res.status(500).send({ message: err.message });
//                         });
//                     }).catch(err => {
//                         res.status(500).send({ message: err.message });
//                     });
//                 }).catch(err => {
//                     res.status(500).send({ message: err.message });
//                 });
//             }).catch(err => {
//                 res.status(500).send({ message: err.message });
//             });
//         }
//     }).catch(err => {
//         res.status(500).send({ message: err.message });
//     });
// };
