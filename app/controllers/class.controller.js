const db = require("../models");
const User = db.user;
const Class = db.class;
const ClassMeeting = db.classmeeting;
const SelfStudy = db.selfstudy;
const Role = db.role;
const FCMReToken = db.fcmretoken;
const Sequelize = db.Sequelize;

const Op = db.Sequelize.Op;
const { validationResult } = require('express-validator');

const moment = require('moment')

exports.myclassmate = (req, res) => {
    User.findOne({ where: { npm: req.npm } }).then(user => {
        Class.findOne({
            where: { id: user.classId },
            include: {
                model: User,
                attributes: ['npm', 'fullname', 'nickname']
            }
        }).then(x => {
            res.send({ data: x.users })
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.get = (req, res) => {
    User.findOne({
        where: { npm: req.npm },
        include: {
            model: Role,
            attributes: ['id']
        }
    }).then(user => {
        let isAdmin = false;
        user.roles.forEach((role) => {
            if (role.id == 5) {
                isAdmin = true;
            }
        });
        Class.findOne({
            where: { id: user.classId },
        }).then(x => {
            let TODAY_START = new Date().setHours(0, 0, 0, 0);
            let NOW = new Date();
            SelfStudy.findAll({
                where: {
                    createdAt: {
                        [Op.gte]: TODAY_START,
                        [Op.lte]: NOW
                    }
                }
            }).then(record => {
                res.send({ notes: x.notes, selfstudies: record, isAdmin: isAdmin })
            }).catch(err => {
                console.log(err);
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.setnotes = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({ where: { npm: req.npm } }).then(user => {
        Class.update({
            notes: req.body.notes
        }, {
            where: { id: user.classId },
        }).then(x => {
            console.log(`[classnote update][${new Date()}] ${req.npm} update classnote`);
            res.send({ message: "Class notes updated" })
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.getmeetings = (req, res) => {
    User.findOne({ where: { npm: req.npm } }).then(user => {
        let where = { classId: user.classId };
        if (req.query.recentonly) {
            where = {
                classId: user.classId,
                date: {
                    [Op.gte]: moment().subtract(14, 'days').toDate()
                }
            }
        }
        ClassMeeting.findAll({
            where: where,
            order: [['date', 'ASC']]
        }).then(x => {
            res.send({ data: x })
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.newmeeting = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({ where: { npm: req.npm } }).then(user => {
        ClassMeeting.create({
            lecture: req.body.lecture,
            date: req.body.date,
            meetingnumber: req.body.meetingnumber,
            resume: req.body.resume,
            coursework: req.body.coursework,
            classId: user.classId,
        }).then(x => {
            console.log(`[classmeeting submit][${new Date()}] ${req.npm} submit classmeeting#${x.id}`);
            res.send({ message: "New meeting note submitted" })
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.editmeeting = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({ where: { npm: req.npm } }).then(user => {
        ClassMeeting.findOne({ where: { id: req.body.meetingId } }).then(meeting => {
            if (meeting.classId != user.classId) {
                return res.status(403).send({ message: "Not your class!" });
            }
            ClassMeeting.update({
                lecture: req.body.lecture,
                date: req.body.date,
                meetingnumber: req.body.meetingnumber,
                resume: req.body.resume,
                coursework: req.body.coursework,
            }, { where: { id: req.body.meetingId } }).then(x => {
                console.log(`[classmeeting edit][${new Date()}] ${req.npm} edit classmeeting#${req.body.meetingId}`);
                res.status(200).send({ message: 'Updated successfully' });
            }).catch(err => {
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.deletemeeting = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    User.findOne({ where: { npm: req.npm } }).then(user => {
        ClassMeeting.findOne({ where: { id: req.body.meetingId } }).then(meeting => {
            if (meeting.classId != user.classId) {
                return res.status(403).send({ message: "Not your class!" });
            }
            ClassMeeting.destroy({ where: { id: req.body.meetingId } }).then(data => {
                console.log(`[classmeeting delete][${new Date()}] ${req.npm} delete classmeeting#${req.body.meetingId}`);
                res.status(200).send({ message: "Destroyed successfully!" });
            }).catch(err => {
                res.status(500).send({ message: err.message });
            });
        }).catch(err => {
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

const csvwriter = require("csv-writer");
const createCsvWriter = csvwriter.createObjectCsvWriter;
const zip = require('express-zip');
exports.meetingreport = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    ClassMeeting.findAll({
        include: {
            model: Class
        },
        order: [['classId', 'ASC'], ['date', 'ASC']]
    }).then(meetings => {
        array = []
        meetings.forEach((record) => {
            array.push({
                class: record.class.name,
                lecture: record.lecture,
                date: record.date,
                meetingnumber: record.meetingnumber,
                resume: record.resume,
                coursework: record.coursework,
            })
        });
        let time = Date.now()
        const csvWriter = createCsvWriter({
            path: `${__dirname}/../export/myclassrecord/datapertemuan_${time}.csv`,
            header: [
                { id: "class", title: "class" },
                { id: "lecture", title: "lecture" },
                { id: "date", title: "date" },
                { id: "meetingnumber", title: "meetingnumber" },
                { id: "resume", title: "resume" },
                { id: "coursework", title: "coursework" }
            ]
        });
        csvWriter.writeRecords(array).then(() => {
            const file = `${__dirname}/../export/myclassrecord/datapertemuan_${time}.csv`;
            console.log(`[classmeeting download][${new Date()}] ${req.npm} download classmeeting data`);
            res.zip([{
                path: file,
                name: `datapertemuan_${time}.csv`
            }]);
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

exports.newselfstudy = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    SelfStudy.create({
        has_homework: req.body.has_homework,
        homework_percentage: req.body.homework_percentage || 0,
        resume: req.body.resume,
        userNpm: req.npm
    }).then(x => {
        res.send({ message: "Attendance submitted" })
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
}

exports.selfstudyreport = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({ message: "Invalid input", errors: errors.array() })
    }
    let start = moment(req.query.date, 'YYYY-MM-DD').startOf('day')
    let end = moment(req.query.date, 'YYYY-MM-DD').endOf('day')

    SelfStudy.findAll({
        include: {
            model: User,
            include: {
                model: Class
            }
        },
        where: {
            createdAt: {
                [Op.gte]: start,
                [Op.lte]: end
            }
        },
        order: [['userNpm', 'ASC']]
    }).then(records => {
        User.findAll({
            include: [{ model: Class }]
        }).then(users => {
            recaparray = []
            notSubmitUsers = {}
            users.forEach((user, i) => {
                notSubmitUsers[user.npm] = { fullname: user.fullname, class: user.class.name }
            });
            records.forEach((record) => {
                recaparray.push({
                    fullname: record.user.fullname,
                    class: record.user.class.name,
                    has_homework: record.has_homework,
                    homework_percentage: record.homework_percentage,
                    resume: record.resume,
                    timestamp: record.createdAt
                })
                delete notSubmitUsers[record.user.npm]
            });
            let time = Date.now()
            const recapcsvWriter = createCsvWriter({
                path: `${__dirname}/../export/myclassrecord/datawajibbelajar_${time}.csv`,
                header: [
                    { id: "fullname", title: "fullname" },
                    { id: "class", title: "class" },
                    { id: "has_homework", title: "has_homework" },
                    { id: "homework_percentage", title: "homework_percentage" },
                    { id: "resume", title: "resume" },
                    { id: "timestamp", title: "timestamp" }
                ]
            });
            recapcsvWriter.writeRecords(recaparray).then(() => {
                const notSubmitcsvWriter = createCsvWriter({
                    path: `${__dirname}/../export/myclassrecord/databelumsubmitwajibbelajar_${time}.csv`,
                    header: [
                        { id: "fullname", title: "fullname" },
                        { id: "class", title: "class" }
                    ]
                });
                notSubmitcsvWriter.writeRecords(Object.values(notSubmitUsers)).then(() => {
                    const recapfile = `${__dirname}/../export/myclassrecord/datawajibbelajar_${time}.csv`;
                    const notSubmitfile = `${__dirname}/../export/myclassrecord/databelumsubmitwajibbelajar_${time}.csv`;
                    console.log(`[selfstudy download][${new Date()}] ${req.npm} download selfstudy data`);
                    res.zip([{
                        path: recapfile,
                        name: `datawajibbelajar_${time}.csv`
                    }, {
                        path: notSubmitfile,
                        name: `databelumsubmitwajibbelajar_${time}.csv`
                    }]);
                });
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};

const { sendNotification } = require('../utils/fcm.js')
exports.notifynotsubmitters = () => {
    let start = moment().startOf('day')
    let end = moment().endOf('day')

    SelfStudy.findAll({
        include: {
            model: User
        },
        where: {
            createdAt: {
                [Op.gte]: start,
                [Op.lte]: end
            }
        },
        order: [['userNpm', 'ASC']]
    }).then(records => {
        User.findAll({
            attributes: ['npm', 'fullname'],
            include: FCMReToken
        }).then(users => {
            notSubmitUsers = {}
            users.forEach((user, i) => {
                notSubmitUsers[user.npm] = { fullname: user.fullname, tokens: user.fcmregistrationtokens }
            });
            records.forEach((record) => {
                delete notSubmitUsers[record.user.npm]
            });
            let tokens = []
            for (const [npm, user] of Object.entries(notSubmitUsers)) {
                if (user.tokens)
                    user.tokens.forEach((token) => {
                        tokens.push(token.token)
                    })
            }
            sendNotification(tokens, {
                notification: {
                    title: 'WARNING!!!',
                    body: 'You haven\'t filled the self study form today'
                }
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({ message: err.message });
    });
};
