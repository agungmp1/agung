const db = require("../models");
const { sendNotification } = require('../utils/fcm.js')
const User = db.user;
const Title = db.title;
const Feedback = db.feedback;
const Sequelize = db.Sequelize;

const Op = db.Sequelize.Op;
const {
    validationResult
} = require('express-validator');
const { feedback } = require("../models");

exports.create = (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return res.status(422).send({
            message: "Invalid input",
            errors: errors.array()
        })
    }
    User.findOne({
        where: {
            npm: req.body.targetNpm
        },
        include: Title
    }).then(user => {
        let hasTitle = false
        user.titles.forEach(title => {
            if (title.id == req.body.titleId) {
                hasTitle = true
            }
        });
        if (!hasTitle) {
            return res.status(422).send({
                message: "Target user doesn't have the title",
            })
        }
        Feedback.create({
            userNpm: req.npm,
            targetNpm: req.body.targetNpm,
            titleId: req.body.titleId,
            rating: req.body.rating,
            body: req.body.body
        }).then(x => {
            console.log(`[feedback submit][${new Date()}] ${req.npm} submit feedback`);
            res.status(200).send({
                message: "Feedback submitted"
            });
        }).catch(err => {
            console.log(err);
            res.status(500).send({
                message: err.message
            });
        });
    }).catch(err => {
        console.log(err);
        res.status(500).send({
            message: err.message
        });
    });
};

exports.all = (req, res) => {
    Feedback.findAll({
        include: [{
            model: User,
            as: 'sender',
            attributes: ['fullname', 'npm']
        }, {
            model: User,
            as: 'target',
            attributes: ['fullname', 'npm']
        }, {
            model: Title,
            attributes: ['name']
        },]
    }).then(feedbacks => {
        res.status(200).send({
            data: feedbacks
        });
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
};

exports.clear = (req, res) => {
    Feedback.destroy({ where: {} }).then(x => {
        res.send({ message: "cleared successfully" })
        console.log(`[feedback clear][${new Date()}] ${req.npm} clear data`);
    }).catch(err => {
        res.status(500).send({ message: err.message });
        console.log(err);
    });
};

const zip = require('express-zip');
const csvwriter = require("csv-writer");
const createCsvWriter = csvwriter.createObjectCsvWriter;
exports.download = (req, res) => {
    Feedback.findAll({
        include: [{
            model: User,
            as: 'sender',
            attributes: ['fullname', 'npm']
        }, {
            model: User,
            as: 'target',
            attributes: ['fullname', 'npm']
        }, {
            model: Title,
            attributes: ['name']
        },]
    }).then(feedbacks => {
        array = []
        feedbacks.forEach((record) => {
            array.push({
                senderNpm: record.userNpm,
                senderName: record.sender.fullname,
                targetNpm: record.targetNpm,
                targetName: record.target.fullname,
                title: record.title.name,
                body: record.body,
                rating: record.rating,
                createdAt: record.createdAt,
            })
        });

        let date = Date.now();

        const csvWriter = createCsvWriter({
            path: `${__dirname}/../export/feedbackrecord/datafeedback_${date}.csv`,
            header: [
                { id: "senderNpm", title: "senderNpm" },
                { id: "senderName", title: "senderName" },
                { id: "targetNpm", title: "targetNpm" },
                { id: "targetName", title: "targetName" },
                { id: "title", title: "title" },
                { id: "body", title: "body" },
                { id: "rating", title: "rating" },
                { id: "createdAt", title: "createdAt" },
            ]
        });
        csvWriter.writeRecords(array).then(() => {
            const file = `${__dirname}/../export/feedbackrecord/datafeedback_${date}.csv`;
            console.log(`[feedback download][${new Date()}] ${req.npm} download feedback data`);
            res.zip([{
                path: file,
                name: `datafeedback_${date}.csv`
            }]);
        }).catch(err => {
            console.log(err);
            return res.status(500).send({ message: err.message });
        });
    }).catch(err => {
        console.log(err);
        return res.status(500).send({ message: err.message });
    });
};
