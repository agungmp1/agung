const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
var https = require('https');
var fs = require('fs');
const corsOptions = {
  origin: 'http://localhost',
  'Access-Control-Allow-Origin': 'http://localhost',
  withCredentials: false,
  'access-control-allow-credentials': true,
  credentials: false,
  optionSuccessStatus: 200,
}
const app = express();
const busboy = require('connect-busboy');
app.use(busboy());
// var corsOptions = {
//     origin: "http://localhost:8080"
// };

app.use(cors());//corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// app.use(function (req, res, next) {
//   res.setHeader('Access-Control-Allow-Origin', 'https://karya.korpstar-poltekssn.org');
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//   res.setHeader('Access-Control-Allow-Credentials', true);
//   next();
// });

const db = require("./app/models");

// db.sequelize.sync({ force: true }).then(() => {
//     console.log('Drop and Resync Db');
// db.sequelize.sync().then(() => {
//     initial();
//     console.log("db structur update");
// });

const cron = require('node-cron');
const fsExtra = require('fs-extra')

// const bot = require("./app/utils/telebot.js");
const fcm = require("./app/utils/fcm.js");

const { autoheal } = require("./app/controllers/foul.controller.js");
// bot.launch();

cron.schedule('0 0 * * *', function () {
  console.log('---------------------');
  console.log('Running Cron Job');
  fsExtra.emptyDirSync('./app/export/fastingrecord', err => {
    if (err) throw err;
  });
  fsExtra.emptyDirSync('./app/export/roomcheckingrecord', err => {
    if (err) throw err;
  });
  fsExtra.emptyDirSync('./app/export/aspiration', err => {
    if (err) throw err;
  });
  // bot.launch()
  autoheal()
});

var swaggerOptions = {
  swaggerOptions: {
    authAction: {
      JWT: {
        name: "JWT",
        schema: {
          type: "apiKey",
          in: "header",
          name: "authorization",
          description: ""
        },
        value: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJucG0iOiIyMDE5MTAxNjA5IiwiaWF0IjoxNjM3NTc1ODA5LCJleHAiOjE2Mzc2NjIyMDl9.WtwA9ZGz9tjyz3T-LCsMDbPRx09CbuYRk4Cf18M7eU0"
      }
    }
  }
};

const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json')
app.use('/doc', swaggerUi.serve, swaggerUi.setup(swaggerFile, swaggerOptions))

// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to korpstar application." });
});
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);
require('./app/routes/class.routes')(app);
require('./app/routes/zoom.routes')(app);
require('./app/routes/room.routes')(app);
require('./app/routes/achievement.routes')(app);
require('./app/routes/achievement.routes')(app);
require('./app/routes/fastingrecord.routes')(app);
require('./app/routes/item.routes')(app);
require('./app/routes/order.routes')(app);
require('./app/routes/config.routes')(app);
require('./app/routes/coin.routes')(app);
require('./app/routes/payment.routes')(app);
require('./app/routes/research.routes')(app);
require('./app/routes/foul.routes')(app);
require('./app/routes/jdih.routes')(app);
require('./app/routes/ctf.routes')(app);
require('./app/routes/aspiration.routes')(app);
require('./app/routes/static.routes')(app);

// var https_options = {
//   key: fs.readFileSync("/etc/ssl/wildcard/private.key"),
//   cert: fs.readFileSync("/etc/ssl/wildcard/star_korpstar-poltekssn_org.crt"),
//   ca: [
//           fs.readFileSync('/etc/ssl/wildcard/star_korpstar-poltekssn_org.crt'),
//           fs.readFileSync('/etc/ssl/wildcard/Chain_RootCA_Bundle.crt')
//        ]
// };

const socket = require('./app/socket/');
//socket.runsocket(app);

// Create an HTTPS service identical to the HTTP service.
// const server = https.createServer(https_options, app).listen(8443);

//set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

// socket.runsocket(server);

const User = db.user;
const Class = db.class;
const Config = db.config;
const FastingRecord = db.fastingrecord;
const Role = db.role;

// const {data} = require('./class.js');

function initial() {
  // Class.bulkCreate(data).then(x => {
  //     console.log("class created");
  // });

  // Class.findAll().then( classes => {
  //     User.findAll({attributes: ['npm', 'class']}).then( users => {
  //         users.forEach((user) => {
  //             let classId = 1;
  //             classes.forEach((classe) => {
  //                 if (user.classname == classe.name){
  //                     classId = classe.id
  //                 }
  //             });
  //             User.update({classId: classId}, {where: {npm: user.npm}})
  //         });
  //         console.log("success");
  //     })
  // })

  // data.forEach(record => {
  //     User.update({
  //         sportclub: record.sportclub,
  //         techclub: record.techclub,
  //         artclub: record.artclub,
  //         birthdate: record.birthdate,
  //         highschool: record.highschool,
  //     }, {where: {npm: record.npm}}).catch(err => {
  //         console.log(record.npm);
  //         console.log({ message: err.message });
  //     });
  // });
  // console.log('db updated');

  // User.bulkCreate(data).then(x => {
  //     FastingRecord.bulkCreate([
  //         { npm: "1817101467" },
  //         { npm: "1817101468" },
  //         { npm: "2019101600" },
  //         { npm: "2019101601" },
  //         { npm: "2019101602" },
  //         { npm: "2019101609" }
  //     ]);
  //     Role.create({
  //         id: 1,
  //         name: "FASTING ADMIN"
  //     });
  //     User.findOne({ where: {npm: "1817101465"} }).then( user => {
  //         user.setRoles(1);
  //     }).catch(err => {
  //         console.log({ message: err.message });
  //     });
  // });
  // Config.create({
  //     id: 1,
  //     fastingopen: true,
  //     fastingdate: 'Sat, 09 Oct 2021',
  // });
}
