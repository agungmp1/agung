# Karya Korps Taruna Poltek SSN Capture The Flag API

## Description
API routes for KaryaCTF, coded in node-express and implementing JWT authentication

## Table of Content
- [Karya Korps Taruna Poltek SSN Capture The Flag API](#karya-korps-taruna-poltek-ssn-capture-the-flag-api)
  * [Description](#description)
  * [Table of Content](#table-of-content)
    + [Get My Point](#get-my-point)
      - [Response](#response)
    + [Get Leaderboard](#get-leaderboard)
      - [Response](#response-1)
    + [Get Categories](#get-categories)
      - [Response](#response-2)
    + [Get Levels](#get-levels)
      - [Response](#response-3)
    + [Get All Challenges](#get-all-challenges)
      - [Sent Data](#sent-data)
      - [Response](#response-4)
    + [Get Challenge](#get-challenge)
      - [Sent Data](#sent-data-1)
      - [Response](#response-5)
    + [Check Flag](#check-flag)
      - [Sent Data](#sent-data-2)
      - [Response](#response-6)
    + [Write Up Submit](#write-up-submit)
      - [Sent Data](#sent-data-3)
      - [Response](#response-7)
    + [Get All Completions](#get-all-completions)
      - [Response](#response-8)
    + [Get Completions of a Challenge](#get-completions-of-a-challenge)
      - [Sent Data](#sent-data-4)
      - [Response](#response-9)
    + [Get Completions of a User](#get-completions-of-a-user)
      - [Sent Data](#sent-data-5)
      - [Response](#response-10)
    + [Get My Completions](#get-my-completions)
      - [Response](#response-11)
  * [Admin Routes](#admin-routes)
    + [Login](#login)
      - [Sent data](#sent-data)
      - [Response](#response-12)
    + [Get Category](#get-category)
      - [Sent Data](#sent-data-6)
      - [Response](#response-13)
    + [Add New Category](#add-new-category)
      - [Sent Data](#sent-data-7)
      - [Response](#response-14)
    + [Edit Category](#edit-category)
      - [Sent Data](#sent-data-8)
      - [Response](#response-15)
    + [Delete Category](#delete-category)
      - [Sent Data](#sent-data-9)
      - [Response](#response-16)
    + [Add New Challenge](#add-new-challenge)
      - [Sent Data](#sent-data-10)
      - [Response](#response-17)
    + [Edit Challenge](#edit-challenge)
      - [Sent Data](#sent-data-11)
      - [Response](#response-18)
    + [Delete Challenge](#delete-challenge)
      - [Sent Data](#sent-data-12)
      - [Response](#response-19)
    + [Get Completion Detail](#get-completion-detail)
      - [Sent Data](#sent-data-13)
      - [Response](#response-20)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>



### Get My Point
[GET] /api/ctf/getpoint
#### Response
```json
{
    "point": 0
}
```
### Get Leaderboard
[GET] /api/ctf/leaderboard
#### Response
```json
{
    "data": [
        {
            "npm": "1817101369",
            "fullname": "Achmad Husein Noor Faizi",
            "class": "4 RKS Echo",
            "ctf_point": 100
        },
        {
            "npm": "1817101370",
            "fullname": "Adam Waluyo",
            "class": "4 RKS Echo",
            "ctf_point": 0
        }
    ]
}
```
### Get Categories
[GET] /api/ctf/allcategories
#### Response
```json
{
    "data": [
        {
            "id": 2,
            "name": "Forensics"
        },
        {
            "id": 1,
            "name": "Web"
        }
    ]
}
```
### Get Levels
[GET] /api/ctf/alllevels
#### Response
```json
{
    "data": [
        {
            "id": 2,
            "name": "normal"
        },
        {
            "id": 1,
            "name": "ez"
        }
    ]
}
```
### Get All Challenges
[GET] /api/ctf/allchallenges
#### Params
specify category id to retrieve only certain category
|   params   | example value |
|:----------:|:-------------:|
| categoryId |       1       |
send nothing to retrieve all
#### Response
```json
{
    "data": [
        {
            "id": 2,
            "title": "Test",
            "point": 1,
            "solvesCount": 1,
            "createdAt": "2021-10-19T01:39:34.000Z",
            "ctf_category": {
                "id": 1,
                "name": "Web"
            },
            "ctf_level": {
                "id": 1,
                "name": "ez"
            }
        }
    ]
}
```
### Get Challenge
[GET] /api/ctf/getchallenge
#### Params
|    params   | example value |
|:-----------:|:-------------:|
| challengeId |       1       |
#### Response
```json
{
    "data": {
        "id": 2,
        "title": "Test",
        "description": "just a test.",
        "point": 1,
        "solvesCount": 1,
        "createdAt": "2021-10-19T01:39:34.000Z",
        "ctf_category": {
            "id": 1,
            "name": "Web"
        },
        "ctf_level": {
            "id": 1,
            "name": "ez"
        }
    },
    "isSolved": false
}
```
### Check Flag
[POST] /api/ctf/checkflag
#### Sent Data
```json
{
    "challengeId": 2,
    "flag": "a_testing_flag"
}
```
#### Response
```json
{
    "isTrue": true
}
```
### Write Up Submit
[POST] /api/ctf/submit
#### Sent Data
```json
{
    "challengeId": 2,
    "flag": "a_testing_flag",
    "writeup": "http://fakelink.example"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Get All Completions
[GET] /api/ctf/allcompletions  
specify date_from and/or date_to to get data from certain time range
|   params  | example value |
|:---------:|:-------------:|
| date_from |   2021-10-11  |
|  date_to  |   2021-10-20  |
send nothing to retrieve all
#### Response
```json
{
    "data": [
        {
            "id": 2,
            "createdAt": "2021-10-19T02:20:58.000Z",
            "user": {
                "npm": "2019101648",
                "fullname": "Rayhan Ramdhany Hanaputra",
                "class": "II Rekayasa Perangkat Lunak Kripto"
            },
            "ctf_challenge": {
                "id": 2,
                "title": "Test",
                "ctf_category": {
                    "id": 1,
                    "name": "Web"
                },
                "ctf_level": {
                    "id": 1,
                    "name": "ez"
                }
            }
        }
    ]
}
```
### Get Completions of a Challenge
[GET] /api/ctf/completionsofchallenge
#### Params
|    params   | example value |
|:-----------:|:-------------:|
| challengeId |       2       |
#### Response
```json
{
    "data": [
        {
            "id": 2,
            "createdAt": "2021-10-19T02:20:58.000Z",
            "user": {
                "npm": "2019101648",
                "fullname": "Rayhan Ramdhany Hanaputra"
            }
        }
    ]
}
```
### Get Completions of a User
[GET] /api/ctf/completionsofuser
#### Params
| params | example value |
|:------:|:-------------:|
|   npm  |   2019101648  |
#### Response
```json
{
    "data": [
        {
            "id": 2,
            "createdAt": "2021-10-19T02:20:58.000Z",
            "ctf_challenge": {
                "id": 2,
                "title": "Test",
                "ctf_category": {
                    "id": 1,
                    "name": "Web"
                },
                "ctf_level": {
                    "id": 1,
                    "name": "ez"
                }
            }
        }
    ]
}
```
### Get My Completions
[GET] /api/ctf/mycompletions
#### Response
```json
{
    "data": [
        {
            "id": 2,
            "writeup": "http://fakelink.example",
            "createdAt": "2021-10-19T02:20:58.000Z",
            "ctf_challenge": {
                "id": 2,
                "title": "Test",
                "ctf_category": {
                    "id": 1,
                    "name": "Web"
                },
                "ctf_level": {
                    "id": 1,
                    "name": "ez"
                }
            }
        }
    ]
}
```
## Admin Routes

### Login
[POST] /api/ctf/admin/signin
#### Sent data
```Json
{
    "npm": "INSERT_NPM",
    "password": "INSERT_PASSWORD_SHA1_DIGEST"
}
```
#### Response
```json
{
    "npm": "USER_NPM",
    "accessToken": "BEARER_TOKEN"
}
```
### Get Category
[GET] /api/ctf/getcategory
#### Params
|   params   | example value |
|:----------:|:-------------:|
| categoryId |       1       |
#### Response
```json
{
    "id": 1,
    "name": "CATEGORY_NAME"
}
```
### Add New Category
[POST] /api/ctf/addcategory
#### Sent Data
```json
{
    "name": "CATEGORY_NAME"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Edit Category
[POST] /api/ctf/editcategory
#### Sent Data
```json
{
    "categoryId": 1,
    "name": "CATEGORY_NAME"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Delete Category
[POST] /api/ctf/deletecategory
#### Sent Data
```json
{
    "categoryId": 1
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Add New Challenge
[POST] /api/ctf/newchallenge
#### Sent Data
```json
{
    "title": "Test",
    "description": "Just a test.",
    "point": 1,
    "categoryId": 1,
    "levelId": 1,
    "flag": "a_testing_flag"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Edit Challenge
[POST] /api/ctf/editchallenge
#### Sent Data
```json
{
    "challengeId": 1,
    "title": "Test",
    "description": "Just a test.",
    "point": 1,
    "categoryId": 1,
    "levelId": 1,
    "flag": "a_testing_flag"
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Delete Challenge
[POST] /api/ctf/deletechallenge
#### Sent Data
```json
{
    "challengeId": 1
}
```
#### Response
```json
{
    "message": "MESSAGE"
}
```
### Get Completion Detail
[GET] /api/ctf/completionsdetail
#### Params
|    params    | example value |
|:------------:|:-------------:|
| completionId |       2       |
#### Response
```json
{
    "data": {
        "id": 2,
        "writeup": "http://fakelink.example",
        "createdAt": "2021-10-19T02:20:58.000Z",
        "user": {
            "npm": "2019101648",
            "fullname": "Rayhan Ramdhany Hanaputra"
        },
        "ctf_challenge": {
            "id": 2,
            "title": "Test",
            "ctf_category": {
                "id": 1,
                "name": "Web"
            },
            "ctf_level": {
                "id": 1,
                "name": "ez"
            }
        }
    }
}
```
